import os
import yaml
from collections import namedtuple
import numpy as np


def convert(
    file_path: str=None,
    dictionary: dict=None
    ):
    """
    Description:
        코드에 적용할 설정 YAML 파일 불러오기.
    Args:
        file_path, str, yaml 파일 경로
        dictionary, dict, 반환시킬 딕셔너리
    Returns:
        GenericDict: namedtuple, yaml 파일을 GenericDict로 바꾼 것(수정불가)
    """
    if file_path is not None:
        assert file_path.endswith("yaml"), "the file should be .yaml format"
        with open(file_path, "r") as f:
            dictionary = yaml.full_load(f)
    if dictionary is not None:
        for k in dictionary.keys():
            if isinstance(dictionary[k], dict):
                dictionary[k] = convert(dictionary=dictionary[k])
        return namedtuple("GenericDict", dictionary.keys())(**dictionary)


def get_arguments(
    params: namedtuple=None
    ):
    args = {}
    for k in params._asdict().keys():
        args[k.lower()] = params._asdict()[k]
    return args


def return_dir(is_making=False, *args):
    """
    Description:
        디렉토리 경로 반환.
        해당 경로에 디렉토리가 없으면 생성.
    Args:
        *args: str, 디렉토리 생성할 개별 명칭
    Returns:
        dir_path: str, 디렉토리 경로
    """
    dir_path = os.path.join(*args)
    if not os.path.isdir(dir_path) and is_making:
        os.makedirs(dir_path)
    return dir_path


def combined_indices_of_cropped(origin_data_shape, model_input_shape):
    """
    Description:
        잘라낸 이미지로 전체 이미지 복원 위해 시작 index 계산.
    Args:
        origin_data_shape: tuple, 원본 영역 데이터 shape
        model_input_shape: tuple, 모델 입력 shape
    Returns:
        h_start_indices: list, 병합 이미지의 Height 쪽 시작 index
        w_start_indices: list, 병합 이미지의 Width 쪽 시작 index
    """
    _, data_h, data_w = origin_data_shape
    input_h, input_w, _ = model_input_shape

    # 가로세로 몇 개 나눌지
    h_num = data_h // input_h + 1
    w_num = data_w // input_w + 1

    # 겹치는 부분 처리
    h_total_res = h_num * input_h - data_h
    w_total_res = w_num * input_w - data_w
    h_res = h_total_res // (h_num - 1) + 1
    w_res = w_total_res // (w_num - 1) + 1
    
    # 겹치는 부분 리스트 생성, 마지막 부분은 원본 영역에 맞게 조정
    h_res_list = [h_res if not i == 0 else i for i in range(h_num)]
    if sum(h_res_list) > h_total_res:
        h_res_list[-1] -= sum(h_res_list) - h_total_res
    w_res_list = [w_res if not i == 0 else i for i in range(w_num)]
    if sum(w_res_list) > w_total_res:
        w_res_list[-1] -= sum(w_res_list) - w_total_res
    
    h_start_indices = [i * input_h - res for i, res in enumerate(np.cumsum(h_res_list))]
    w_start_indices = [i * input_w - res for i, res in enumerate(np.cumsum(w_res_list))]
    
    return h_start_indices, w_start_indices
