import os
import random
import numpy as np
import tensorflow as tf


def set_gpu(gpu_num, n_GB=1):
    """
    Description:
        사용할 GPU 및 해당 GPU의 메모리 제한 설정.
    Args:
        gpu_num: int or list, GPU index 값
        n_GB: float, 할당할 메모리 크기(GB) (default=1)
    Returns:
        None
    """
    # 물리적 GPU리스트 + 메모리 제한
    physical_devices = tf.config.list_physical_devices("GPU")
    if isinstance(gpu_num, int):
        tf.config.set_logical_device_configuration(
            physical_devices[gpu_num],
            [tf.config.LogicalDeviceConfiguration(memory_limit=1024*n_GB)]
            )
        # x번 GPU만 사용(여러개 쓸거면 슬라이싱도 됨)
        tf.config.set_visible_devices(physical_devices[gpu_num], "GPU")
    elif isinstance(gpu_num, list):
        for gpu in gpu_num:
            tf.config.set_logical_device_configuration(
                physical_devices[gpu],
                [tf.config.LogicalDeviceConfiguration(memory_limit=1024*n_GB)]
                )
        # x번 GPU만 사용(여러개 쓸거면 슬라이싱도 됨)
        tf.config.set_visible_devices([physical_devices[gpu] for gpu in gpu_num], "GPU")
    # 논리적 GPU리스트
    logical_devices = tf.config.list_logical_devices("GPU")
    # print(logical_devices)
    # return logical_devices
    

def set_gpu_growth(gpu_num):
    """
    Description:
        사용할 GPU gpu, memory_growth 설정.
        set_memory_growth로 해당 GPU 최대한 사용.
    Args:
        gpu_num: int or list, GPU index 값
    Returns:
        None
    """
    physical_devices = tf.config.list_physical_devices("GPU")
    if isinstance(gpu_num, int):
        device = physical_devices[gpu_num]
        tf.config.experimental.set_memory_growth(device, True)
        tf.config.set_visible_devices(device, "GPU")
    elif isinstance(gpu_num, list):
        devices = [physical_devices[gpu] for gpu in gpu_num]
        for device in devices:
            tf.config.experimental.set_memory_growth(device, True)
        tf.config.set_visible_devices(devices, "GPU")
    # 논리적 GPU리스트
    logical_devices = tf.config.list_logical_devices("GPU")


def set_random_seed(random_seed):
    """
    Description:
        Tensorflow에서 RANDOM SEED 고정.
    Args:
        random_seed: int, 설정할 random seed 값
    Returns:
        None
    """
    os.environ["PYTHONHASHSEED"] = str(random_seed)
    random.seed(random_seed)
    tf.random.set_seed(random_seed)
    np.random.seed(random_seed)
