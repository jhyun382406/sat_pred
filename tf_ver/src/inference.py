import os
import time
import numpy as np

from src.extract_data import Data
from src.preprocess import SatCalibration
from src.data_parser import min_max_normalizer, inv_min_max_normalizer
from utils.code_utils import return_dir, get_arguments, combined_indices_of_cropped
from utils.log_module import print_elapsed_time


class SatInfer(object):
    """Inference Satellite Timeseries data."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            위성(GK2A) 영상 시계열 데이터 추론 init 설정.
            모델 추론 결과 산출 및 저장.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(SatInfer, self).__init__()
        cfg_pr = cfg.PARAMS
        cfg_r = cfg.RAWDATA
        cfg_i = cfg.INFER
        
        self.chn = cfg_pr.DATA.CHANNEL
        self.output_seq = cfg_pr.DATA.OUTPUT_SEQ
        self.norm = cfg_pr.NORMALIZER
        self.input_shape = cfg_pr.DATA.INPUT_SHAPE
        self.time_step = cfg_r.TIME_STEP
        self.dtype = cfg_r.DATA_TYPE
        self.infer_loop = cfg_i.LOOP
        self.batch_size = cfg_i.BATCH_SIZE
        self.infer_saved_dir = cfg_i.OUTPUT_DIR
        self.infer_saved_file = cfg_i.OUTPUT_FILE
        
        self.time_data = Data()
        self.calib = SatCalibration(cfg, log, err_log)
        self.calib_col = cfg_pr.DATA.CALIBRATION.USE_COL
        
        self.log, self.err_log = log, err_log

    def __call__(self, model, infer_dataloader, utc_time_list):
        """
        Description:
            모델 추론.
            추론 후 위성 영상 이미지를 npy로 저장.
        Args:
            model: tf.keras.Model, Unet + ConvLSTM 모델
            infer_dataloader, tf.keras.Sequence 객체, Dataloader 역할(추론)
            utc_time_list: list, 추론 시작~최종시점 UTC로 변환한 시간대 리스트
        Returns:
            None
        """
        self.log.info(">>> Start Inference")
        start_time = time.time()
        
        # Calibration table & Latitude/Longitude array
        calibration_table = self.calib.make_calibration_table()
        latlon = self.calib.load_latlon()
        effective_area_pixel = self.calib.make_effective_area_pixel(latlon)
        
        result_concat = self.inference(model,
                                       infer_dataloader,
                                       utc_time_list,
                                       calibration_table,
                                       effective_area_pixel)
        self.save_results(result_concat, utc_time_list)
        
        h, m, s = print_elapsed_time(start_time)
        self.log.info(">>> Elapsed time: {0} hour {1} minute {2} second".format(h, m, s))
        self.log.info(">>> End Inference")
    
    def inference(self,
                  model,
                  infer_dataloader,
                  utc_time_list,
                  calibration_table,
                  effective_area_pixel):
        """
        Description:
            모델 추론.
            처음 추론 LOOP는 dataloader, 이후부턴 그냥 data 사용.
            추론 결과로 재추론을 할 때, 추론값이 방사됨.
                - min_max_normalizer로 다시 범주를 0~1로 맞춤.
                - 단순 0~1 clipping은 발산 정도를 잘 못 잡는 것으로 보임.
        Args:
            model: tf.keras.Model, Unet + ConvLSTM 모델
            infer_dataloader: tf.keras.Sequence 객체, Dataloader 역할(추론)
            utc_time_list: list, 추론 시작~최종시점 UTC로 변환한 시간대 리스트
            calibration_table: numpy array, 해당 채널의 Calibration table
            effective_area_pixel: tuple, 직사각 영역 위/경도 픽셀 (위도min, 위도max, 경도min, 경도max)
        Returns:
            result_concat: np.array, 추론 결과 데이터
                - Shape: (input_time_nums, input_seq_len, height, width, 1)
        """
        # Dataloader로 최초 추론
        result = model.predict(infer_dataloader)

        # # Sequence 길이만 LOOP만큼 곱함
        # result_concat_shape = list(result.shape)
        # result_concat_shape[1] *= self.infer_loop
        # result_concat = np.empty(tuple(result_concat_shape), dtype=np.float32)
        
        # # LOOP 돌며 추가 Sequence 추론
        # for i in range(self.infer_loop):
        #     result_concat[:, i*self.output_seq:(i+1)*self.output_seq, :, :, :] = result
        #     if not i == self.infer_loop - 1:
        #         # rersult = np.clip(result, 0, 1)
        #         result = min_max_normalizer(result, np.min(result), np.max(result))
        #         result = model.predict(result, batch_size=self.batch_size)
        
        # 원본 위성값으로 복원
        # result_reshape = self.restore_effective_area(result_concat,
        result_reshape = self.restore_effective_area(result,
                                                     utc_time_list,
                                                     effective_area_pixel)
        result_reshape = self.restore_origin_range(result_reshape,
                                                   calibration_table)
        self.log.info(">>> Result Shape: {0}".format(result_reshape.shape))
        
        return result_reshape
    
    def restore_effective_area(self,
                               result_concat,
                               utc_time_list,
                               effective_area_pixel):
        """
        Description:
            추론 결과를 input_shape 대로 crop 전의 유효 영역으로 복원.
        Args:
            result_concat: np.array, 추론 결과 데이터
            utc_time_list: list, 추론 시작~최종시점 UTC로 변환한 시간대 리스트
            effective_area_pixel: tuple, 직사각 영역 위/경도 픽셀 (위도min, 위도max, 경도min, 경도max)
        Return:
            result_reshape: np.array, 유효 영역으로 복원한 추론 결과 데이터
        """
        lat_pix_min, lat_pix_max, lon_pix_min, lon_pix_max = effective_area_pixel
        data_h, data_w = lat_pix_max - lat_pix_min, lon_pix_max - lon_pix_min
        input_h, input_w, _ = self.input_shape

        # 가로세로 몇 개 나눌지
        h_num = data_h // input_h + 1
        w_num = data_w // input_w + 1
        h_start_indices, w_start_indices = combined_indices_of_cropped((1, data_h, data_w), self.input_shape)
        
        result_reshape_shape = (len(utc_time_list), self.output_seq, data_h, data_w, 1)
        result_reshape = np.empty(result_reshape_shape, dtype=np.float32)
        result_reshape_divide = np.zeros_like(result_reshape)
        adapt_indices_range = np.arange(result_concat.shape[0], step=h_num*w_num)
        
        # Dataloader에서 crop한대로 복원
        CHECKNUM = 0
        for h_i in h_start_indices:
            for w_i in w_start_indices:
                adapt_indices = adapt_indices_range + CHECKNUM
                result_reshape[:, :, h_i:h_i+input_h, w_i:w_i+input_w, :] += result_concat[adapt_indices, ...]
                result_reshape_divide[:, :, h_i:h_i+input_h, w_i:w_i+input_w, :] += 1
                CHECKNUM += 1
        
        # 중복해서 더해진 부분 나누기
        result_reshape = np.divide(result_reshape, result_reshape_divide)

        return result_reshape

    def restore_origin_range(self, result_reshape, calibration_table):
        """
        Description:
            추론 결과를 normalize 전의 값으로 복원.
                - Calibration table 있으면 Calibration table 값으로 복원 후 위성 raw 값으로 복원
                - Calibration table 없으면 위성 raw 값으로 복원
                    - VI, NR: Radiance와 Albedo를 변환
                    - SW, WV, IR: Radiance와 Brightness Temperature를 변환
        Args:
            result_reshape: np.array, 유효 영역으로 복원한 추론 결과 데이터
            calibration_table: numpy array, 채널의 Calibration table
        Return:
            result_reshape: np.array, 위성 raw 값으로 복원한 추론 결과 데이터
        """
        # Normalizer inverse (반올림 처리까지)
        inv_normalizer_name = self.norm.INV_FUNCTION
        inv_normalizer = self.get_inv_normalizer(inv_normalizer_name, calibration_table)
        result_reshape = np.clip(result_reshape, 0, 1)
        result_reshape = inv_normalizer(result_reshape)
        
        # raw 값은 data type을 원본 netCDF 와 동일하게 적용
        if calibration_table is None:
            result_reshape = np.round(result_reshape)
            result_reshape = result_reshape.astype(getattr(np, self.dtype))
        else:
            const_cal_coeff, chn_cal_coeff = self.calib.load_calibration_coeff()
            chn = self.chn.lower()
            if chn.startswith("vi") or chn.startswith("nr"):
                result_reshape = self.calib.convert_alb2raw(result_reshape, chn_cal_coeff)
            elif chn.startswith("sw") or chn.startswith("wv") or chn.startswith("ir"):
                result_reshape = self.calib.convert_bt2raw(result_reshape, const_cal_coeff, chn_cal_coeff)
            else:
                self.err_log.error(">>> Cannot use channel: {0}".format(chn))
                self.log.info(">>> Cannot use channel: {0}".format(chn))
                self.log.info(">>> Just use vi, nr, sw, wv, ir")

        return result_reshape

    def get_inv_normalizer(self, inv_normalizer_name, calibration_table):
        """
        Description:
            Inverse Normalizer 만들기.
        Args:
            inv_normalizer_name: str, inverse normalizer 함수명 (data_parser.py)
            calibration_table: numpy array, 채널의 Calibration table
        Return:
            inv_normalizer: function, lambda 로 만들어진 inverse normalizer
        """
        self.log.info(">>> Adapt Inverse Normalize: {0}".format(inv_normalizer_name))
        normalizer_args = get_arguments(getattr(self.norm, self.chn.upper()))
        if not calibration_table is None:
            self.log.info(">>> Using Calibration table: {0}".format(self.calib_col))
            normalizer_args = self._raw_to_calib_normalizer(normalizer_args,
                                                            calibration_table)
        inv_normalizer = lambda x: eval(inv_normalizer_name)(x, **normalizer_args)
        return inv_normalizer
    
    def _raw_to_calib_normalizer(self, normalizer_args, calibration_table):
        """
        Description:
            Normalizer에서 raw 값이 아닌 Calibration table 값 사용할 때.
            VI 및 NR 채널은 raw 값과 Calibration 값의 양의 관계.
            SW, WV, IR 채널은 raw 값과 Calibration 값의 음의 관계.
            SW, WV, IR 채널에서 NaN인 경우는 가장 가까운 nanmin 값 반영.
        Args:
            normalizer_args: dict, config의 normalizer 옵션
            calibration_table: numpy array, 채널의 Calibration table
        Return:
            normalizer_args: dict, 값이 변경된 config의 normalizer 옵션
        """
        if self.chn.startswith("vi") or self.chn.startswith("nr"):
            normalizer_args = {
                k: calibration_table[v]
                for k, v
                in normalizer_args.items()
            }
        else:
            k1, k2 = normalizer_args.keys()
            normalizer_args[k1], normalizer_args[k2] = normalizer_args[k2], normalizer_args[k1]
            normalizer_args = {
                k: np.nanmin(calibration_table)
                if np.isnan(calibration_table[v])
                else calibration_table[v]
                for k, v
                in normalizer_args.items()
            }
        return normalizer_args

    def save_results(self, result_concat, utc_time_list):
        """
        Description:
            모델 결과 npy로 저장.
            np.save로 저장하여 np.load로 불러와야 함.
            저장은 ~~~/ticket_time/pred_time_01 ~ pred_time_xx
        Args:
            result_concat: np.array, 추론 결과 데이터
            utc_time_list: list, 추론 시작~최종시점 UTC로 변환한 시간대 리스트
        Returns:
            None
        """
        # 입력 UTC 시간대로 폴더 생성
        for idx, utc_time in enumerate(utc_time_list):
            saved_dir = return_dir(True,
                                   self.infer_saved_dir,
                                   self.chn,
                                   utc_time[:4],   # year
                                   utc_time[4:6],   # month
                                   utc_time[6:8],   # day
                                   utc_time)
            one_preds = result_concat[idx]
            try:
                # 각각 UTC 시간대로 폴더마다 OUTPUT_SEQ * LOOP 개수만큼 결과 저장
                self._save_result_npy(one_preds, utc_time, saved_dir)
            except Exception as e:
                self.err_log.error(">>> Error on save_results in {0}(UTC)".format(utc_time))
                self.err_log.error(">>> Error on save_results in {0}(UTC): {1}".format(utc_time, e))
            else:
                self.log.info(">>> Save {0}(UTC) results successfully".format(utc_time))
    
    def _save_result_npy(self, one_preds, utc_time, saved_dir):
        """
        Description:
            모델 결과 UTC_TIME 별 폴더에 개별 npy로 저장.
        Args:
            one_preds: np.array, 1개 UTC_TIME에 대한 추론 결과 데이터
                - Shape: (output_seq_len * loop, height, width, 1)
            utc_time: str, UTC 연월일시분(12자리)
            saved_dir: 해당 UTC_TIME의 추론 결과 저장 디렉토리
        Returns:
            None
        """
        for idx in range(self.output_seq * self.infer_loop):
            one_pred = one_preds[idx]
            utc_time = self.time_data._compute_date(utc_time, "min", self.time_step)
            save_file = self.infer_saved_file.format(self.chn, utc_time)
            save_path = os.path.join(saved_dir, save_file)
            np.save(save_path, one_pred)