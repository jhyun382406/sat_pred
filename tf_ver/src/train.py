import os
import time
import numpy as np
from tensorflow.keras.callbacks import Callback, CSVLogger

from utils.code_utils import return_dir
from utils.log_module import print_elapsed_time


class SatTrain(object):
    """Train using Satellite Timeseries data."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            위성(GK2A) 영상 시계열 데이터 학습 init 설정.
            모델 학습.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(SatTrain, self).__init__()
        cfg_tr = cfg.TRAIN
        self.chn = cfg.PARAMS.DATA.CHANNEL
        self.max_epoch = cfg_tr.EPOCH
        self.ckpt_period = cfg_tr.CKPT_PERIOD
        self.patience = cfg_tr.PATIENCE
        self.val_loss_ratio = cfg_tr.VAL_LOSS_RATIO
        self.min_train_epoch = cfg_tr.MIN_TRAIN_EPOCH
        self.model_name_fmt = cfg_tr.MODEL_NAME
        self.ckpt_dir = cfg_tr.CKPT_DIR
        self.model_dir = cfg_tr.MODEL_DIR
        self.hist_dir = cfg_tr.HIST_DIR
        self.trainlog_dir = cfg_tr.TRAINLOG_DIR

        self.log, self.err_log = log, err_log
        
    def __call__(self,
                 start_date,
                 model,
                 train_dataloader,
                 valid_dataloader):
        """
        Description:
            모델 학습.
            학습 중간 로그, 체크포인트, 최종모델 저장.
        Args:
            start_date: str, 모델 생성일자
            model: tf.keras.model 객체
            train_dataloader, tf.keras.Sequence 객체, Dataloader 역할(학습)
            valid_dataloader, tf.keras.Sequence 객체, Dataloader 역할(검증)
        """
        self.log.info(">>> Start Training")
        start_time = time.time()
        model_name = self.model_name_fmt.format(self.chn, start_date)
        # Make Csv logger directory
        trainlog_dir = return_dir(True, self.trainlog_dir, self.chn)
        csv_logger = CSVLogger(os.path.join(trainlog_dir, "{0}.txt".format(model_name)),
                               separator=",",
                               append=False)
        earlystop_ckpt = EarlyStopAndModelCkpt(self.chn,
                                               model_name,
                                               start_date,
                                               self.ckpt_dir,
                                               self.model_dir,
                                               self.log,
                                               self.ckpt_period,
                                               self.patience,
                                               self.val_loss_ratio,
                                               self.min_train_epoch)
        model_log = model.fit(train_dataloader,
                              validation_data=valid_dataloader,
                              epochs=self.max_epoch,
                              callbacks=[csv_logger, earlystop_ckpt],
                              verbose=0,   # 0: None / 1: progress bar / 2: print per epoch
                              shuffle=True)
                            #   workers=2,
                            #   use_multiprocessing=True)
        h, m, s = print_elapsed_time(start_time)
        actual_epoch = len(model_log.history["loss"])
        self.log.info(">>> Training Epochs: {0} / {1}".format(actual_epoch, self.max_epoch))
        self.log.info(">>> Elapsed time: {0} hour {1} minute {2} second".format(h, m, s))
        self.log.info(">>> End Training")


class EarlyStopAndModelCkpt(Callback):
    def __init__(self,
                 chn,
                 model_name,
                 start_date,
                 ckpt_dir,
                 model_dir,
                 log,
                 ckpt_period=1,
                 patience=0,
                 val_loss_ratio=0.7,
                 min_train_epoch=10):
        """
        Description:
            모델 학습 조기종료 및 중간 저장, 최종 모델 저장 init 설정.
            최소 학습 epoch은 학습하도록 설정.
            학습 중간의 로그를 찍기 위해 추가.
        Args:
            chn, str, 위성 채널
            model_name: str, 모델 이름
            start_date: str, 모델 생성일자
            ckpt_dir: str, 체크포인트 저장 디렉토리
            model_dir: str, 모델 저장 디렉토리
            log: logging.logger, 로그 객체
            ckpt_period: int, 체크포인트 저장 epoch 주기
            patience: int, 조기종료에 반영할 개선되지 않는 epoch 수
            val_loss_ratio: float, 최적 모델 기준에 반영할 validation loss 비율
                            (1-val_loss_ratio) * loss + val_loss_ratio * val_loss 로 이용
            min_train_epoch: int, 최소한 학습에는 적용할 epoch 수
        """
        super(EarlyStopAndModelCkpt, self).__init__()
        self.ckpt_period = ckpt_period
        self.patience = patience
        self.val_loss_ratio = val_loss_ratio
        self.min_train_epoch = min_train_epoch
        # best_weights to store the weights at which the minimum loss occurs.
        self.best_weights = None
        # Make Saved directory
        self.ckpt_path = return_dir(True, ckpt_dir, chn, start_date, model_name)
        self.model_path = return_dir(True, model_dir, chn, start_date, model_name)
        self.log = log
        self.print_log_head = "   Epoch {0} :::::>>> "
        self.add_str = "{0}:{1:5.5f} / "

    def on_train_begin(self, logs=None):
        """
        Description:
            모델 학습 시작 때 1회 적용.
            loss가 작아야 좋으면 초기 best를 Infinity, 커야 좋으면 0으로 설정.
        """
        # The number of epoch it has waited when loss is no longer minimum.
        self.wait = 0
        # The epoch the training stops at.
        self.stopped_epoch = 0
        # Initialize the best as infinity.
        self.best = np.Inf
        #self.best = 0

    def on_epoch_end(self, epoch, logs=None):
        """
        Description:
            모델 학습 epoch 종료 때 실행.
            current는 checkpoint, 조기종료 때 보는 기준값.
            loss가 작아야 좋으면 np.less, 커야 좋으면 np.greater로 설정.
            Best current 일 때는 모델도 저장.
            SavedModel 방식으로 protobuf 바이너리와 TensorFlow 체크포인트를 포함하는 디렉토리.
        """
        # Print log
        log_str = ""
        for k, v in logs.items():
            log_str += self.add_str.format(k, v)
        log_str = self.print_log_head.format(epoch+1) + log_str
        self.log.info(log_str)

        # Save Checkpoint
        if (epoch + 1) % self.ckpt_period == 0:
            self.model.save_weights(self.ckpt_path)

        current = self.val_loss_ratio * logs.get("val_loss") + (1 - self.val_loss_ratio) * logs.get("loss")
        if epoch <= self.min_train_epoch - 1:
            self.log.info("   Save Model before Minimum train epoch: {0}/{1}".format(epoch + 1, self.min_train_epoch))
            self.model.save(self.model_path)
        #if np.greater(current, self.best):
        elif np.less(current, self.best):
            self.best = current
            self.wait = 0
            # Record the best weights if current results is better (greater).
            self.best_weights = self.model.get_weights()
            # Save the best weights checkpoint if current results is better (greater).
            self.log.info("   Save Best Model : {0:5.5f}".format(current))
            self.model.save(self.model_path)
        else:
            self.wait += 1
            if self.wait >= self.patience:
                self.stopped_epoch = epoch
                self.model.stop_training = True
                self.log.info("   Restoring model weights from the end of the best epoch.")
                self.model.set_weights(self.best_weights)

    def on_train_end(self, logs=None):
        """
        Description:
            모델 학습 종료 때 1회 실행.
            모델을 SavedModel 방식로 저장(모델의 아키텍처, 가중치 및 훈련 구성을 저장).
            형식은 protobuf 바이너리와 TensorFlow 체크포인트를 포함하는 디렉토리
        """
        if self.stopped_epoch > 0:
            self.log.info("   Epoch {0:03d} : End".format(self.stopped_epoch + 1))
            self.model.set_weights(self.best_weights)
            self.model.save(self.model_path)
        
