import tensorflow as tf
from tensorflow.keras.losses import Loss


class AmpMae(Loss):
    """
    Description:
        증폭 계수 반영된 MAE.
    """
    def __init__(self, amplification=100):
        """
        Description:
            Class init.
        Args:
            amplification: int, 결과값에 곱할 배수 (default=100)
        """
        super(AmpMae, self).__init__()
        self.amplification = amplification
    
    def call(self, y_true, y_pred):
        """
        Description:
            증폭 계수 반영된 MAE 계산.
        Args:
            y_true: tensor, 실제값
            y_pred: tensor, 추론값
        Returns:
            loss: float, Loss 결과
        """
        err = self.amplification * tf.math.abs(y_true - y_pred)
        loss = tf.math.reduce_mean(err)
        return loss


class AmpMse(Loss):
    """
    Description:
        증폭 계수 반영된 MSE.
    """
    def __init__(self, amplification=100):
        """
        Description:
            Class init.
        Args:
            amplification: int, 결과값에 곱할 배수 (default=100)
        """
        super(AmpMse, self).__init__()
        self.amplification = amplification
    
    def call(self, y_true, y_pred):
        """
        Description:
            증폭 계수 반영된 MSE 계산.
        Args:
            y_true: tensor, 실제값
            y_pred: tensor, 추론값
        Returns:
            loss: float, Loss 결과
        """
        err = self.amplification * tf.math.abs(y_true - y_pred)
        loss = tf.math.reduce_mean(tf.math.square(err))
        return loss


class AmpHuber(Loss):
    """
    Description:
        증폭 계수 반영된 Huber.
    """
    def __init__(self, delta=1.0, amplification=100):
        """
        Description:
            Class init.
        Args:
            delta: float, Huber에서 linear 함수에 반영할 기울기 (default=1.0)
            amplification: int, 결과값에 곱할 배수 (default=100)
        """
        super(AmpHuber, self).__init__()
        self.delta = delta
        self.amplification = amplification
    
    def call(self, y_true, y_pred):
        """
        Description:
            증폭 계수 반영된 Huber 계산.
        Args:
            y_true: tensor, 실제값
            y_pred: tensor, 추론값
        Returns:
            loss: float, Loss 결과
        """
        err = self.amplification * tf.math.abs(y_true - y_pred)
        is_small_error = tf.math.abs(err) <= self.delta
        small_err_loss = 0.5 * tf.math.square(err)
        big_err_loss = self.delta * (tf.math.abs(err) - (0.5 * self.delta))
        return tf.where(is_small_error, small_err_loss, big_err_loss)


class AmpLogCosh(Loss):
    """
    Description:
        증폭 계수 반영된 LogCosh.
    """
    def __init__(self, amplification=100):
        """
        Description:
            Class init.
        Args:
            amplification: int, 결과값에 곱할 배수 (default=100)
        """
        super(AmpLogCosh, self).__init__()
        self.amplification = amplification
    
    def call(self, y_true, y_pred):
        """
        Description:
            증폭 계수 반영된 LogCosh 계산.
        Args:
            y_true: tensor, 실제값
            y_pred: tensor, 추론값
        Returns:
            loss: float, Loss 결과
        """
        err = self.amplification * tf.math.abs(y_pred - y_true)
        loss = tf.math.log((tf.math.exp(err) + tf.math.exp((-1)*err)) / 2)
        return loss


class PixelWiseRegularizer(Loss):
    """
    Description:
        DGMR에 반영됐던 PixelWiseRegularizer.
    """
    def __init__(self,
                 amplification=10,
                 magnitude=20,
                 min_value=0.5,
                 max_value=1.5,
                 inverse=False):
        """
        Description:
            Class init.
            큰 값에 더 큰 가중치 or 작은 값에 더 큰 가중치 flag로 추가.
        Args:
            amplification: int, 실제값과 추론값 차이에 곱할 배수 (default=10)
            magnitude: int, 가중치 적용 후 최종 Loss에 반영할 증폭값 (default=20)
            min_value: float, 가중치에 적용할 최소값 (default=0.5)
            max_value: float, 가중치에 적용할 최대값 (default=1.5)
            inverse: bool, 가중치를 역수로 반영할지 여부 (default=False)
        """
        super(PixelWiseRegularizer, self).__init__()
        self.amplification = amplification
        self.magnitude = magnitude
        self.min_value = min_value
        self.max_value = max_value
        self.inverse = inverse
    
    def call(self, y_true, y_pred):
        """
        Description:
            PixelWiseRegularizer loss 계산.
            DGMR 때와의 변경점은 y_true, y_pred가 normalize되어 0~1이기 때문에
            가중치 곱해서 값이 작아지는 경우 방지하기 위해 exp 를 추가 반영.
        Args:
            y_true: tensor, 실제값
            y_pred: tensor, 추론값
        Returns:
            loss: float, Loss 결과
        """
        weight = tf.clip_by_value(y_true,
                                  clip_value_min=self.min_value,
                                  clip_value_max=self.max_value)
        weight = tf.math.exp(weight)
        if self.inverse:
            weight = 1 / weight
        difference = self.amplification * tf.math.abs(y_pred - y_true)
        loss = difference * weight
        return self.magnitude * tf.math.reduce_mean(loss)


class AdaptRegularizerLoss(Loss):
    """
    Description:
        Regularizer를 반영한 loss.
    """
    def __init__(self, main_loss, regularizer):
        """
        Description:
            Class init.
        Args:
            main_loss: function, 메인 손실함수
            regularizer: function, Regularizer
        """
        super(AdaptRegularizerLoss, self).__init__()
        self.main_loss = main_loss
        self.regularizer = regularizer
    
    def call(self, y_true, y_pred):
        """
        Description:
            Regularizer 값도 반영한 loss 계산.
        Args:
            y_true: tensor, 실제값
            y_pred: tensor, 추론값
        Returns:
            loss: float, Loss 결과
        """
        return self.main_loss(y_true, y_pred) + self.regularizer(y_true, y_pred)
