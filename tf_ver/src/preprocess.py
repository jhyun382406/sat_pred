import os
from datetime import timezone, timedelta, datetime
from itertools import chain
import numpy as np
import pandas as pd
import dask.array as da
import ray
from pysolar.solar import get_altitude

from utils.code_utils import get_arguments, convert
from src.extract_data import GK2ADataExtractor
from src.dataset import GK2ADataset, GK2ADatasetV2
from src.data_parser import *


class SatPrep(object):
    """Preprocess using Satellite Timeseries data."""
    def __init__(self, cfg, log, err_log, pred_mode=False):
        """
        Description:
            위성(GK2A) 영상 시계열 데이터 전처리 init 설정.
            학습: Model에 fit시킬 Dataloader 생성.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
            pred_mode: bool, 추론 모드 판단
        """
        super(SatPrep, self).__init__()
        # Data
        cfg_d = cfg.PARAMS.DATA
        cfg_tr = cfg.TRAIN
        cfg_i = cfg.INFER
        self.cfg_params = cfg.PARAMS
        self.pred_mode = pred_mode
        self.chn = cfg_d.CHANNEL
        self.in_seq = cfg_d.INPUT_SEQ
        self.out_seq = cfg_d.OUTPUT_SEQ
        self.fill_method = cfg_d.FILL.METHOD
        self.fill_limit = cfg_d.FILL.LIMIT
        if not pred_mode:
            self.total_seq = cfg_d.INPUT_SEQ + cfg_d.OUTPUT_SEQ
        else:
            self.total_seq = cfg_d.INPUT_SEQ
        
        # Training preprocess
        self.cfg_norm = cfg.PARAMS.NORMALIZER
        self.dat_dir = cfg_tr.DATA_DIR
        self.dat_names = [d.format(self.chn) for d in cfg_tr.DATA_FILE]
        self.shape = cfg_tr.DATA_SHAPE
        self.valid_dat_names = [d.format(self.chn) for d in cfg_tr.VALID_DATA_FILE]
        self.valid_shape = cfg_tr.VALID_DATA_SHAPE
        self.dtype = getattr(np, cfg_tr.DATA_TYPE)
        self.batch_size = cfg_tr.BATCH_SIZE

        # Inference preprocessing
        self.infer_batch_size = cfg_i.BATCH_SIZE
        self.time_step = cfg.RAWDATA.TIME_STEP
        self.sat = GK2ADataExtractor(cfg, log, err_log, pred_mode)
        
        # Additional
        self.CVT_TIME_FORMAT = cfg.RAWDATA.CVT_TIME_FORMAT
        self.TZ_KST = timezone(timedelta(hours=9))
        self.calib = SatCalibration(cfg, log, err_log)
        self.use_random_crop = cfg_d.RANDOMCROP.USE
        self.input_shape = cfg_d.INPUT_SHAPE

        self.log, self.err_log = log, err_log
        
    def __call__(self, kst_start_time=None, kst_end_time=None):
        """
        Description:
            학습, 추론 전처리.
            GK2A Calibration table 사용 분기 및 태양천정각 사용 분기 적용.
                - GK2A Calibration table: 위성 raw 값 말고 radiance, albedo, brightness temperature 등으로 변환.
                - 태양천정각: 주간, 야간, 여명/황혼 분리.
        Args:
            학습 전처리
                None
            추론 전처리
                - kst_start_time: str, 추론할 기준 시작시점(연월일시분 12자리)
                - kst_end_time: str, 추론할 기준 최종시점(연월일시분 12자리)
        Returns:
            학습 전처리
                - train_dataloader: tf.keras.Sequence 객체, Dataloader 역할(학습)
                - valid_dataloader: tf.keras.Sequence 객체, Dataloader 역할(검증)
            추론 전처리
                - infer_dataloader: tf.keras.Sequence 객체, Dataloader 역할(추론)
                - utc_time_list: list, 추론 시작~최종시점 UTC로 변환한 시간대 리스트
        """
        # Calibration table & Latitude/Longitude array
        calibration_table = self.calib.make_calibration_table()
        latlon = self.calib.load_latlon()
        effective_area_pixel = self.calib.make_effective_area_pixel(latlon)
        
        # Branch Training mode or Prediction mode
        if not self.pred_mode:
            train_dataloader, valid_dataloader = self.train_prep(calibration_table,
                                                                 latlon,
                                                                 effective_area_pixel)
            return train_dataloader, valid_dataloader
        else:
            infer_datas = self.get_infer_datas(kst_start_time,
                                               kst_end_time,
                                               effective_area_pixel)
            kst_time_list = self.sat._make_time_list(kst_start_time,
                                                     kst_end_time,
                                                     "min",
                                                     self.time_step)
            utc_time_list = [self.sat._kst_to_utc(t) for t in kst_time_list]
            infer_dataloader = self.infer_prep(infer_datas,
                                               utc_time_list,
                                               calibration_table,
                                               latlon)
            return infer_dataloader, utc_time_list
    
    def train_prep(self, calibration_table, latlon, effective_area_pixel):
        """
        Description:
            학습 전처리.
            모델에 사용할 dataloader 생성.
                - dat 파일 불러오기
                - 파일이 없어 결측 처리 안된 부분을 전후 데이터로 결측 처리
                - 연속적인 시간대 Sequence의 index를 추출
                - dataloader 생성
        Args:
            calibration_table: numpy array, 채널의 Calibration table
            latlon: numpy array, 채널의 해상도에 맞는 위/경도
            effective_area_pixel: tuple, 직사각 영역 위/경도 픽셀 (위도min, 위도max, 경도min, 경도max)
        Returns:
            train_dataloader: tf.keras.Sequence 객체, Dataloader 역할(학습)
                - Input Shape: (batch_size, input_seq_len, height, width, 1)
                - Output Shape: (batch_size, output_seq_len, height, width, 1)
            valid_dataloader: tf.keras.Sequence 객체, Dataloader 역할(검증)
                - Input Shape: (batch_size, input_seq_len, height, width, 1)
                - Output Shape: (batch_size, output_seq_len, height, width, 1)
        """
        train_datas = self.get_train_datas("Train",
                                           self.dat_names,
                                           self.shape,
                                           effective_area_pixel)
        train_datas, train_nonfill_indices = self.fill_zero_frame(train_datas)
        valid_datas = self.get_train_datas("Valid",
                                           self.valid_dat_names,
                                           self.valid_shape,
                                           effective_area_pixel)
        valid_datas, valid_nonfill_indices = self.fill_zero_frame(valid_datas)
        
        normalizer_name = self.cfg_norm.FUNCTION
        if normalizer_name is not None:
            normalizer = self.get_normalizer(normalizer_name, calibration_table)
        else:
            normalizer = None
        
        # Branch using SZA
        if latlon is None:
            train_dataloader = GK2ADataset(
                memory_map=train_datas,
                batch_size=self.batch_size,
                in_seq=self.in_seq,
                out_seq=self.out_seq,
                normalizer=normalizer,
                pred_mode=self.pred_mode
            )
            valid_dataloader = GK2ADataset(
                memory_map=valid_datas,
                batch_size=self.batch_size,
                in_seq=self.in_seq,
                out_seq=self.out_seq,
                normalizer=normalizer,
                pred_mode=self.pred_mode
            )
        else:
            # Make usable indices
            train_kst_obj_list = self.get_file_times(self.dat_names)
            valid_kst_obj_list = self.get_file_times(self.valid_dat_names)
            file_exclude_seq_indices = self.make_file_exclude_seq_indices(self.dat_names, self.shape)
            train_seq_indices, valid_seq_indices = self.make_trainable_indices(latlon,
                                                                               train_kst_obj_list,
                                                                               valid_kst_obj_list,
                                                                               train_nonfill_indices,
                                                                               valid_nonfill_indices,
                                                                               file_exclude_seq_indices)
            # Make dataloader
            train_dataloader = GK2ADatasetV2(
                memory_map=train_datas,
                batch_size=self.batch_size,
                in_seq=self.in_seq,
                out_seq=self.out_seq,
                use_random_crop=self.use_random_crop,
                input_shape=self.input_shape,
                normalizer=normalizer,
                calibration_table=calibration_table,
                seq_indices=train_seq_indices,
                pred_mode=self.pred_mode
            )
            valid_dataloader = GK2ADatasetV2(
                memory_map=valid_datas,
                batch_size=self.batch_size,
                in_seq=self.in_seq,
                out_seq=self.out_seq,
                use_random_crop=self.use_random_crop,
                input_shape=self.input_shape,
                normalizer=normalizer,
                calibration_table=calibration_table,
                seq_indices=valid_seq_indices,
                pred_mode=self.pred_mode
            )
        self.log.info(">>> Using Train Data nums: {0} (Batch_size: {1})".format(len(train_dataloader), self.batch_size))
        self.log.info(">>> Using Valid Data nums: {0} (Batch_size: {1})".format(len(valid_dataloader), self.batch_size))
        return train_dataloader, valid_dataloader
    
    def get_train_datas(self, kinds, dat_names, shape, effective_area_pixel):
        """
        Description:
            학습용, 검증용 데이터 불러오기.
            유효 학습 영역만 잘라내어 가져오기.
        Args:
            kinds: str, 학습/검증 구분
            dat_names: str, 데이터 파일명(KST)
            shape: list, 데이터 shape
            effective_area_pixel: tuple, 직사각 영역 위/경도 픽셀 (위도min, 위도max, 경도min, 경도max)
        Return:
            datas: dask.array, np.memmap으로 불러온 데이터를 dask array화
                - Shape: (Data nums, height, width)
        """
        self.log.info(">>> Load {0} datas".format(kinds))
        memmap_list = []
        for data_name, sp in zip(dat_names, shape):
            self.log.info(">>> file: {0}, shape: {1}, dtype: {2}".format(data_name, sp, self.dtype))
            data_path = os.path.join(self.dat_dir, data_name)
            memmap = np.memmap(data_path, dtype=self.dtype, shape=tuple(sp), mode="r", order="C")
            memmap_list.append(memmap)
        datas = da.concatenate(memmap_list, axis=0)
        
        # Crop effective area
        if not effective_area_pixel is None:
            lat_pixel_min, lat_pixel_max, lon_pixel_min, lon_pixel_max = effective_area_pixel
            self.log.info("Using Effective area lat/lon pixel : {0} ~ {1}, {2} ~ {3}".format(lat_pixel_min, lat_pixel_max, lon_pixel_min, lon_pixel_max))
            datas = datas[:, lat_pixel_min:lat_pixel_max, lon_pixel_min:lon_pixel_max]

        self.log.info(">>> {0} data shape: {1}".format(kinds, datas.shape))
        return datas
    
    def fill_zero_frame(self, datas):
        """
        Description:
            학습용 데이터에 결측 처리.
                - dat 파일 생성 시, 전체 결측 파일은 모든 값이 0
            bfill 또는 ffill 방식으로 결측 처리.
                - bfill: 이후 데이터를 limit 개수만큼 복제
                - ffill: 이전 데이터를 limit 개수만큼 복제
            limit를 넘어 결측 처리하지 않는 데이터는 학습에 포함시키지 않음.
        Args:
            datas: dask.array, np.memmap으로 불러온 데이터를 dask array화
        Return:
            datas: dask.array, 결측 처리한 dask array
            nonfill_indices: list, 결측 처리하지 않는 index (학습에도 제외할 데이터)
        """
        # NaN array의 index 찾기
        sum_0 = da.sum(datas == 0, axis=(1, 2))
        sum_0_mask = da.where(sum_0 > 0, True, False).compute()
        sum_0_indices = np.where(sum_0_mask == True)[0]
        sum_0_len = sum_0_mask.shape[0]
        
        self.log.info("Fill NaN datas using: {0} / limit frames: {1}".format(self.fill_method, self.fill_limit))
        if self.fill_method == "bfill":
            idxes = np.where(~sum_0_mask, np.arange(sum_0_len), sum_0_len-1)
            idxes = np.minimum.accumulate(idxes[::-1], axis=0)[::-1]
        elif self.fill_method == "ffill":
            idxes = np.where(~sum_0_mask, np.arange(sum_0_len), 0)
            idxes = np.maximum.accumulate(idxes, axis=0)
        else:
            # 결측 처리 하지 않은 채로 진행
            return datas, []
        
        # 결측 처리 하지 않을 index 값을 복원
        nonfill_indices = self.make_nonfill_indices(sum_0_indices)
        idxes[nonfill_indices] = nonfill_indices
        self.log.info("Unused datas: {0}".format(len(nonfill_indices)))
        
        return datas[idxes, :, :], nonfill_indices
    
    def make_nonfill_indices(self, sum_0_indices):
        """
        Description:
            결측 처리할 때 limit 범위 밖의 index 계산.
            해당 index 데이터는 결측 처리 진행하지 않음.
        Args:
            sum_0_indices: numpy.array, 결측 데이터의 index
        Return:
            nonfill_indices: list, 결측 처리하지 않는 index (학습에도 제외할 데이터)
        """
        # 연속 결측 데이터 index list 생성
        start_idx_diff = np.ediff1d(np.concatenate((sum_0_indices[:1], sum_0_indices)))
        start_idx_diff_1 = sum_0_indices[np.not_equal(start_idx_diff, 1)]
        end_idx_diff = np.ediff1d(np.concatenate((sum_0_indices, sum_0_indices[-1:])))
        end_idx_diff_1 = sum_0_indices[np.not_equal(end_idx_diff, 1)]

        continuous_indices_bundle = []
        for s_idx, e_idx in zip(start_idx_diff_1, end_idx_diff_1):
            continuous_indices_bundle.append(np.arange(s_idx, e_idx + 1))
        
        # Limit 초과인 경우에 결측처리하지 않을 index 추출
        if self.fill_method == "bfill":
            nonfill_indices_bundle = [
                elm[:-self.fill_limit]
                for elm
                in continuous_indices_bundle
                if elm.shape[0] > self.fill_limit
            ]
        elif self.fill_method == "ffill":
            nonfill_indices_bundle = [
                elm[self.fill_limit:]
                for elm
                in continuous_indices_bundle
                if elm.shape[0] > self.fill_limit
            ]
        else:
            nonfill_indices_bundle = list()
        
        # List flatten
        nonfill_indices = list(chain.from_iterable(nonfill_indices_bundle))
        return nonfill_indices
    
    def get_file_times(self, dat_names):
        """
        Description:
            학습용, 검증용 데이터의 시간대 리스트 산출(KST).
        Args:
            kinds: str, 학습/검증 구분
            dat_names: str, 데이터 파일명(KST)
        Return:
            kst_obj_list: list, dat 파일에서의 KST datetime object의 리스트
        """
        # Extract KST times
        names = [os.path.splitext(dat_name)[0] for dat_name in dat_names]
        name_splits = [name.split("_") for name in names]
        kst_start_times = [splits[-2] for splits in name_splits]
        kst_end_times = [splits[-1] for splits in name_splits]

        # Make UTC times
        total_kst_time_list = list()
        for kst_start_time, kst_end_time in zip(kst_start_times, kst_end_times):
            kst_time_list = self.sat._make_time_list(
                kst_start_time,
                kst_end_time,
                "min",
                self.time_step
            )
            total_kst_time_list.extend(kst_time_list)
        utc_time_list = [self.sat._kst_to_utc(t) for t in total_kst_time_list]
        
        # Adapt Timezone
        kst_obj_list = [
            datetime.strptime(utc_time, self.CVT_TIME_FORMAT).astimezone(self.TZ_KST)
            for utc_time
            in utc_time_list
        ]
        
        return kst_obj_list

    def make_file_exclude_seq_indices(self, dat_names, shape):
        """
        Description:
            파일이 불연속적일 경우 연속적인 Sequence를 만들 수 없는 index 산출.
        Args:
            dat_names: list, 데이터 파일명(KST)
            shape: list, 데이터 shape
        Return:
            file_exclude_seq_indices: list, dat 파일에서의 불연속적 Sequence 생성되는 index
        """
        # Extract KST times
        names = [os.path.splitext(dat_name)[0] for dat_name in dat_names]
        name_splits = [name.split("_") for name in names]
        kst_start_times = [splits[-2] for splits in name_splits]
        kst_end_times = [splits[-1] for splits in name_splits]
        
        # Find Non-continuous times from files
        accum_file_indices = np.cumsum([shp[0] for shp in shape])
        file_exclude_seq_indices = []
        for e_time, s_time, accum_idx in zip(kst_end_times[:-1],
                                             kst_start_times[1:],
                                             accum_file_indices[1:]):
            next_e_time = self.sat._compute_date(e_time, "min", self.time_step)
            if not next_e_time == s_time:
                exclude_seq_indices = list(np.arange(accum_idx - self.total_seq + 1, accum_idx))
                file_exclude_seq_indices.extend(exclude_seq_indices)
    
        return file_exclude_seq_indices

    def get_normalizer(self, normalizer_name, calibration_table):
        """
        Description:
            Normalizer 만들기.
        Args:
            normalizer_name: str, normalizer 함수명 (data_parser.py)
            calibration_table: numpy array, 채널의 Calibration table
        Return:
            normalizer: function, lambda 로 만들어진 normalizer
        """
        self.log.info("Adapt Normalize: {0}".format(normalizer_name))
        normalizer_args = get_arguments(getattr(self.cfg_norm, self.chn.upper()))
        if not calibration_table is None:
            normalizer_args = self._raw_to_calib_normalizer(normalizer_args,
                                                            calibration_table)
        normalizer = lambda x: eval(normalizer_name)(x, **normalizer_args)
        return normalizer
    
    def _raw_to_calib_normalizer(self, normalizer_args, calibration_table):
        """
        Description:
            Normalizer에서 raw 값이 아닌 Calibration table 값 사용할 때.
            VI 및 NR 채널은 raw 값과 Calibration 값의 양의 관계.
            SW, WV, IR 채널은 raw 값과 Calibration 값의 음의 관계.
            SW, WV, IR 채널에서 NaN인 경우는 가장 가까운 nanmin 값 반영.
        Args:
            normalizer_args: dict, config의 normalizer 옵션
            calibration_table: numpy array, 채널의 Calibration table
        Return:
            normalizer_args: dict, 값이 변경된 config의 normalizer 옵션
        """
        if self.chn.startswith("vi") or self.chn.startswith("nr"):
            normalizer_args = {
                k: calibration_table[v]
                for k, v
                in normalizer_args.items()
            }
        else:
            k1, k2 = normalizer_args.keys()
            normalizer_args[k1], normalizer_args[k2] = normalizer_args[k2], normalizer_args[k1]
            normalizer_args = {
                k: np.nanmin(calibration_table)
                if np.isnan(calibration_table[v])
                else calibration_table[v]
                for k, v
                in normalizer_args.items()
            }
        return normalizer_args
    
    def make_trainable_indices(self,
                               latlon,
                               train_kst_obj_list,
                               valid_kst_obj_list,
                               train_nonfill_indices,
                               valid_nonfill_indices,
                               file_exclude_seq_indices):
        """
        Description:
            학습/검증 데이터의 유효한 Sequence 시작 index 산출.
            Ray 라이브러리 사용 여부는 config에서 제어.
        Args:
            latlon: numpy array, 채널의 해상도에 맞는 위/경도
            train_kst_obj_list: list, 학습 dat 파일에서의 KST datetime object의 리스트
            valid_kst_obj_list: list, 검증 dat 파일에서의 KST datetime object의 리스트
            train_nonfill_indices: list, 학습 dat에서 결측 처리하지 않는 index (학습에도 제외할 데이터)
            valid_nonfill_indices: list, 검증 dat에서 결측 처리하지 않는 index (학습에도 제외할 데이터)
            file_exclude_seq_indices: list, dat 파일에서의 불연속적 Sequence 생성되는 index
        Return:
            train_seq_indices: list, 학습에서 사용할 sequence 생성 가능한 시간대 index (주간, 야간, 여명/황혼)
            valid_seq_indices: list, 검증에서 사용할 sequence 생성 가능한 시간대 index (주간, 야간, 여명/황혼)
        """
        self.calib.ray_start()
        
        # Make Sequence indices
        west_pixel, east_pixel = self.calib.setting_sza(latlon)
        train_seq_indices = self.calib.make_seq_indices(latlon,
                                                        west_pixel,
                                                        east_pixel,
                                                        train_kst_obj_list,
                                                        self.total_seq)
        valid_seq_indices = self.calib.make_seq_indices(latlon,
                                                        west_pixel,
                                                        east_pixel,
                                                        valid_kst_obj_list,
                                                        self.total_seq)
        
        # Make Excluding Sequence indices
        train_unused_indices = train_nonfill_indices + file_exclude_seq_indices
        valid_unused_indices = valid_nonfill_indices + file_exclude_seq_indices
        train_seq_indices = self.calib.remove_unused_indices(train_seq_indices,
                                                             train_unused_indices)
        valid_seq_indices = self.calib.remove_unused_indices(valid_seq_indices,
                                                             valid_unused_indices)
        self.log.info("Sequence init indices nums (Train): {0}".format(len(train_seq_indices)))
        self.log.info("Sequence init indices nums (Valid): {0}".format(len(valid_seq_indices)))
        
        self.calib.ray_stop()
        
        return train_seq_indices, valid_seq_indices

    def infer_prep(self,
                   infer_datas,
                   utc_time_list,
                   calibration_table, 
                   latlon):
        """
        Description:
            추론 전처리.
            추론에 사용할 입력 시계열 데이터 생성.
        Args:
            infer_datas: np.array, 모델 입력 데이터(추론)
            utc_time_list: list, 입력 데이터의 UTC 시간
            calibration_table: numpy array, 채널의 Calibration table
            latlon: numpy array, 채널의 해상도에 맞는 위/경도
        Returns:
            infer_dataloader: tf.keras.Sequence 객체, Dataloader 역할(추론)
                - Shape: (batch_size, input_seq_len, height, width, 1)
        """
        infer_datas, _ = self.fill_zero_frame(infer_datas)
        
        normalizer_name = self.cfg_norm.FUNCTION
        if normalizer_name is not None:
            normalizer = self.get_normalizer(normalizer_name, calibration_table)
        else:
            normalizer = None
        
        if latlon is None:
            infer_dataloader = GK2ADataset(
                memory_map=infer_datas,
                batch_size=self.infer_batch_size,
                in_seq=self.in_seq,
                normalizer=normalizer,
                pred_mode=self.pred_mode
            )
        else:
            infer_dataloader = GK2ADatasetV2(
                memory_map=infer_datas,
                batch_size=self.infer_batch_size,
                in_seq=self.in_seq,
                out_seq=self.out_seq,
                input_shape=self.input_shape,
                normalizer=normalizer,
                calibration_table=calibration_table,
                pred_mode=self.pred_mode
            )

        self.log.info(">>> Using Predict Data nums: {0} (Batch_size: {1})".format(len(infer_dataloader), self.infer_batch_size))
        return infer_dataloader

    def get_infer_datas(self, kst_start_time, kst_end_time, effective_area_pixel):
        """
        Description:
            추론에 사용할 데이터 불러오기.
        Args:
            kst_start_time: str, 추론할 기준 시작시점(연월일시분 12자리)
            kst_end_time: str, 추론할 기준 최종시점(연월일시분 12자리)
            effective_area_pixel: tuple, 직사각 영역 위/경도 픽셀 (위도min, 위도max, 경도min, 경도max)
        Returns:
            datas: np.array, 모델 입력 데이터(추론)
                - Shape: (Data nums, height, width)
        """
        self.log.info(">>> Load Predict datas")
        datas = self.sat(kst_start_time, kst_end_time, self.time_step)
        # Crop effective area
        if not effective_area_pixel is None:
            lat_pixel_min, lat_pixel_max, lon_pixel_min, lon_pixel_max = effective_area_pixel
            datas = datas[:, lat_pixel_min:lat_pixel_max, lon_pixel_min:lon_pixel_max]
            self.log.info("Using Effective area lat/lon pixel : {0} ~ {1}, {2} ~ {3}".format(lat_pixel_min, lat_pixel_max, lon_pixel_min, lon_pixel_max))
        self.log.info(">>> Predict data shape: {0}".format(datas.shape))
        return datas


class SatCalibration(object):
    """Calibrate Satellite Timeseries data."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            위성(GK2A) 영상 시계열 데이터 Calibration init 설정.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(SatCalibration, self).__init__()
        # Data
        cfg_d = cfg.PARAMS.DATA
        cfg_rw = cfg.RAWDATA
        self.in_seq = cfg_d.INPUT_SEQ
        self.out_seq = cfg_d.OUTPUT_SEQ

        # Calibrate
        self.TZ_KST = timezone(timedelta(hours=9))
        self.chn = cfg_d.CHANNEL
        self.use_calibration = cfg_d.CALIBRATION.USE
        self.use_cal_col = cfg_d.CALIBRATION.USE_COL
        self.calibration_dir = cfg_rw.CALIBRATION_DIR
        self.cal_table_file = cfg_rw.CAL_TABLE_FILE
        self.cal_table_dtype = getattr(np, cfg_rw.CAL_TABLE_DTYPE)
        self.cal_coeff_file = cfg_rw.CAL_COEFF_FILE
        self.raw_dtype = cfg_rw.DATA_TYPE
        self.use_sza = cfg_d.SZA.USE
        self.latlon_file = cfg_rw.LATLON_FILE
        self.h, self.w = cfg_rw.LATALON_RESOLUTION
        self.use_ray = cfg_d.SZA.USE_RAY
        self.sza_interval = cfg_d.SZA.SZA_INTERVAL
        self.west_latlon = cfg_d.SZA.WEST_LATLON
        self.east_latlon = cfg_d.SZA.EAST_LATLON
        self.day_bool = cfg_d.SZA.DAY_BOOL
        self.night_bool = cfg_d.SZA.NIGHT_BOOL
        self.dawn_twil_bool = cfg_d.SZA.DAWN_TWIL_BOOL
        self.use_crop_area = cfg_d.CROP_AREA.USE
        self.area_north_lat = cfg_d.CROP_AREA.NORTH_LAT
        self.area_south_lat = cfg_d.CROP_AREA.SOUTH_LAT
        self.area_west_lon = cfg_d.CROP_AREA.WEST_LON
        self.area_east_lon = cfg_d.CROP_AREA.EAST_LON

        self.log, self.err_log = log, err_log
        
    def __call__(self):
        """
        Description:
            None
        Args:
            None
        Returns:
            None
        """
        pass
    
    def make_calibration_table(self):
        """
        Description:
            cfg에 설정한 채널의 Calibration table.
        Args:
            None
        Returns:
            calibration_table: numpy array, 해당 채널의 Calibration table
        """
        if self.use_calibration:
            calibration_table = self.load_calibration_table()
            self.log.info(">>> Use Calibration table: {0}".format(self.use_cal_col))
        else:
            calibration_table = None
            self.log.info(">>> Use Raw data: {0}".format(self.chn))
        return calibration_table

    def ray_start(self):
        """
        Description:
            Ray 시작.
        """
        if not ray.is_initialized() and self.use_ray:
            ray.init()
    
    def ray_stop(self):
        """
        Description:
            Ray 종료.
        """
        if ray.is_initialized() and self.use_ray:
            ray.shutdown()

    def setting_sza(self, latlon):
        """
        Description:
            태양천정각 계산 준비.
        Args:
            latlon: numpy array, 채널의 해상도에 맞는 위/경도
        Returns:
            west_pixel: tuple, 구분 기준 서쪽 픽셀 (위도, 경도)
            east_pixel: tuple, 구분 기준 동쪽 픽셀 (위도, 경도)
        """
        if self.use_sza:
            west_pixel = self.get_criteria_pixel(latlon, self.west_latlon)
            east_pixel = self.get_criteria_pixel(latlon, self.east_latlon)
            return west_pixel, east_pixel
        else:
            return None, None
    
    def make_seq_indices(self,
                         latlon,
                         west_pixel,
                         east_pixel,
                         kst_obj_list,
                         total_seq):
        """
        Description:
            태양천정각 계산 및 사용할 인덱스 추출 (Sequence 반영).
        Args:
            latlon: numpy array, 채널의 해상도에 맞는 위/경도
            west_pixel: tuple, 구분 기준 서쪽 픽셀 (위도, 경도)
            east_pixel: tuple, 구분 기준 동쪽 픽셀 (위도, 경도)
            kst_obj_list: list, KST datetime object의 리스트
            day_bool: bool, 주간 시간대 결과 산출 여부
            night_bool: bool, 야간 시간대 결과 산출 여부
            dawn_twil_bool: bool, 여명/황혼 시간대 결과 산출 여부
            total_seq: int, Sequence 전체 길이 (학습은 input+output 길이, 추론은 input 길이)
        Returns:
            seq_indices: list, Sequence 시작 index
        """
        if self.use_sza:
            sza = self.make_sza(latlon, west_pixel, east_pixel, kst_obj_list)
            day_indices, night_indices, dawn_twil_indices = self.judge_indices(sza)
            use_indices = self.make_use_indices(day_indices, night_indices, dawn_twil_indices)
        else:
            use_indices = [i for i in range(len(kst_obj_list))]
        seq_indices = self.extract_seq_indices(use_indices, total_seq)
        return seq_indices

    def load_calibration_table(self):
        """
        Description:
            GK2A Calibration table 불러오기.
            가시 ~ 근적외: Radiance, Albedo
            단파적외 ~ 적외: Radiance, Brightness Temperature
        Args:
            None
        Returns:
            calibration_table: numpy array, 채널의 Calibration table
        """
        calibration_table_path = os.path.join(
            self.calibration_dir,
            self.cal_table_file
        )
        try:
            calibration_table = pd.read_csv(
                calibration_table_path,
                encoding="utf-8",
                dtype=self.cal_table_dtype
            )
        except Exception as e:
            self.log.info("Load csv file incompletely: {0}".format(self.cal_table_file))
            self.err_log.error("Load csv file incompletely: {0}".format(self.cal_table_file))
            self.err_log.error("{0}".format(e))
            raise e
        else:
            calibration_table = calibration_table[self.use_cal_col].to_numpy()
            return calibration_table
    
    def load_calibration_coeff(self):
        """
        Description:
            GK2A Calibration coefficient 불러오기.
            가시 ~ 근적외: Radiance, Albedo 관련 계산에 필요한 상수
            단파적외 ~ 적외: Radiance, Brightness Temperature 관련 계산에 필요한 상수
        Args:
            None
        Returns:
            const_cal_coeff: namedtuple, Calibration 공용 상수
            chn_cal_coeff: namedtuple, 채널의 Calibration coefficient
        """
        calibration_coeff_path = os.path.join(
            self.calibration_dir,
            self.cal_coeff_file
        )
        try:
            cal_coeff = convert(calibration_coeff_path)
        except Exception as e:
            self.log.info("Load yaml file incompletely: {0}".format(self.cal_coeff_file))
            self.err_log.error("Load yaml file incompletely: {0}".format(self.cal_coeff_file))
            self.err_log.error("{0}".format(e))
            raise e
        else:
            const_cal_coeff = cal_coeff.CONSTANT
            chn_cal_coeff = getattr(cal_coeff, self.chn.upper())
            return const_cal_coeff, chn_cal_coeff

    def load_latlon(self):
        """
        Description:
            위/경도 파일 불러오기.
        Args:
            None
        Returns:
            latlon: numpy array, 채널의 해상도에 맞는 위/경도
                - 500m: (3600, 3600, 2)
                - 1km: (1800, 1800, 2)
                - 2km: (900, 900, 2)
        """
        latlon_path = os.path.join(self.calibration_dir, self.latlon_file)
        try:
            latlon = np.loadtxt(latlon_path, delimiter="\t", dtype=float)
            latlon = latlon.reshape(self.h, self.w, -1)
        except Exception as e:
            self.log.info("Load txt file incompletely: {0}".format(self.latlon_file))
            self.err_log.error("Load txt file incompletely: {0}".format(self.latlon_file))
            self.err_log.error("{0}".format(e))
            raise e
        else:
            self.log.info(">>> Latitude & Longitude Resolution: {0}".format(latlon.shape))
            return latlon
    
    def get_criteria_pixel(self, latlon, criteria_latlon):
        """
        Description:
            태양천정각으로 주간, 야간, 여명/황혼 구분하는 기준위치의 픽셀 찾기.
            단순히 픽셀 L2 길이 최소인 픽셀로 반환.
        Args:
            latlon: numpy array, 채널의 해상도에 맞는 위/경도
            criteria_latlon: list, 구분 기준 위/경도 (위도, 경도)
        Returns:
            pixel: tuple, 구분 기준 픽셀 (위도, 경도)
        """
        criteria_ary = np.tile(criteria_latlon, reps=[self.h, self.w, 1])
        l2 = np.linalg.norm(latlon - criteria_ary, axis=-1)
        pixel = np.unravel_index(l2.argmin(), l2.shape)
        return pixel
    
    def make_sza(self, latlon, west_pixel, east_pixel, kst_obj_list):
        """
        Description:
            태양천정각(SZA) 계산.
            Ray 사용여부 flag가 있음.
        Args:
            latlon: numpy array, 채널의 해상도에 맞는 위/경도
            west_pixel: tuple, 구분 기준 서쪽 픽셀 (위도, 경도)
            east_pixel: tuple, 구분 기준 동쪽 픽셀 (위도, 경도)
            kst_obj_list: list, KST datetime object의 리스트
        Returns:
            sza: numpy array, 입력된 KST 시간에 따른 기준 픽셀 태양천정각
        """
        if self.use_ray:
            west_latlon = ray.put(latlon[west_pixel])
            east_latlon = ray.put(latlon[east_pixel])
            sza_ids = [
                # @ray.remote에는 self도 argument로 넣어줘야 함
                self.ray_make_sza_criteria.remote(self, west_latlon, east_latlon, kst_obj)
                for kst_obj
                in kst_obj_list
            ]
            sza = np.array(ray.get(sza_ids), dtype=np.float32)
        else:
            west_latlon = latlon[west_pixel]
            east_latlon = latlon[east_pixel]
            sza = np.empty((len(kst_obj_list), 2), dtype=np.float32)
            for idx, kst_obj in enumerate(kst_obj_list):
                sza[idx] = self.make_sza_criteria(west_latlon, east_latlon, kst_obj)
        return sza
    
    def compute_sza(self, lat, lon, kst_obj):
        """
        Description:
            태양천정각(SZA) = 90 - 태양고도.
        Args:
            lat: float or numpy array, 위도
            lon: float or numpy array, 경도
            kst_obj: datetime, Timezone이 KST 설정된 객체
        Returns:
            sza: float or numpy array, 태양천정각
        """
        return 90 - get_altitude(lat, lon, kst_obj)
    
    def make_sza_criteria(self, west_latlon, east_latlon, kst_obj):
        """
        Description:
            동서쪽 기준위치의 태양천정각.
        Args:
            west_latlon: numpy array, 구분 기준 서쪽 위/경도
            east_latlon: numpy array, 구분 기준 동쪽 위/경도
            kst_obj: datetime, Timezone이 KST 설정된 객체
        Returns:
            one_time_sza: numpy array, 해당 KST 시각의 태양천정각
        """
        west_sza = self.compute_sza(west_latlon[0], west_latlon[1], kst_obj)
        east_sza = self.compute_sza(east_latlon[0], east_latlon[1], kst_obj)
        return np.array([west_sza, east_sza])
    
    @ray.remote
    def ray_make_sza_criteria(self, west_latlon, east_latlon, kst_obj):
        """
        Description:
            동서쪽 기준위치의 태양천정각 (Ray 사용).
            Ray는 독립적인 함수나 Class에 적용하는 것을 권장.
        Args:
            west_latlon: numpy array, 구분 기준 서쪽 위/경도
            east_latlon: numpy array, 구분 기준 동쪽 위/경도
            kst_obj: datetime, Timezone이 KST 설정된 객체
        Returns:
            one_time_sza: list, 해당 KST 시각의 태양천정각
        """
        west_sza = self.compute_sza(west_latlon[0], west_latlon[1], kst_obj)
        east_sza = self.compute_sza(east_latlon[0], east_latlon[1], kst_obj)
        return [west_sza, east_sza]

    def judge_indices(self, sza):
        """
        Description:
            태양천정각(SZA)을 이용하여 주간, 야간, 여명/황혼 분리.
                - 주간: SZA < 80
                - 야간: SZA >= 88
                - 여명/황혼: 80 <= SZA < 88
            각각에 해당하는 index들만 추출.
            Ray 사용여부 flag가 있음.
        Args:
            sza: numpy array, 시간에 따른 기준 픽셀 태양천정각
        Returns:
            day_indices: list, 주간 시간대 index
            night_indices: list, 야간 시간대 index
            dawn_twil_indices: list, 여명/황혼 시간대 index
        """
        day_bool = sza[:, 0] < self.sza_interval[0]   # Day
        night_bool = sza[:, 1] >= self.sza_interval[1]   # Night
        dawn_twil_bool = ~np.logical_or(day_bool, night_bool)   # Dawn / Twilight
        day_indices = np.where(day_bool == True)[0]
        night_indices = np.where(night_bool == True)[0]
        dawn_twil_indices = np.where(dawn_twil_bool == True)[0]
        return day_indices, night_indices, dawn_twil_indices

    def make_use_indices(self, day_indices, night_indices, dawn_twil_indices):
        """
        Description:
            사용할 index를 순서대로 정리.
            분리한 주간, 야간, 여명/황혼에서 사용할 시간대를 합침.
        Args:
            day_indices: list, 주간 시간대 index
            night_indices: list, 야간 시간대 index
            dawn_twil_indices: list, 여명/황혼 시간대 index
        Returns:
            use_indices: list, 사용할 시간대 index
        """
        use_indices = list()
        if self.day_bool:
            use_indices.extend(day_indices)
            self.log.info("Contain Day times indices")
        if self.night_bool:
            use_indices.extend(night_indices)
            self.log.info("Contain Night times indices")
        if self.dawn_twil_bool:
            use_indices.extend(dawn_twil_indices)
            self.log.info("Contain Dawn/Twilight times indices")
        use_indices = sorted(use_indices)
        return use_indices

    def extract_seq_indices(self, idxes, total_seq):
        """
        Description:
            Sequence 생성할 수 있는 시작 index만 추출.
                - step_indices 예시
                  [[    0,     1, ...,    22,    23],
                   [    0,     1, ...,    22,    23],
                   ...,
                   [    0,     1, ...,    22,    23],
                   [    0,     1, ...,    22,    23]]
                - start_indices 예시
                  [[   50,    50, ...,    50,    50],
                   [   51,    51, ...,    51,    51],
                   ...,
                   [13216, 13216, ..., 13216, 13216],
                   [13217, 13217, ..., 13217, 13217]]
        Args:
            idxes: list, 사용할 시간대 index
            total_seq: int, Sequence 전체 길이 (학습은 input+output 길이, 추론은 input 길이)
        Returns:
            seq_indices: list, sequence 생성 가능한 시간대 index
        """
        # 차이가 1인 index 여야 연속된 Sequence 생성 가능
        idxes = np.array(idxes)
        idx_diff = idxes[1:] - idxes[:-1]
        continuous_indices = idxes[np.where(idx_diff == 1)[0]]
        
        # 차이가 1인 index 와 계산하기 위한 array 준비
        step_indices = np.tile(np.arange(total_seq),
                             reps=[continuous_indices.shape[0], 1])
        start_indices = np.tile(continuous_indices,
                              reps=[total_seq, 1]).T
        all_seq_indices = step_indices + start_indices

        # True 개수가 학습 in/out Sequence 길이가 같으면 Sequence 생성 가능
        seq_indices = list()
        for seq_idx in all_seq_indices:
            if np.isin(continuous_indices, seq_idx).sum() == total_seq:
                seq_indices.append(seq_idx[0])
        
        return seq_indices

    def make_effective_area_pixel(self, latlon):
        """
        Description:
            위/경도로 해당 해상도의 픽셀 위치 계산.
            계산한 사각형 영역을 직사각형 영역으로 최종 변경.
        Args:
            latlon: numpy array, 채널의 해상도에 맞는 위/경도
        Returns:
            effective_area_pixel: tuple, 직사각 영역 위/경도 픽셀 (위도min, 위도max, 경도min, 경도max)
        """
        if self.use_crop_area:
            # 위/경도 -> 픽셀
            nw_pixel = self._latlon_to_pixel(self.area_north_lat, self.area_west_lon, latlon)
            ne_pixel = self._latlon_to_pixel(self.area_north_lat, self.area_east_lon, latlon)
            sw_pixel = self._latlon_to_pixel(self.area_south_lat, self.area_west_lon, latlon)
            se_pixel = self._latlon_to_pixel(self.area_south_lat, self.area_east_lon, latlon)
            # 직사각형 영역 계산
            pixel_ary = np.array((nw_pixel, ne_pixel, sw_pixel, se_pixel))
            lat_pixel_min = pixel_ary[:, 0].min()
            lat_pixel_max = pixel_ary[:, 0].max()
            lon_pixel_min = pixel_ary[:, 1].min()
            lon_pixel_max = pixel_ary[:, 1].max()
            effective_area_pixel = (lat_pixel_min, lat_pixel_max, lon_pixel_min, lon_pixel_max)
        else:
            effective_area_pixel = None
        return effective_area_pixel

    def _latlon_to_pixel(self, lat, lon, latlon):
        """
        Description:
            위/경도로 해당 해상도의 픽셀 위치 계산.
            L2 norm이 가장 작은 위치 픽셀을 반환.
        Args:
            lat: float, 위도
            lon: float, 경도
            latlon: numpy array, 채널의 해상도에 맞는 위/경도
        Returns:
            latlon_pixel: tuple, 위/경도 픽셀, (위도, 경도)
        """
        latlon_tile = np.tile((lat, lon), reps=[self.h, self.w, 1])
        latlon_l2 = np.linalg.norm(latlon - latlon_tile, axis=-1)
        latlon_pixel = np.unravel_index(latlon_l2.argmin(), latlon_l2.shape)
        return latlon_pixel
    
    def remove_unused_indices(self, origin_indices, rm_indices):
        """
        Description:
            불필요 index를 제외.
        Args:
            origin_indices: list, 기준 index 리스트
            rm_indices: list, 제외할 index 리스트
        Returns:
            filtered_indices: list, 제외 처리 완료한 index 리스트
        """
        return [elm for elm in origin_indices if not elm in rm_indices]

    def convert_alb2raw(self, sat_array, chn_cal_coeff):
        """
        Description:
            GK2A Calibration radiance, albedo 결과로부터 원래 위성 raw 값 계산.
            가시 ~ 근적외 채널에 해당.
            Albedo -> Radiance -> Raw
        Args:
            sat_array: numpy array, 모델 결과 역변환까지 완료한 데이터
            chn_cal_coeff: namedtuple, 채널의 Calibration coefficient
        Returns:
            sat_array: numpy array, 위성 raw  변환 완료한 데이터
        """
        wv = chn_cal_coeff.CENTER_WAVE_LENGTH
        gain = chn_cal_coeff.DN2RAD_GAIN
        offset = chn_cal_coeff.DN2RAD_OFFSET
        rad2alb = chn_cal_coeff.RAD2ALB
        raw_max = chn_cal_coeff.RAW_MAX
        
        if self.use_cal_col.endswith("radiance"):
            sat_array = np.round((sat_array - offset) / gain)
        elif self.use_cal_col.endswith("albedo"):
            sat_array = sat_array / rad2alb
            sat_array = np.round((sat_array - offset) / gain)
        else:
            self.log.info("Cannot convert satellite data: {0}".format(self.use_cal_col))
            self.log.info("Use right postfix: radiance, albedo")
            self.err_log.error("Cannot convert satellite data: {0}".format(self.use_cal_col))
        
        # Raw와 같은 data type으로 변환
        sat_array = np.clip(sat_array, 0, raw_max)
        sat_array = sat_array.astype(getattr(np, self.raw_dtype))
        
        return sat_array
    
    def convert_bt2raw(self, sat_array, const_cal_coeff, chn_cal_coeff):
        """
        Description:
            GK2A Calibration 결과로부터 원래 위성 raw 값 계산.
            단파적외 ~ 적외 채널에 해당.
            Brightness Temperature (BT) -> Effective BT -> Radiance -> Raw
        Args:
            sat_array: numpy array, 
            const_cal_coeff: namedtuple, Calibration 공용 상수
            chn_cal_coeff: namedtuple, 채널의 Calibration coefficient
        Returns:
            sat_array: numpy array, 위성 raw  변환 완료한 데이터
        """
        c = const_cal_coeff.LIGHT_SPEED
        h = const_cal_coeff.PLANCK
        k = const_cal_coeff.BOLTZMANN
        wv = chn_cal_coeff.CENTER_WAVE_NUM
        gain = chn_cal_coeff.DN2RAD_GAIN
        offset = chn_cal_coeff.DN2RAD_OFFSET
        c0 = chn_cal_coeff.C0
        c1 = chn_cal_coeff.C1
        c2 = chn_cal_coeff.C2
        raw_max = chn_cal_coeff.RAW_MAX
        
        _hc_k = h*c/k
        _2hc2 = 2*h*(c**2)
        wv100 = wv * 100
        
        if self.use_cal_col.endswith("radiance"):
            sat_array = np.round((sat_array - offset) / gain)
        elif self.use_cal_col.endswith("bt"):
            sat_array = (-c1 + np.sqrt(c1 ** 2 - 4 * c2 * (c0 - sat_array))) / (2 * c2)
            sat_array = (_2hc2 * wv100**3) / ( (np.exp(_hc_k * wv100 / sat_array) - 1) * 10**(-5) )
            sat_array = np.round((sat_array - offset) / gain)
        else:
            self.log.info("Cannot convert satellite data: {0}".format(self.use_cal_col))
            self.log.info("Use right postfix: radiance, bt")
            self.err_log.error("Cannot convert satellite data: {0}".format(self.use_cal_col))
        
        # Raw와 같은 data type으로 변환
        sat_array = np.clip(sat_array, 0, raw_max)
        sat_array = sat_array.astype(getattr(np, self.raw_dtype))
        
        return sat_array
