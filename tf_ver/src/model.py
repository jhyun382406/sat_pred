import os
import math
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, Concatenate, BatchNormalization, Dropout
from tensorflow.keras.layers import Conv3D, Conv3DTranspose, ConvLSTM2D, Conv2D, Conv2DTranspose
from tensorflow.keras.layers import LeakyReLU, ReLU, MaxPooling3D, MaxPooling2D
from tensorflow.keras.layers import Lambda, Reshape, Permute, Cropping2D, Cropping3D, RepeatVector, GaussianNoise
from tensorflow.keras.initializers import he_uniform
from tensorflow.keras.metrics import MeanAbsoluteError, RootMeanSquaredError, LogCoshError
from tensorflow.keras.optimizers import Adam, RMSprop, SGD, Adagrad

from src.custom_loss import AmpMae, AmpMse, AmpHuber, AmpLogCosh, PixelWiseRegularizer, AdaptRegularizerLoss
from utils.code_utils import return_dir


class SatPredModel(object):
    """Satellite Timeseries Prediction Model."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            위성(GK2A) 영상 시계열 예측 모델 init 설정.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(SatPredModel, self).__init__()
        self.cfg = cfg
        self.cfg_opt = cfg.PARAMS.OPT
        self.cfg_loss = cfg.PARAMS.LOSS
        self.cfg_d = cfg.PARAMS.DATA
        self.cfg_m = cfg.PARAMS.MODEL
        self.cfg_norm = cfg.PARAMS.NORMALIZER
        self.cfg_tr = cfg.TRAIN
        self.ckpt_path = cfg.TRAIN.IMPORT_CKPT_PATH
        
        self.log, self.err_log = log, err_log
    
    def __call__(self, start_date):
        """
        Description:
            모델 생성 및 관련 설정값 저장.
        Args:
            start_date: str, 모델 생성일자
        Returns:
            model: tf.keras.model 객체
        """
        chn = self.cfg_d.CHANNEL
        model = self.make_model(chn, start_date)
        self.save_model_cfg(chn, start_date, model)
        if self.ckpt_path is not None:
            self.load_ckpt(model)
        return model

    def make_model(self, chn, start_date):
        """
        Description:
            이미지 시계열 예측 모델 생성(U-net + ConvLSTM).
        Args:
            chn: str, 위성 채널
            start_date: str, 모델 생성일자
        Return:
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        """
        self.log.info(">>> Make Model on {0} channel".format(chn.upper()))
        ipt_h, ipt_w, ipt_c = self.cfg_d.INPUT_SHAPE
        ipt_shape = (self.cfg_d.INPUT_SEQ, ipt_h, ipt_w, ipt_c)
        
        ds_filters = self.cfg_m.DS.FILTERS
        ds_ks = [(1, ks, ks) for ks in self.cfg_m.DS.KN_SIZES]
        ds_strides = [(1, st, st) for st in self.cfg_m.DS.STRIDES]
        leak_relu_alpha = self.cfg_m.DS.LEAKY_RELU_ALPHA
        use_bns = self.cfg_m.DS.USE_BNS
        use_maxps = self.cfg_m.DS.USE_MAXPS
        
        cvlstm_filters = self.cfg_m.CONVLSTM.FILTERS
        cvlstm_ks = self.cfg_m.CONVLSTM.KN_SIZES
        cvlstm_rc_drp = self.cfg_m.CONVLSTM.RC_DRP
        
        use_us_drps = self.cfg_m.US.USE_DRPS
        us_drp = self.cfg_m.US.DRP
        last_filters = self.cfg_m.US.LAST_FILTER
        last_ks = (1, self.cfg_m.US.LAST_KN_SIZE, self.cfg_m.US.LAST_KN_SIZE)
        
        # Conv3d Unet + ConvLSTM
        model = self._unet_convlstm(ipt_shape,
                                    ds_filters,
                                    ds_ks,
                                    ds_strides,
                                    leak_relu_alpha,
                                    use_bns,
                                    use_maxps,
                                    cvlstm_filters,
                                    cvlstm_ks,
                                    cvlstm_rc_drp,
                                    use_us_drps,
                                    us_drp,
                                    last_filters,
                                    last_ks,
                                    start_date,
                                    chn)
        opt = self.get_optimizer()
        loss = self.get_loss_func()
        metrics = [MeanAbsoluteError(), RootMeanSquaredError(), LogCoshError()]
        model.compile(optimizer=opt,
                      loss=loss,
                      metrics=metrics,
                      steps_per_execution=self.cfg_tr.STEPS_EXEC)
    
        return model

    def _downsample(self,
                    x,
                    filters,
                    kn_size,
                    name,
                    stride,
                    use_bn=True,
                    leak_relu_alpha=0.3,
                    use_maxp=True):
        """
        Description:
            DownSampling 3-D convolution block.
        Args:
            x: tf.tensor, input tensor.
            filters, int, Conv에 적용할 filters (hidden state)
            kn_size: tuple, Conv에 적용할 kernel size
            name: str, layer 이름에 붙일 것
            stride: tuple, Conv에 적용할 stride
            use_bn: bool, BatchNormalization 사용 여부, default=True
            leak_relu_alpha: float, Leakly ReLU에 적용할 alpha 값, default=0.3
            use_maxp: bool, Maxpooling 사용 여부, default=True
        Return:
            Downsampling block: tf.keras.Model
        """
        init = he_uniform()
        x = Conv3D(filters,
                   kn_size,
                   strides=stride,
                   padding="same",
                   kernel_initializer=init,
                   use_bias=False,
                   name="ds_conv_{0}".format(name))(x)
        if use_bn:
            x = BatchNormalization(name="ds_bn_{0}".format(name))(x)
        x = LeakyReLU(alpha=leak_relu_alpha, name="ds_act_{0}".format(name))(x)
        if use_maxp:
            x = MaxPooling3D(pool_size=kn_size,
                             strides=(1, 1, 1),
                             padding="same",
                             name="ds_maxp_{0}".format(name))(x)    
        return x
    
    def _upsample(self,
                  x,
                  filters,
                  kn_size,
                  name,
                  stride,
                  use_us_drps=True,
                  us_drp=0.5,
                  last=False):
        """
        Description:
            UpSampling 3-D convolution block.
        Args:
            x: tf.tensor, input tensor.
            filters, int, Conv에 적용할 filters (hidden state)
            kn_size: tuple, Conv에 적용할 kernel size
            name: str, layer 이름에 붙일 것
            stride: tuple, Conv에 적용할 stride
            use_us_drps: bool, Dropout 사용 여부, default=True
            us_drp: float Dropout 사용할 때 적용할 비율, default=0.5
            last: bool, 마지막 layer 확인 여부, default=False
        Return:
            Upsampling block: tf.keras.Model
        """
        init = he_uniform()
        if not last:
            x = Conv3DTranspose(filters,
                                kn_size,
                                strides=stride,
                                padding="same",
                                kernel_initializer=init,
                                use_bias=False,
                                name="us_conv_{0}".format(name))(x)
            x = BatchNormalization(name="us_bn_{0}".format(name))(x)
            if use_us_drps:
                x = Dropout(us_drp, name="us_drp_{0}".format(name))(x)
            x = ReLU(name="us_act_{0}".format(name))(x)
        else:
            x = Conv3DTranspose(filters,
                                kn_size,
                                strides=stride,
                                padding="same",
                                kernel_initializer=init,
                                activation="linear",
                                name="output")(x)    
        return x
    
    def _convlstm(self,
                  x,
                  filters,
                  kn_size,
                  name,
                  cv_lstm_rc_drp=0.5):
        """
        Description:
            2-D Convolutional LSTM block.
        Args:
            x: tf.tensor, input tensor.
            filters, int, ConvLSTM에 적용할 filters (hidden state)
            kn_size: tuple, ConvLSTM에 적용할 kernel size
            name: str, layer 이름에 붙일 것
            cv_lstm_rc_drp: tuple, ConvLSTM에 적용할 recurrent_dropout 비율, default=0.5
        Return:
            ConvLSTM2D block: tf.keras.Model
        """
        init = he_uniform()
        x = ConvLSTM2D(filters,
                       kn_size,
                       padding="same",
                       kernel_initializer=init,
                       use_bias=True,
                       recurrent_dropout=cv_lstm_rc_drp,
                       return_sequences=True,
                       name="conv_lstm_{0}".format(name))(x)
        x = BatchNormalization(name="conv_lstm_bn_{0}".format(name))(x)        
        return x

    def _unet_convlstm(self,
                       ipt_shape,
                       ds_filters,
                       ds_ks,
                       ds_strides,
                       leak_relu_alpha,
                       use_bns,
                       use_maxps,
                       cv_lstm_filters,
                       cv_lstm_ks,
                       cv_lstm_rc_drp,
                       use_us_drps,
                       us_drp,
                       last_filters,
                       last_ks,
                       start_date,
                       chn):
        """
        Description:
            U-net 3D와 ConvLSTM 2D를 조합한 모델 생성.
        Args:
            ipt_shape: tuple, 입력 데이터 shape
            ds_filters: list, Downsampling layer의 filters
            ds_ks: list, Downsampling layer의 kernel_sizes
            ds_strides: list, Downsampling layer의 strides
            leak_relu_alpha: float, Downsampling layer의 Leaky ReLU의 alpha 값
            use_bns: list, Downsampling layer의 BatchNormalization 사용여부
            use_maxps: list, Downsampling layer의 Maxpooling 사용여부
            cv_lstm_filters: list, ConvLSTM layer의 filters
            cv_lstm_ks: list, ConvLSTM layer의 kernel_sizes
            cv_lstm_rc_drp: float, ConvLSTM layer의 recurrent_dropout 비율
            use_us_drps: list, Upsampling layer의 Dropout 사용여부
            us_drp: float, Upsampling layer의 Dropout 비율
            last_filters: int, 최종 결과 features
            last_ks: tuple, 최종 layer의 kernel_size
            start_date: str, 모델 생성일자
            chn, str, 위성 채널
        Return:
            model: tf.keras.Model, Unet + ConvLSTM 모델
        """
        us_filters = list(reversed(ds_filters[:-1]))
        us_ks = list(reversed(ds_ks[:-1]))
        us_strides = list(reversed(ds_strides[1:]))
        
        # Input
        ipt = Input(shape=ipt_shape, name="input")
        x = ipt
        self.log.info("      input----------- {0}".format(x.shape))
        
        # DownSampling
        down_stack = []
        for i, (f, ks, st, bn, mxp) in enumerate(zip(ds_filters,
                                                     ds_ks,
                                                     ds_strides,
                                                     use_bns,
                                                     use_maxps)):
            x = self._downsample(x,
                                 filters=f,
                                 kn_size=ks,
                                 name="{0:02d}".format(i+1),
                                 stride=st,
                                 use_bn=bn,
                                 leak_relu_alpha=leak_relu_alpha,
                                 use_maxp=mxp)
            down_stack.append(x)
            self.log.info("      down----------- {0}".format(x.shape))            
        skips = list(reversed(down_stack[:-1]))
        
        # ConvLSTM
        for i, (f, ks) in enumerate(zip(cv_lstm_filters, cv_lstm_ks)):
            x = self._convlstm(x,
                               filters=f,
                               kn_size=ks,
                               name="{0:02d}".format(i+1),
                               cv_lstm_rc_drp=cv_lstm_rc_drp)
            self.log.info("      convlstm----------- {0}".format(x.shape))
        
        # UpSampling
        for i, (f, ks, st, d, skip) in enumerate(zip(us_filters,
                                                     us_ks,
                                                     us_strides,
                                                     use_us_drps,
                                                     skips)):
            x = self._upsample(x,
                               filters=f,
                               kn_size=ks,
                               name="{0:02d}".format(i+1),
                               stride=st,
                               use_us_drps=d,
                               us_drp=us_drp)
            x = Concatenate(name="us_concat_{0:02d}".format(i+1))([x, skip])
            self.log.info("      up_concat----------- {0}".format(x.shape))
        
        # Output
        last = self._upsample(x, filters=last_filters, kn_size=last_ks,
                              name="output",
                              stride=ds_strides[0],
                              last=True)
        self.log.info("      output----------- {0}".format(last.shape))
        
        return Model(inputs=ipt,
                     outputs=last,
                     name=self.cfg_tr.MODEL_NAME.format(chn, start_date))

    def get_optimizer(self):
        """
        Description:
            모델 학습에 사용할 Optimizer 선택.
            adam, rmsprop, sgd, adagrad 사용가능
        Args:
            None
        Returns:
            optimizer: 옵티마이저
        """
        name = self.cfg_opt.USE
        if name == "adam":
            optimizer = Adam(learning_rate=self.cfg_opt.ADAM.LR,
                            beta_1=self.cfg_opt.ADAM.BETAS[0],
                            beta_2=self.cfg_opt.ADAM.BETAS[1],
                            epsilon=float(self.cfg_opt.ADAM.EPSILON))
        elif name == "rmsprop":
            optimizer = RMSprop(learning_rate=self.cfg_opt.RMSPROP.LR,
                                rho=self.cfg_opt.RMSPROP.RHO,
                                momentum=self.cfg_opt.RMSPROP.MOMENTUM,
                                epsilon=float(self.cfg_opt.RMSPROP.EPSILON))
        elif name == "sgd":
            optimizer = SGD(learning_rate=self.cfg_opt.SGD.LR,
                            momentum=self.cfg_opt.SGD.MOMENTUM)
        elif name == "adagrad":
            optimizer = Adagrad(learning_rate=self.cfg_opt.ADAGRAD.LR,
                                initial_accumulator_value=self.cfg_opt.ADAGRAD.INIT_ACCUM,
                                epsilon=float(self.cfg_opt.ADAGRAD.EPSILON))
        else:
            log_sent = ">>> Able to use : adam, rmsprop, sgd, adagrad / Cannot use : {0}"
            self.log.info(log_sent.format(name))
            self.err_log.error(log_sent.format(name))
        return optimizer
    
    def get_loss_func(self):
        """
        Description:
            모델 학습에 사용할 Loss function 선택.
            mae, mse, logcosh, huber, pixelwise_regular 사용가능
        Args:
            None
        Returns:
            loss_func: 손실 함수
        """
        name = self.cfg_loss.USE
        if name == "mae":
            loss_func = AmpMae(amplification=self.cfg_loss.MAE.AMPLIFICATION)
        elif name == "mse":
            loss_func = AmpMse(amplification=self.cfg_loss.MSE.AMPLIFICATION)
        elif name == "huber":
            loss_func = AmpHuber(delta=self.cfg_loss.HUBER.DELTA,
                                 amplification=self.cfg_loss.HUBER.AMPLIFICATION)
        elif name == "logcosh":
            loss_func = AmpLogCosh(amplification=self.cfg_loss.LOGCOSH.AMPLIFICATION)
        elif name == "pixelwise_regular":
            loss_func = PixelWiseRegularizer(amplification=self.cfg_loss.PIXELWISE_REGULAR.AMPLIFICATION,
                                             magnitude=self.cfg_loss.PIXELWISE_REGULAR.MAGNITUDE,
                                             min_value=self.cfg_loss.PIXELWISE_REGULAR.MIN_VALUE,
                                             max_value=self.cfg_loss.PIXELWISE_REGULAR.MAX_VALUE)
        else:
            log_sent = ">>> Able to use : mae, mse, huber, logcosh, pixelwise_regular / Cannot use : {0}"
            self.log.info(log_sent.format(name))
            self.err_log.error(log_sent.format(name))
        return loss_func

    def save_model_cfg(self, chn, start_date, model):
        """
        Description:
            모델의 설정값 저장
        Args:
            chn, str, 위성 채널
            start_date: str, 모델 생성일자
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        Returns:
            None
        """
        self.log.info(">>> Save Model's Configs")
        model_name = self.cfg_tr.MODEL_NAME.format(chn, start_date)
        hist_file = "{0}.txt".format(model_name)
        hist_dir = return_dir(True, self.cfg_tr.HIST_DIR, chn)
        hist_path = os.path.join(hist_dir, hist_file)
        TRAIN_DATA_LEN = 0
        VALID_DATA_LEN = 0
        try:
            with open(hist_path, "w") as f:
                f.write("Model name: {0}\n".format(model_name))
                f.write("Used Train datas: ({0})\n".format(self.cfg_tr.DATA_TYPE))
                for f_name, f_shape in zip(self.cfg_tr.DATA_FILE, self.cfg_tr.DATA_SHAPE):
                    f.write("    File: {0} / Shape: {1}\n".format(f_name.format(chn), f_shape))
                    TRAIN_DATA_LEN += f_shape[0]
                f.write("Used Validation datas: ({0})\n".format(self.cfg_tr.DATA_TYPE))
                for f_name, f_shape in zip(self.cfg_tr.VALID_DATA_FILE, self.cfg_tr.VALID_DATA_SHAPE):
                    f.write("    File: {0} / Shape: {1}\n".format(f_name.format(chn), f_shape))
                    VALID_DATA_LEN += f_shape[0]
                TRAIN_DATA_SEQLEN = math.ceil((TRAIN_DATA_LEN - self.cfg_d.INPUT_SEQ - self.cfg_d.OUTPUT_SEQ + 1) / self.cfg_tr.BATCH_SIZE)
                VALID_DATA_SEQLEN = math.ceil((VALID_DATA_LEN - self.cfg_d.INPUT_SEQ - self.cfg_d.OUTPUT_SEQ + 1) / self.cfg_tr.BATCH_SIZE)
                f.write("Training / Validation data nums: {0} / {1}\n".format(TRAIN_DATA_LEN, VALID_DATA_LEN))
                f.write("Training / Validation sequence nums: {0} / {1}\n".format(TRAIN_DATA_SEQLEN, VALID_DATA_SEQLEN))
                f.write("Parametes: Data\n")
                self._print_cfg(self.cfg_d, f)
                f.write("Parametes: Model\n")
                self._print_cfg(self.cfg_m, f)
                f.write("Parametes: Optimizer using {0}\n".format(self.cfg_opt.USE.upper()))
                self._print_cfg(getattr(self.cfg_opt, self.cfg_opt.USE.upper()), f)
                f.write("Parametes: Loss function using {0}\n".format(self.cfg_loss.USE.upper()))
                self._print_cfg(getattr(self.cfg_loss, self.cfg_loss.USE.upper()), f)
                f.write("Parametes: Normalizer using {0}\n".format(self.cfg_norm.FUNCTION.upper()))
                self._print_cfg(getattr(self.cfg_norm, chn.upper()), f)
                f.write("Batch size: {0}\n".format(self.cfg_tr.BATCH_SIZE))
                f.write("Steps per 1 Epoch: {0}\n".format(TRAIN_DATA_SEQLEN))
                f.write("Maximum Epoch: {0}\n".format(self.cfg_tr.EPOCH))
                f.write("Early Stopping Patience: {0}\n".format(self.cfg_tr.PATIENCE))
                f.write("Steps per Execution on Model compile: {0}\n".format(self.cfg_tr.STEPS_EXEC))
                f.write("Checkpoint term: {0}\n".format(self.cfg_tr.CKPT_PERIOD))
                if self.ckpt_path is not None:
                    f.write("Import Checkpoint: {0}\n".format(self.ckpt_path.split("/")[-1]))
                model.summary(line_length=120, print_fn=lambda x: f.write(x + "\n"))   # 모델 summary
        except Exception as e:
            self.err_log.error(">>> Error on Saving Model's Parameters : {0}".format(e))
        else:
            self.log.info(">>> Save Model's Configs completely")
    
    def _print_cfg(self, cfg, f, blank=2, cnt=1):
        """
        Description:
            Namedtuple 유형의 설정값 출력
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            f: file object, 작성할 파일 객체
            subject: str, 출력 설정 중 일부만 출력하고자 하는 optimizer, normalizer
            blank: str, 앞의 공백 수
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        Returns:
            None
        """
        cnt += 1
        for field in cfg._fields:
            cfg_f = getattr(cfg, field)
            # Namedtuple 이면 depth를 더 들어감
            if isinstance(cfg_f, tuple) and hasattr(cfg_f, "_asdict") and hasattr(cfg_f, "_fields"):
                f.write("{0}{1}:\n".format(" "*blank*cnt, field))
                self._print_cfg(cfg_f, f, blank, cnt)
            else:
                f.write("{0}{1}: {2}\n".format(" "*blank*cnt, field, cfg_f))
    
    def load_ckpt(self, model):
        """
        Description:
            Checkpoint 경로가 설정되어 있으면 해당 Checkpoint 불러오기.
        Args:
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        Returns:
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델(Checkpoint 반영)
        """
        model.load_weights(self.ckpt_path)
        self.log.info(">>> Load Checkpoint from {0}".format(self.ckpt_path.split("/")[-1]))
    
    def load_model(self, model_path):
        """
        Description:
            Tensorflow 모델 불러오기.
            Custom Class 사용한 걸로 인해 일반 compile=True로 load가 안 됨.
            ==> https://www.tensorflow.org/guide/keras/save_and_serialize#registering_the_custom_object 로 custom 객체 저장방법 수정 필요
        Args:
            model_path: str, 모델 디렉토리 경로
        Returns:
            model: tf.keras.Model, Unet + ConvLSTM 모델
        """
        from tensorflow.keras.models import load_model
        model = load_model(model_path, compile=False)
        self.log.info(">>> Use model: {0}".format(model.name))
        return model


class SatPredModelV2(object):
    """Satellite Timeseries Prediction Model."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            위성(GK2A) 영상 시계열 예측 모델 init 설정.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(SatPredModelV2, self).__init__()
        self.cfg = cfg
        self.cfg_opt = cfg.PARAMS.OPT
        self.cfg_loss = cfg.PARAMS.LOSS
        self.cfg_d = cfg.PARAMS.DATA
        self.cfg_m = cfg.PARAMS.MODEL
        self.cfg_norm = cfg.PARAMS.NORMALIZER
        self.cfg_tr = cfg.TRAIN
        self.ckpt_path = cfg.TRAIN.IMPORT_CKPT_PATH
        
        self.input_seq = cfg.PARAMS.DATA.INPUT_SEQ
        self.output_seq = cfg.PARAMS.DATA.OUTPUT_SEQ
        
        self.log, self.err_log = log, err_log
    
    def __call__(self, start_date):
        """
        Description:
            모델 생성 및 관련 설정값 저장.
        Args:
            start_date: str, 모델 생성일자
        Returns:
            model: tf.keras.model 객체
        """
        chn = self.cfg_d.CHANNEL
        model = self.make_model(chn, start_date)
        self.save_model_cfg(chn, start_date, model)
        if self.ckpt_path is not None:
            self.load_ckpt(model)
        return model

    def make_model(self, chn, start_date):
        """
        Description:
            이미지 시계열 예측 모델 생성(U-net + ConvLSTM).
            입력/출력 시계열 길이 다르게 설정 가능.
            출력 시계열 길이만큼 ConvLSTM 구성 뒤 각각 Upsampling 진행 후 결합.
        Args:
            chn: str, 위성 채널
            start_date: str, 모델 생성일자
        Return:
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        """
        self.log.info(">>> Make Model on {0} channel".format(chn.upper()))
        ipt_h, ipt_w, ipt_c = self.cfg_d.INPUT_SHAPE
        ipt_shape = (self.input_seq, ipt_h, ipt_w, ipt_c)
        
        ds_filters = self.cfg_m.DS.FILTERS
        ds_ks = [(1, ks, ks) for ks in self.cfg_m.DS.KN_SIZES]
        ds_strides = [(1, st, st) for st in self.cfg_m.DS.STRIDES]
        leak_relu_alpha = self.cfg_m.DS.LEAKY_RELU_ALPHA
        use_bns = self.cfg_m.DS.USE_BNS
        use_maxps = self.cfg_m.DS.USE_MAXPS
        
        cvlstm_filters = [self.cfg_m.CONVLSTM.FILTERS for _ in range(self.output_seq)]
        _ks = self.cfg_m.CONVLSTM.KN_SIZES
        cvlstm_ks = [(_ks, _ks) for _ in range(self.output_seq)]
        cvlstm_rc_drp = self.cfg_m.CONVLSTM.RC_DRP

        use_us_drps = self.cfg_m.US.USE_DRPS
        us_drp = self.cfg_m.US.DRP
        last_filters = self.input_seq
        last_ks = (self.cfg_m.US.LAST_KN_SIZE, self.cfg_m.US.LAST_KN_SIZE)
        last_seq_filters = self.cfg_m.US.LAST_SEQ_FILTER
        last_seq_ks = (1, self.cfg_m.US.LAST_SEQ_KN_SIZE, self.cfg_m.US.LAST_SEQ_KN_SIZE)
        
        # Conv3d Unet + ConvLSTM
        model = self._unet_convlstm(ipt_shape,
                                    ds_filters,
                                    ds_ks,
                                    ds_strides,
                                    leak_relu_alpha,
                                    use_bns,
                                    use_maxps,
                                    cvlstm_filters,
                                    cvlstm_ks,
                                    cvlstm_rc_drp,
                                    use_us_drps,
                                    us_drp,
                                    last_filters,
                                    last_ks,
                                    last_seq_filters,
                                    last_seq_ks,
                                    start_date,
                                    chn)
        opt = self.get_optimizer()
        loss = self.get_loss_func()
        metrics = [MeanAbsoluteError(), RootMeanSquaredError(), LogCoshError()]
        model.compile(optimizer=opt,
                      loss=loss,
                      metrics=metrics,
                      steps_per_execution=self.cfg_tr.STEPS_EXEC)
    
        return model

    def _downsample(self,
                    x,
                    filters,
                    kn_size,
                    name,
                    stride,
                    use_bn=True,
                    leak_relu_alpha=0.3,
                    use_maxp=True):
        """
        Description:
            DownSampling 3-D convolution block.
        Args:
            x: tf.tensor, input tensor.
            filters, int, Conv에 적용할 filters (hidden state)
            kn_size: tuple, Conv에 적용할 kernel size
            name: str, layer 이름에 붙일 것
            stride: tuple, Conv에 적용할 stride
            use_bn: bool, BatchNormalization 사용 여부, default=True
            leak_relu_alpha: float, Leakly ReLU에 적용할 alpha 값, default=0.3
            use_maxp: bool, Maxpooling 사용 여부, default=True
        Return:
            Downsampling block: tf.keras.Model
        """
        init = he_uniform()
        x = Conv3D(filters,
                   kn_size,
                   strides=stride,
                   padding="same",
                   kernel_initializer=init,
                   use_bias=False,
                   name="ds_conv_{0}".format(name))(x)
        if use_bn:
            x = BatchNormalization(name="ds_bn_{0}".format(name))(x)
        x = LeakyReLU(alpha=leak_relu_alpha, name="ds_act_{0}".format(name))(x)
        if use_maxp:
            x = MaxPooling3D(pool_size=kn_size,
                             strides=(1, 1, 1),
                             padding="same",
                             name="ds_maxp_{0}".format(name))(x)    
        return x
    
    def _upsample_2d(self,
                     x,
                     filters,
                     kn_size,
                     name,
                     seq_idx,
                     stride,
                     use_us_drps=True,
                     us_drp=0.5,
                     last=False):
        """
        Description:
            UpSampling 2-D convolution block.
        Args:
            x: tf.tensor, input tensor.
            filters, int, Conv에 적용할 filters (hidden state)
            kn_size: tuple, Conv에 적용할 kernel size
            name: str, layer 이름에 붙일 것
            seq_idx: str, layer 이름에 붙일 sequence index
            stride: tuple, Conv에 적용할 stride
            use_us_drps: bool, Dropout 사용 여부, default=True
            us_drp: float Dropout 사용할 때 적용할 비율, default=0.5
            last: bool, 마지막 layer 확인 여부, default=False
        Return:
            Upsampling block: tf.keras.Model
        """
        init = he_uniform()
        if not last:
            x = Conv2DTranspose(filters,
                                kn_size,
                                strides=stride,
                                padding="same",
                                kernel_initializer=init,
                                use_bias=False,
                                name="us_conv_{0}_{1}".format(name, seq_idx))(x)
            x = BatchNormalization(name="us_bn_{0}_{1}".format(name, seq_idx))(x)
            if use_us_drps:
                x = Dropout(us_drp, name="us_drp_{0}_{1}".format(name, seq_idx))(x)
            x = ReLU(name="us_act_{0}_{1}".format(name, seq_idx))(x)
        else:
            x = Conv2DTranspose(filters,
                                kn_size,
                                strides=stride,
                                padding="same",
                                kernel_initializer=init,
                                activation="linear",
                                name="{0}_{1}".format(name, seq_idx))(x)
        return x
    
    def _convlstm(self,
                  x,
                  filters,
                  kn_size,
                  name,
                  cv_lstm_rc_drp=0.5):
        """
        Description:
            2-D Convolutional LSTM block.
        Args:
            x: tf.tensor, input tensor.
            filters, int, ConvLSTM에 적용할 filters (hidden state)
            kn_size: tuple, ConvLSTM에 적용할 kernel size
            name: str, layer 이름에 붙일 것
            cv_lstm_rc_drp: tuple, ConvLSTM에 적용할 recurrent_dropout 비율, default=0.5
        Return:
            ConvLSTM2D block: tf.keras.Model
        """
        init = he_uniform()
        x = ConvLSTM2D(filters,
                       kn_size,
                       padding="same",
                       kernel_initializer=init,
                       use_bias=True,
                       recurrent_dropout=cv_lstm_rc_drp,
                       return_sequences=True,
                       name="conv_lstm_{0}".format(name))(x)
        x = BatchNormalization(name="conv_lstm_bn_{0}".format(name))(x)        
        return x

    def _input(self, ipt_shape):
        """
        Description:
            Input layer.
        Args:
            ipt_shape: tuple, Input shape
        Return:
            ipt: KerasTensor, 입력 layer
        """
        ipt = Input(shape=ipt_shape, name="input")
        self.log.info("      input----------- {0}".format(ipt.shape))
        return ipt

    def _ds_stack(self,
                  x,
                  ds_filters,
                  ds_ks,
                  ds_strides,
                  leak_relu_alpha,
                  use_bns,
                  use_maxps):
        """
        Description:
            DownSampling 3-D convolution stacks.
            (seq_len, h, w, filters) 를 (h, w, filters * seq_len)로 변환.
        Args:
            x: KerasTensor, 입력 layer
            ds_filters: list, Downsampling layer의 filters
            ds_ks: list, Downsampling layer의 kernel_sizes
            ds_strides: list, Downsampling layer의 strides
            leak_relu_alpha: float, Downsampling layer의 Leaky ReLU의 alpha 값
            use_bns: list, Downsampling layer의 BatchNormalization 사용여부
            use_maxps: list, Downsampling layer의 Maxpooling 사용여부
        Return:
            down_stack: list, Downsampling을 적용한 부분
            skip_stack: list, Skip connection으로 사용할 부분
        """
        down_stack = []
        skip_stack = []
        for i, (f, ks, st, bn, mxp) in enumerate(zip(ds_filters,
                                                     ds_ks,
                                                     ds_strides,
                                                     use_bns,
                                                     use_maxps)):
            x = self._downsample(x,
                                 filters=f,
                                 kn_size=ks,
                                 name="{0:02d}".format(i+1),
                                 stride=st,
                                 use_bn=bn,
                                 leak_relu_alpha=leak_relu_alpha,
                                 use_maxp=mxp)
            down_stack.append(x)
            self.log.info("      down----------- {0}".format(x.shape))
            x_concat = [Lambda(lambda z: z[:, k, ...],
                               name="ds_cvt_{0:02d}_{1:02d}".format(i+1, k+1))(x)
                        for k in range(self.input_seq)]
            x_concat = Concatenate(name="skip_{0:02d}".format(i+1))(x_concat)
            if i < len(ds_filters) - 1:
                skip_stack.append(x_concat)
                self.log.info("      down_cvt----------- {0}".format(x_concat.shape))
        skip_stack = list(reversed(skip_stack))
        return down_stack, skip_stack

    def _convlstm_list(self,
                       down_stack,
                       cv_lstm_filters,
                       cv_lstm_ks,
                       cv_lstm_rc_drp):
        """
        Description:
            2-D Convolutional LSTM list.
            ConvLSTM -> ConvLSTM 은 seq 상태로 데이터 전달.
            ConvLSTM에서 Upsampling할 부분은 seq 마지막 부분만 전달.
        Args:
            down_stack: list, Downsampling을 적용한 부분
            cv_lstm_filters: list, ConvLSTM layer의 filters
            cv_lstm_ks: list, ConvLSTM layer의 kernel_sizes
            cv_lstm_rc_drp: float, ConvLSTM layer의 recurrent_dropout 비율
        Return:
            convlstm_us_list: list, 각각 seq마다 Upsampling에 사용할 부분
        """
        x = down_stack[-1]
        convlstm_us_list = []
        for i, (f, ks) in enumerate(zip(cv_lstm_filters, cv_lstm_ks)):
            x = self._convlstm(x,
                               filters=f,
                               kn_size=ks,
                               name="{0:02d}".format(i+1),
                               cv_lstm_rc_drp=cv_lstm_rc_drp)
            x_us = Lambda(lambda x: x[:, -1, ...], name="conv_lstm_tail_{0:02d}".format(i+1))(x)
            convlstm_us_list.append(x_us)
        self.log.info("      convlstm----------- {0} * {1}".format(x.shape, self.output_seq))
        self.log.info("      convlstm_us----------- {0} * {1}".format(x_us.shape, self.output_seq))
        return convlstm_us_list
    
    def _us_list(self,
                 skip_stack,
                 convlstm_us_list,
                 us_filters,
                 us_ks,
                 us_strides,
                 use_us_drps,
                 us_drp):
        """
        Description:
            UpSampling 2-D convolution list (Sequence).
            개별 seq 결과마다 Upsampling 결과 산출.
            Downsampling 쪽에서의 skip connection을 1 times 밀어가며 다음 seq에 적용.
        Args:
            skip_stack: list, Skip connection으로 사용할 부분
            convlstm_us_list: list, 각각 seq마다 Upsampling에 사용할 부분(convlstm 바로 다음)
            us_filters: list, Upsampling layer의 filters
            us_ks: list, Upsampling layer의 kernel_sizes
            us_strides: list, Upsampling layer의 strides
            use_us_drps: list, Upsampling layer의 Dropout 사용여부
            us_drp: float, Upsampling layer의 Dropout 비율
        Return:
            convlstm_us_list: list, 각각 seq마다 Upsampling에 사용할 부분(마지막 것)
        """
        # total_us_list = []
        for i, (f, ks, st, d, skip) in enumerate(zip(us_filters,
                                                     us_ks,
                                                     us_strides,
                                                     use_us_drps,
                                                     skip_stack)):
            us_list = []
            one_f = skip.shape[-1] // self.input_seq
            for j, c_us in enumerate(convlstm_us_list):
                us = self._upsample_2d(c_us,
                                       filters=f,
                                       kn_size=ks,
                                       name="{0:02d}".format(i+1),
                                       seq_idx="{0:02d}".format(j+1),
                                       stride=st,
                                       use_us_drps=d,
                                       us_drp=us_drp)
                us_list.append(us)
                # 다음 convlstm으로 보낼 t ~ t+5 와 upsampling 결과 결합
                skip = Concatenate(name="us_concat_{0:02d}_{1:02d}".format(i+1, j+1))([skip, us])
                # concat결과에서 t를 제외하고 t+1 ~ t+6만 보냄
                skip = Lambda(lambda z: z[..., one_f:],
                              name="seq_skip_{0:02d}_{1:02d}".format(i+1, j+1))(skip)
                self.log.debug("      next_skip----------- {0} : {1}".format(j, skip.shape))
            self.log.info("      up_concat----------- {0} * {1}".format(us.shape, self.output_seq))
            convlstm_us_list = us_list
            # total_us_list.append(us_list)
        return convlstm_us_list
    
    def _output(self,
                ipt,
                convlstm_us_list,
                ds_strides,
                last_filters,
                last_ks,
                last_seq_filters,
                last_seq_ks):
        """
        Description:
            Last output.
            입력도 skip connection 사용하여 개별 seq 결과마다 Upsampling 결과 산출.
            Downsampling 쪽에서의 skip connection을 1 times 밀어가며 다음 seq에 적용.
            마지막에 Convolution 이용하여 Output 형상에 맞춤.
        Args:
            ipt: KerasTensor, 입력 layer
            convlstm_us_list: list, 각각 seq마다 Upsampling에 사용할 부분(마지막 것)
            ds_strides: list, Downsampling layer의 strides
            last_filters: int, 최종 Upsampling 결과 features
            last_ks: tuple, 최종 Upsampling의 kernel_size
            last_seq_filters: int, 최종 layer 결과 features
            last_seq_ks: 최종 layer의 kernel_size
        Return:
            last: KerasTensor, 최종 layer
        """
        # Output (one scene result)
        last_skip_seq_list = []
        ipt_skip = [Lambda(lambda x: x[:, k, ...],
                           name="ipt_cvt_{0:02d}".format(k+1))(ipt)
                    for k in range(self.input_seq)]
        ipt_skip = Concatenate(name="ipt_skip")(ipt_skip)
        self.log.info("      ipt_skip----------- {0}".format(ipt_skip.shape))
        for j, us in enumerate(convlstm_us_list):
            last_us = self._upsample_2d(us,
                                        filters=last_filters,
                                        kn_size=last_ks,
                                        name="last",
                                        seq_idx="{0:02d}".format(j+1),
                                        stride=ds_strides[0][1:],   # 맨처음 DS
                                        last=True)
            # 다음 convlstm으로 보낼 t ~ t+5 와 upsampling 결과 결합
            last_skip = Concatenate(name="us_concat_last_{0:02d}".format(j+1))([ipt_skip, last_us])
            last_skip_shape = [1] + list(last_skip.shape)[1:]
            last_skip_seq = Reshape(last_skip_shape,
                                    name="us_concat_last_seq_{0:02d}".format(j+1))(last_skip)
            last_skip_seq_list.append(last_skip_seq)
            self.log.debug("      last_skip----------- {0} {1}".format(j, last_skip.shape))
            # concat결과에서 t를 제외하고 t+1 ~ t+6만 skip connection 보냄
            ipt_skip = Lambda(lambda x: x[..., last_filters:], name="ipt_seq_skip_{0:02d}".format(j+1))(last_skip)
            self.log.debug("      next_ipt_skip----------- {0} {1}".format(j, ipt_skip.shape))
        self.log.info("      last_skip----------- {0} * {1}".format(last_skip.shape, self.output_seq))

        # Output (make sequence)
        init = he_uniform()
        last = Concatenate(axis=1, name="concat_last")(last_skip_seq_list)
        last = Conv3D(last_seq_filters,
                      last_seq_ks,
                      padding="same",
                      kernel_initializer=init,
                      use_bias=False,
                      name="last")(last)
        self.log.info("      output----------- {0}".format(last.shape))
        return last

    def _unet_convlstm(self,
                       ipt_shape,
                       ds_filters,
                       ds_ks,
                       ds_strides,
                       leak_relu_alpha,
                       use_bns,
                       use_maxps,
                       cv_lstm_filters,
                       cv_lstm_ks,
                       cv_lstm_rc_drp,
                       use_us_drps,
                       us_drp,
                       last_filters,
                       last_ks,
                       last_seq_filters,
                       last_seq_ks,
                       start_date,
                       chn):
        """
        Description:
            U-net 3D와 ConvLSTM 2D를 조합한 모델 생성.
        Args:
            ipt_shape: tuple, 입력 데이터 shape
            ds_filters: list, Downsampling layer의 filters
            ds_ks: list, Downsampling layer의 kernel_sizes
            ds_strides: list, Downsampling layer의 strides
            leak_relu_alpha: float, Downsampling layer의 Leaky ReLU의 alpha 값
            use_bns: list, Downsampling layer의 BatchNormalization 사용여부
            use_maxps: list, Downsampling layer의 Maxpooling 사용여부
            cv_lstm_filters: list, ConvLSTM layer의 filters
            cv_lstm_ks: list, ConvLSTM layer의 kernel_sizes
            cv_lstm_rc_drp: float, ConvLSTM layer의 recurrent_dropout 비율
            use_us_drps: list, Upsampling layer의 Dropout 사용여부
            us_drp: float, Upsampling layer의 Dropout 비율
            last_filters: int, 최종 Upsampling 결과 features
            last_ks: tuple, 최종 Upsampling의 kernel_size
            last_seq_filters: int, 최종 layer 결과 features
            last_seq_ks: 최종 layer의 kernel_size
            start_date: str, 모델 생성일자
            chn, str, 위성 채널
        Return:
            model: tf.keras.Model, Unet + ConvLSTM 모델
        """
        us_filters = list(reversed(ds_filters[:-1]))
        us_ks = [elm[1:] for elm in reversed(ds_ks[:-1])]
        us_strides = [elm[1:] for elm in reversed(ds_strides[1:])]
        
        # Input
        ipt = self._input(ipt_shape)
        # DownSampling
        down_stack, skip_stack = self._ds_stack(ipt,
                                                ds_filters,
                                                ds_ks,
                                                ds_strides,
                                                leak_relu_alpha,
                                                use_bns,
                                                use_maxps)
        # ConvLSTM
        convlstm_us_list = self._convlstm_list(down_stack,
                                               cv_lstm_filters,
                                               cv_lstm_ks,
                                               cv_lstm_rc_drp)
        # UpSampling
        convlstm_us_list = self._us_list(skip_stack,
                                         convlstm_us_list,
                                         us_filters,
                                         us_ks,
                                         us_strides,
                                         use_us_drps,
                                         us_drp)
        # Output
        last = self._output(ipt,
                            convlstm_us_list,
                            ds_strides,
                            last_filters,
                            last_ks,
                            last_seq_filters,
                            last_seq_ks)
        
        return Model(inputs=ipt,
                     outputs=last,
                     name=self.cfg_tr.MODEL_NAME.format(chn, start_date))

    def get_optimizer(self):
        """
        Description:
            모델 학습에 사용할 Optimizer 선택.
            adam, rmsprop, sgd, adagrad 사용가능
        Args:
            None
        Returns:
            optimizer: 옵티마이저
        """
        name = self.cfg_opt.USE
        if name == "adam":
            optimizer = Adam(learning_rate=self.cfg_opt.ADAM.LR,
                            beta_1=self.cfg_opt.ADAM.BETAS[0],
                            beta_2=self.cfg_opt.ADAM.BETAS[1],
                            epsilon=float(self.cfg_opt.ADAM.EPSILON))
        elif name == "rmsprop":
            optimizer = RMSprop(learning_rate=self.cfg_opt.RMSPROP.LR,
                                rho=self.cfg_opt.RMSPROP.RHO,
                                momentum=self.cfg_opt.RMSPROP.MOMENTUM,
                                epsilon=float(self.cfg_opt.RMSPROP.EPSILON))
        elif name == "sgd":
            optimizer = SGD(learning_rate=self.cfg_opt.SGD.LR,
                            momentum=self.cfg_opt.SGD.MOMENTUM)
        elif name == "adagrad":
            optimizer = Adagrad(learning_rate=self.cfg_opt.ADAGRAD.LR,
                                initial_accumulator_value=self.cfg_opt.ADAGRAD.INIT_ACCUM,
                                epsilon=float(self.cfg_opt.ADAGRAD.EPSILON))
        else:
            log_sent = ">>> Able to use : adam, rmsprop, sgd, adagrad / Cannot use : {0}"
            self.log.info(log_sent.format(name))
            self.err_log.error(log_sent.format(name))
        return optimizer
    
    def get_loss_func(self):
        """
        Description:
            모델 학습에 사용할 Loss function 선택.
            mae, mse, logcosh, huber, pixelwise_regular 사용가능
        Args:
            None
        Returns:
            loss_func: 손실 함수
        """
        name = self.cfg_loss.USE
        if name == "mae":
            loss_func = AmpMae(amplification=self.cfg_loss.MAE.AMPLIFICATION)
        elif name == "mse":
            loss_func = AmpMse(amplification=self.cfg_loss.MSE.AMPLIFICATION)
        elif name == "huber":
            loss_func = AmpHuber(delta=self.cfg_loss.HUBER.DELTA,
                                 amplification=self.cfg_loss.HUBER.AMPLIFICATION)
        elif name == "logcosh":
            loss_func = AmpLogCosh(amplification=self.cfg_loss.LOGCOSH.AMPLIFICATION)
        elif name == "pixelwise_regular":
            loss_func = PixelWiseRegularizer(amplification=self.cfg_loss.PIXELWISE_REGULAR.AMPLIFICATION,
                                             magnitude=self.cfg_loss.PIXELWISE_REGULAR.MAGNITUDE,
                                             min_value=self.cfg_loss.PIXELWISE_REGULAR.MIN_VALUE,
                                             max_value=self.cfg_loss.PIXELWISE_REGULAR.MAX_VALUE)
        else:
            log_sent = ">>> Able to use : mae, mse, huber, logcosh, pixelwise_regular / Cannot use : {0}"
            self.log.info(log_sent.format(name))
            self.err_log.error(log_sent.format(name))
        return loss_func

    def save_model_cfg(self, chn, start_date, model):
        """
        Description:
            모델의 설정값 저장
        Args:
            chn, str, 위성 채널
            start_date: str, 모델 생성일자
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        Returns:
            None
        """
        self.log.info(">>> Save Model's Configs")
        model_name = self.cfg_tr.MODEL_NAME.format(chn, start_date)
        hist_file = "{0}.txt".format(model_name)
        hist_dir = return_dir(True, self.cfg_tr.HIST_DIR, chn)
        hist_path = os.path.join(hist_dir, hist_file)
        TRAIN_DATA_LEN = 0
        VALID_DATA_LEN = 0
        try:
            with open(hist_path, "w") as f:
                f.write("Model name: {0}\n".format(model_name))
                f.write("Used Train datas: ({0})\n".format(self.cfg_tr.DATA_TYPE))
                for f_name, f_shape in zip(self.cfg_tr.DATA_FILE, self.cfg_tr.DATA_SHAPE):
                    f.write("    File: {0} / Shape: {1}\n".format(f_name.format(chn), f_shape))
                    TRAIN_DATA_LEN += f_shape[0]
                f.write("Used Validation datas: ({0})\n".format(self.cfg_tr.DATA_TYPE))
                for f_name, f_shape in zip(self.cfg_tr.VALID_DATA_FILE, self.cfg_tr.VALID_DATA_SHAPE):
                    f.write("    File: {0} / Shape: {1}\n".format(f_name.format(chn), f_shape))
                    VALID_DATA_LEN += f_shape[0]
                TRAIN_DATA_SEQLEN = math.ceil((TRAIN_DATA_LEN - self.cfg_d.INPUT_SEQ - self.cfg_d.OUTPUT_SEQ + 1) / self.cfg_tr.BATCH_SIZE)
                VALID_DATA_SEQLEN = math.ceil((VALID_DATA_LEN - self.cfg_d.INPUT_SEQ - self.cfg_d.OUTPUT_SEQ + 1) / self.cfg_tr.BATCH_SIZE)
                f.write("Training / Validation data nums: {0} / {1}\n".format(TRAIN_DATA_LEN, VALID_DATA_LEN))
                f.write("Training / Validation sequence nums: {0} / {1}\n".format(TRAIN_DATA_SEQLEN, VALID_DATA_SEQLEN))
                f.write("Parametes: Data\n")
                self._print_cfg(self.cfg_d, f)
                f.write("Parametes: Model\n")
                self._print_cfg(self.cfg_m, f)
                f.write("Parametes: Optimizer using {0}\n".format(self.cfg_opt.USE.upper()))
                self._print_cfg(getattr(self.cfg_opt, self.cfg_opt.USE.upper()), f)
                f.write("Parametes: Loss function using {0}\n".format(self.cfg_loss.USE.upper()))
                self._print_cfg(getattr(self.cfg_loss, self.cfg_loss.USE.upper()), f)
                f.write("Parametes: Normalizer using {0}\n".format(self.cfg_norm.FUNCTION.upper()))
                self._print_cfg(getattr(self.cfg_norm, chn.upper()), f)
                f.write("Batch size: {0}\n".format(self.cfg_tr.BATCH_SIZE))
                f.write("Steps per 1 Epoch: {0}\n".format(TRAIN_DATA_SEQLEN))
                f.write("Maximum Epoch: {0}\n".format(self.cfg_tr.EPOCH))
                f.write("Early Stopping Patience: {0}\n".format(self.cfg_tr.PATIENCE))
                f.write("Steps per Execution on Model compile: {0}\n".format(self.cfg_tr.STEPS_EXEC))
                f.write("Checkpoint term: {0}\n".format(self.cfg_tr.CKPT_PERIOD))
                if self.ckpt_path is not None:
                    f.write("Import Checkpoint: {0}\n".format(self.ckpt_path.split("/")[-1]))
                model.summary(line_length=120, print_fn=lambda x: f.write(x + "\n"))   # 모델 summary
        except Exception as e:
            self.err_log.error(">>> Error on Saving Model's Parameters : {0}".format(e))
        else:
            self.log.info(">>> Save Model's Configs completely")
    
    def _print_cfg(self, cfg, f, blank=2, cnt=1):
        """
        Description:
            Namedtuple 유형의 설정값 출력
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            f: file object, 작성할 파일 객체
            subject: str, 출력 설정 중 일부만 출력하고자 하는 optimizer, normalizer
            blank: str, 앞의 공백 수
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        Returns:
            None
        """
        cnt += 1
        for field in cfg._fields:
            cfg_f = getattr(cfg, field)
            # Namedtuple 이면 depth를 더 들어감
            if isinstance(cfg_f, tuple) and hasattr(cfg_f, "_asdict") and hasattr(cfg_f, "_fields"):
                f.write("{0}{1}:\n".format(" "*blank*cnt, field))
                self._print_cfg(cfg_f, f, blank, cnt)
            else:
                f.write("{0}{1}: {2}\n".format(" "*blank*cnt, field, cfg_f))
    
    def load_ckpt(self, model):
        """
        Description:
            Checkpoint 경로가 설정되어 있으면 해당 Checkpoint 불러오기.
        Args:
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        Returns:
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델(Checkpoint 반영)
        """
        model.load_weights(self.ckpt_path)
        self.log.info(">>> Load Checkpoint from {0}".format(self.ckpt_path.split("/")[-1]))
    
    def load_model(self, model_path):
        """
        Description:
            Tensorflow 모델 불러오기.
            Custom Class 사용한 걸로 인해 일반 compile=True로 load가 안 됨.
            ==> https://www.tensorflow.org/guide/keras/save_and_serialize#registering_the_custom_object 로 custom 객체 저장방법 수정 필요
        Args:
            model_path: str, 모델 디렉토리 경로
        Returns:
            model: tf.keras.Model, Unet + ConvLSTM 모델
        """
        from tensorflow.keras.models import load_model
        model = load_model(model_path, compile=False)
        self.log.info(">>> Use model: {0}".format(model.name))
        return model


class SatPredModelV3(object):
    """Satellite Timeseries Prediction Model."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            위성(GK2A) 영상 시계열 예측 모델 init 설정.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(SatPredModelV3, self).__init__()
        self.cfg = cfg
        self.cfg_opt = cfg.PARAMS.OPT
        self.cfg_loss = cfg.PARAMS.LOSS
        self.cfg_d = cfg.PARAMS.DATA
        self.cfg_m = cfg.PARAMS.MODEL
        self.cfg_norm = cfg.PARAMS.NORMALIZER
        self.cfg_tr = cfg.TRAIN
        self.ckpt_path = cfg.TRAIN.IMPORT_CKPT_PATH
        
        self.input_seq = cfg.PARAMS.DATA.INPUT_SEQ
        self.output_seq = cfg.PARAMS.DATA.OUTPUT_SEQ
        
        self.log, self.err_log = log, err_log
    
    def __call__(self, start_date):
        """
        Description:
            모델 생성 및 관련 설정값 저장.
        Args:
            start_date: str, 모델 생성일자
        Returns:
            model: tf.keras.model 객체
        """
        chn = self.cfg_d.CHANNEL
        model = self.make_model(chn, start_date)
        self.save_model_cfg(chn, start_date, model)
        if self.ckpt_path is not None:
            self.load_ckpt(model)
        return model

    def make_model(self, chn, start_date):
        """
        Description:
            이미지 시계열 예측 모델 생성(U-net + ConvLSTM).
            입력/출력 시계열 길이 다르게 설정 가능.
            출력 시계열 길이만큼 ConvLSTM 구성 뒤 각각 Upsampling 진행 후 결합.
            V2에서 _us_list에 skip connection 연결점이 끊어져 연결점 추가.
        Args:
            chn: str, 위성 채널
            start_date: str, 모델 생성일자
        Return:
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        """
        self.log.info(">>> Make Model on {0} channel".format(chn.upper()))
        ipt_h, ipt_w, ipt_c = self.cfg_d.INPUT_SHAPE
        ipt_shape = (self.input_seq, ipt_h, ipt_w, ipt_c)
        
        ds_filters = self.cfg_m.DS.FILTERS
        ds_ks = [(1, ks, ks) for ks in self.cfg_m.DS.KN_SIZES]
        ds_strides = [(1, st, st) for st in self.cfg_m.DS.STRIDES]
        leak_relu_alpha = self.cfg_m.DS.LEAKY_RELU_ALPHA
        use_bns = self.cfg_m.DS.USE_BNS
        use_maxps = self.cfg_m.DS.USE_MAXPS
        
        cvlstm_filters = [self.cfg_m.CONVLSTM.FILTERS for _ in range(self.output_seq)]
        _ks = self.cfg_m.CONVLSTM.KN_SIZES
        cvlstm_ks = [(_ks, _ks) for _ in range(self.output_seq)]
        cvlstm_rc_drp = self.cfg_m.CONVLSTM.RC_DRP

        use_us_drps = self.cfg_m.US.USE_DRPS
        us_drp = self.cfg_m.US.DRP
        last_filters = self.cfg_m.US.LAST_SEQ_FILTER
        last_ks = (self.cfg_m.US.LAST_KN_SIZE, self.cfg_m.US.LAST_KN_SIZE)
        last_seq_filters = self.cfg_m.US.LAST_SEQ_FILTER
        last_seq_ks = (1, self.cfg_m.US.LAST_SEQ_KN_SIZE, self.cfg_m.US.LAST_SEQ_KN_SIZE)
        
        # Conv3d Unet + ConvLSTM
        model = self._unet_convlstm(ipt_shape,
                                    ds_filters,
                                    ds_ks,
                                    ds_strides,
                                    leak_relu_alpha,
                                    use_bns,
                                    use_maxps,
                                    cvlstm_filters,
                                    cvlstm_ks,
                                    cvlstm_rc_drp,
                                    use_us_drps,
                                    us_drp,
                                    last_filters,
                                    last_ks,
                                    last_seq_filters,
                                    last_seq_ks,
                                    start_date,
                                    chn)
        opt = self.get_optimizer()
        loss = self.get_loss_func()
        metrics = [MeanAbsoluteError(), RootMeanSquaredError(), LogCoshError()]
        model.compile(optimizer=opt,
                      loss=loss,
                      metrics=metrics,
                      steps_per_execution=self.cfg_tr.STEPS_EXEC)
    
        return model

    def _downsample(self,
                    x,
                    filters,
                    kn_size,
                    name,
                    stride,
                    use_bn=True,
                    leak_relu_alpha=0.3,
                    use_maxp=True):
        """
        Description:
            DownSampling 3-D convolution block.
        Args:
            x: tf.tensor, input tensor.
            filters, int, Conv에 적용할 filters (hidden state)
            kn_size: tuple, Conv에 적용할 kernel size
            name: str, layer 이름에 붙일 것
            stride: tuple, Conv에 적용할 stride
            use_bn: bool, BatchNormalization 사용 여부, default=True
            leak_relu_alpha: float, Leakly ReLU에 적용할 alpha 값, default=0.3
            use_maxp: bool, Maxpooling 사용 여부, default=True
        Return:
            Downsampling block: tf.keras.Model
        """
        init = he_uniform()
        x = Conv3D(filters,
                   kn_size,
                   strides=stride,
                   padding="same",
                   kernel_initializer=init,
                   use_bias=False,
                   name="ds_conv_{0}".format(name))(x)
        if use_bn:
            x = BatchNormalization(name="ds_bn_{0}".format(name))(x)
        x = LeakyReLU(alpha=leak_relu_alpha, name="ds_act_{0}".format(name))(x)
        if use_maxp:
            x = MaxPooling3D(pool_size=kn_size,
                             strides=(1, 1, 1),
                             padding="same",
                             name="ds_maxp_{0}".format(name))(x)    
        return x
    
    def _upsample_2d(self,
                     x,
                     filters,
                     kn_size,
                     name,
                     seq_idx,
                     stride,
                     use_us_drps=True,
                     us_drp=0.5,
                     last=False):
        """
        Description:
            UpSampling 2-D convolution block.
        Args:
            x: tf.tensor, input tensor.
            filters, int, Conv에 적용할 filters (hidden state)
            kn_size: tuple, Conv에 적용할 kernel size
            name: str, layer 이름에 붙일 것
            seq_idx: str, layer 이름에 붙일 sequence index
            stride: tuple, Conv에 적용할 stride
            use_us_drps: bool, Dropout 사용 여부, default=True
            us_drp: float Dropout 사용할 때 적용할 비율, default=0.5
            last: bool, 마지막 layer 확인 여부, default=False
        Return:
            Upsampling block: tf.keras.Model
        """
        init = he_uniform()
        if not last:
            x = Conv2DTranspose(filters,
                                kn_size,
                                strides=stride,
                                padding="same",
                                kernel_initializer=init,
                                use_bias=False,
                                name="us_conv_{0}_{1}".format(name, seq_idx))(x)
            x = BatchNormalization(name="us_bn_{0}_{1}".format(name, seq_idx))(x)
            if use_us_drps:
                x = Dropout(us_drp, name="us_drp_{0}_{1}".format(name, seq_idx))(x)
            x = ReLU(name="us_act_{0}_{1}".format(name, seq_idx))(x)
        else:
            x = Conv2DTranspose(filters,
                                kn_size,
                                strides=stride,
                                padding="same",
                                kernel_initializer=init,
                                activation="linear",
                                name="{0}_{1}".format(name, seq_idx))(x)
        return x
    
    def _convlstm(self,
                  x,
                  filters,
                  kn_size,
                  name,
                  cv_lstm_rc_drp=0.5):
        """
        Description:
            2-D Convolutional LSTM block.
        Args:
            x: tf.tensor, input tensor.
            filters, int, ConvLSTM에 적용할 filters (hidden state)
            kn_size: tuple, ConvLSTM에 적용할 kernel size
            name: str, layer 이름에 붙일 것
            cv_lstm_rc_drp: tuple, ConvLSTM에 적용할 recurrent_dropout 비율, default=0.5
        Return:
            ConvLSTM2D block: tf.keras.Model
        """
        init = he_uniform()
        x = ConvLSTM2D(filters,
                       kn_size,
                       padding="same",
                       kernel_initializer=init,
                       use_bias=True,
                       recurrent_dropout=cv_lstm_rc_drp,
                       return_sequences=True,
                       name="conv_lstm_{0}".format(name))(x)
        x = BatchNormalization(name="conv_lstm_bn_{0}".format(name))(x)        
        return x

    def _input(self, ipt_shape):
        """
        Description:
            Input layer.
        Args:
            ipt_shape: tuple, Input shape
        Return:
            ipt: KerasTensor, 입력 layer
        """
        ipt = Input(shape=ipt_shape, name="input")
        self.log.info("      input----------- {0}".format(ipt.shape))
        return ipt

    def _ds_stack(self,
                  x,
                  ds_filters,
                  ds_ks,
                  ds_strides,
                  leak_relu_alpha,
                  use_bns,
                  use_maxps):
        """
        Description:
            DownSampling 3-D convolution stacks.
            (seq_len, h, w, filters) 를 (h, w, filters * seq_len)로 변환.
        Args:
            x: KerasTensor, 입력 layer
            ds_filters: list, Downsampling layer의 filters
            ds_ks: list, Downsampling layer의 kernel_sizes
            ds_strides: list, Downsampling layer의 strides
            leak_relu_alpha: float, Downsampling layer의 Leaky ReLU의 alpha 값
            use_bns: list, Downsampling layer의 BatchNormalization 사용여부
            use_maxps: list, Downsampling layer의 Maxpooling 사용여부
        Return:
            down_stack: list, Downsampling을 적용한 부분
            skip_stack: list, Skip connection으로 사용할 부분
        """
        down_stack = []
        skip_stack = []
        for i, (f, ks, st, bn, mxp) in enumerate(zip(ds_filters,
                                                     ds_ks,
                                                     ds_strides,
                                                     use_bns,
                                                     use_maxps)):
            x = self._downsample(x,
                                 filters=f,
                                 kn_size=ks,
                                 name="{0:02d}".format(i+1),
                                 stride=st,
                                 use_bn=bn,
                                 leak_relu_alpha=leak_relu_alpha,
                                 use_maxp=mxp)
            down_stack.append(x)
            self.log.info("      down----------- {0}".format(x.shape))
            x_concat = [Lambda(lambda z: z[:, k, ...],
                               name="ds_cvt_{0:02d}_{1:02d}".format(i+1, k+1))(x)
                        for k in range(self.input_seq)]
            x_concat = Concatenate(name="skip_{0:02d}".format(i+1))(x_concat)
            if i < len(ds_filters) - 1:
                skip_stack.append(x_concat)
                self.log.info("      down_cvt----------- {0}".format(x_concat.shape))
        skip_stack = list(reversed(skip_stack))
        return down_stack, skip_stack

    def _convlstm_list(self,
                       down_stack,
                       cv_lstm_filters,
                       cv_lstm_ks,
                       cv_lstm_rc_drp):
        """
        Description:
            2-D Convolutional LSTM list.
            ConvLSTM -> ConvLSTM 은 seq 상태로 데이터 전달.
            ConvLSTM에서 Upsampling할 부분은 seq 마지막 부분만 전달.
        Args:
            down_stack: list, Downsampling을 적용한 부분
            cv_lstm_filters: list, ConvLSTM layer의 filters
            cv_lstm_ks: list, ConvLSTM layer의 kernel_sizes
            cv_lstm_rc_drp: float, ConvLSTM layer의 recurrent_dropout 비율
        Return:
            convlstm_us_list: list, 각각 seq마다 Upsampling에 사용할 부분
        """
        x = down_stack[-1]
        convlstm_us_list = []
        for i, (f, ks) in enumerate(zip(cv_lstm_filters, cv_lstm_ks)):
            x = self._convlstm(x,
                               filters=f,
                               kn_size=ks,
                               name="{0:02d}".format(i+1),
                               cv_lstm_rc_drp=cv_lstm_rc_drp)
            x_us = Lambda(lambda x: x[:, -1, ...], name="conv_lstm_tail_{0:02d}".format(i+1))(x)
            convlstm_us_list.append(x_us)
        self.log.info("      convlstm----------- {0} * {1}".format(x.shape, self.output_seq))
        self.log.info("      convlstm_us----------- {0} * {1}".format(x_us.shape, self.output_seq))
        return convlstm_us_list
    
    def _next_us_convlstm(self,
                          us,
                          skip,
                          us_filter,
                          us_ks,
                          leak_relu_alpha,
                          use_bn,
                          use_maxp,
                          i,
                          j):
        """
        Description:
            Next ConvLSTM result upsampling.
            이전 skip connection 반영하여 개별 seq 결과마다 Upsampling 결과 산출.
            Downsampling 쪽에서의 skip connection을 1 times 밀어가며 다음 seq에 적용.
        Args:
            us: KerasTensor, Upsampling 결과
            skip: KerasTensor, Downsampling에서 전달된 skip connection
            us_filter: int, Upsampling layer의 filter
            us_ks: int, Upsampling layer의 kernel_size
            leak_relu_alpha: float, Downsampling layer의 Leaky ReLU의 alpha 값 동일하게 사용
            use_bns: bool, Upsampling의 BatchNormalization 사용여부
            use_maxps: bool, Upsampling의 Maxpooling 사용여부
            i: int, Upsampling level index
            j: int, ConvLSTM index
        Return:
            next_us: KerasTensor, Skip Connection 반영된 Upsampling 결과 (다음 Upsampling에 전달)
            next_skip: KerasTensor, 1 times 씩 밀린 skip connection (다음 ConvLSTM에 전달)
        """
        init = he_uniform()
        # 다음 convlstm으로 보낼 t ~ t+5 (skip) 와 upsampling 결과 결합
        concat = Concatenate(name="us_concat_{0:02d}_{1:02d}".format(i+1, j+1))([skip, us])
        next_us = Conv2D(us_filter,
                         us_ks,
                         padding="same",
                         kernel_initializer=init,
                         use_bias=False,
                         name="next_us_{0:02d}_{1:02d}".format(i+1, j+1))(concat)
        if use_bn:
            next_us = BatchNormalization(name="next_us_bn_{0:02d}_{1:02d}".format(i+1, j+1))(next_us)
        next_us = LeakyReLU(alpha=leak_relu_alpha,
                            name="next_us_act_{0:02d}_{1:02d}".format(i+1, j+1))(next_us)
        if use_maxp:
            next_us = MaxPooling2D(pool_size=us_ks,
                                   strides=(1, 1),
                                   padding="same",
                                   name="next_us_maxp_{0:02d}_{1:02d}".format(i+1, j+1))(next_us)
        # concat결과에서 t를 제외하고 t+1 ~ t+6만 새로운 skip으로 보냄
        next_skip = Lambda(lambda z: z[..., us_filter:],
                           name="next_skip_{0:02d}_{1:02d}".format(i+1, j+1))(concat)
        return next_us, next_skip
    
    def _us_list(self,
                 skip_stack,
                 convlstm_us_list,
                 us_filters,
                 us_ks,
                 us_strides,
                 use_us_drps,
                 us_drp,
                 leak_relu_alpha,
                 us_use_bns,
                 us_use_maxps):
        """
        Description:
            UpSampling 2-D convolution list (Sequence).
            개별 seq 결과마다 Upsampling 결과 산출.
            Downsampling 쪽에서의 skip connection을 1 times 밀어가며 다음 seq에 적용.
        Args:
            skip_stack: list, Skip connection으로 사용할 부분
            convlstm_us_list: list, 각각 seq마다 Upsampling에 사용할 부분(convlstm 바로 다음)
            us_filters: list, Upsampling layer의 filters
            us_ks: list, Upsampling layer의 kernel_sizes
            us_strides: list, Upsampling layer의 strides
            use_us_drps: list, Upsampling layer의 Dropout 사용여부
            us_drp: float, Upsampling layer의 Dropout 비율
            leak_relu_alpha: float, Downsampling layer의 Leaky ReLU의 alpha 값 동일하게 사용
            us_use_bns: list, ConvLSTM Upsampling의 BatchNormalization 사용여부
            us_use_maxps: list, ConvLSTM Upsampling의 Maxpooling 사용여부
        Return:
            convlstm_us_list: list, 각각 seq마다 Upsampling에 사용할 부분(마지막 것)
        """
        for i, (f, ks, st, d, skip, bn, maxp) in enumerate(zip(us_filters,
                                                               us_ks,
                                                               us_strides,
                                                               use_us_drps,
                                                               skip_stack,
                                                               us_use_bns,
                                                               us_use_maxps)):
            us_list = []
            for j, c_us in enumerate(convlstm_us_list):
                us = self._upsample_2d(c_us,
                                       filters=f,
                                       kn_size=ks,
                                       name="{0:02d}".format(i+1),
                                       seq_idx="{0:02d}".format(j+1),
                                       stride=st,
                                       use_us_drps=d,
                                       us_drp=us_drp)
                # 다음 convlstm 및 us 로 보낼 것 계산
                us, skip = self._next_us_convlstm(us,
                                                  skip,
                                                  f,
                                                  ks,
                                                  leak_relu_alpha,
                                                  bn,
                                                  maxp,
                                                  i,
                                                  j)
                us_list.append(us)
                self.log.debug("      next_skip----------- {0} : {1}".format(j, skip.shape))
            self.log.info("      up_concat----------- {0} * {1}".format(us.shape, self.output_seq))
            convlstm_us_list = us_list
        return convlstm_us_list
    
    def _output(self,
                ipt,
                convlstm_us_list,
                ds_strides,
                last_filters,
                last_ks,
                last_seq_filters,
                last_seq_ks):
        """
        Description:
            Last output.
            입력도 skip connection 사용하여 개별 seq 결과마다 Upsampling 결과 산출.
            Downsampling 쪽에서의 skip connection을 1 times 밀어가며 다음 seq에 적용.
            마지막에 Convolution 이용하여 Output 형상에 맞춤.
        Args:
            ipt: KerasTensor, 입력 layer
            convlstm_us_list: list, 각각 seq마다 Upsampling에 사용할 부분(마지막 것)
            ds_strides: list, Downsampling layer의 strides
            last_filters: int, 최종 Upsampling 결과 features
            last_ks: tuple, 최종 Upsampling의 kernel_size
            last_seq_filters: int, 최종 layer 결과 features
            last_seq_ks: 최종 layer의 kernel_size
        Return:
            last: KerasTensor, 최종 layer
        """
        # Output (one scene result)
        last_skip_seq_list = []
        ipt_skip = [Lambda(lambda x: x[:, k, ...],
                           name="ipt_cvt_{0:02d}".format(k+1))(ipt)
                    for k in range(self.input_seq)]
        ipt_skip = Concatenate(name="ipt_skip")(ipt_skip)
        self.log.info("      ipt_skip----------- {0}".format(ipt_skip.shape))
        for j, us in enumerate(convlstm_us_list):
            last_us = self._upsample_2d(us,
                                        filters=last_filters,
                                        kn_size=last_ks,
                                        name="last",
                                        seq_idx="{0:02d}".format(j+1),
                                        stride=ds_strides[0][1:],   # 맨처음 DS
                                        last=True)
            # 다음 convlstm으로 보낼 t ~ t+5 와 upsampling 결과 결합
            last_skip = Concatenate(name="us_concat_last_{0:02d}".format(j+1))([ipt_skip, last_us])
            last_skip_shape = [1] + list(last_skip.shape)[1:]
            last_skip_seq = Reshape(last_skip_shape,
                                    name="us_concat_last_seq_{0:02d}".format(j+1))(last_skip)
            last_skip_seq_list.append(last_skip_seq)
            self.log.debug("      last_skip----------- {0} {1}".format(j, last_skip.shape))
            # concat결과에서 t를 제외하고 t+1 ~ t+6만 skip connection 보냄
            ipt_skip = Lambda(lambda x: x[..., last_filters:], name="ipt_seq_skip_{0:02d}".format(j+1))(last_skip)
            self.log.debug("      next_ipt_skip----------- {0} {1}".format(j, ipt_skip.shape))
        self.log.info("      last_skip----------- {0} * {1}".format(last_skip.shape, self.output_seq))

        # Output (make sequence)
        init = he_uniform()
        last = Concatenate(axis=1, name="concat_last")(last_skip_seq_list)
        last = Conv3D(last_seq_filters,
                      last_seq_ks,
                      padding="same",
                      kernel_initializer=init,
                      use_bias=False,
                      name="last")(last)
        self.log.info("      output----------- {0}".format(last.shape))
        return last

    def _unet_convlstm(self,
                       ipt_shape,
                       ds_filters,
                       ds_ks,
                       ds_strides,
                       leak_relu_alpha,
                       use_bns,
                       use_maxps,
                       cv_lstm_filters,
                       cv_lstm_ks,
                       cv_lstm_rc_drp,
                       use_us_drps,
                       us_drp,
                       last_filters,
                       last_ks,
                       last_seq_filters,
                       last_seq_ks,
                       start_date,
                       chn):
        """
        Description:
            U-net 3D와 ConvLSTM 2D를 조합한 모델 생성.
        Args:
            ipt_shape: tuple, 입력 데이터 shape
            ds_filters: list, Downsampling layer의 filters
            ds_ks: list, Downsampling layer의 kernel_sizes
            ds_strides: list, Downsampling layer의 strides
            leak_relu_alpha: float, Downsampling layer의 Leaky ReLU의 alpha 값
            use_bns: list, Downsampling layer의 BatchNormalization 사용여부
            use_maxps: list, Downsampling layer의 Maxpooling 사용여부
            cv_lstm_filters: list, ConvLSTM layer의 filters
            cv_lstm_ks: list, ConvLSTM layer의 kernel_sizes
            cv_lstm_rc_drp: float, ConvLSTM layer의 recurrent_dropout 비율
            use_us_drps: list, Upsampling layer의 Dropout 사용여부
            us_drp: float, Upsampling layer의 Dropout 비율
            last_filters: int, 최종 Upsampling 결과 features
            last_ks: tuple, 최종 Upsampling의 kernel_size
            last_seq_filters: int, 최종 layer 결과 features
            last_seq_ks: 최종 layer의 kernel_size
            start_date: str, 모델 생성일자
            chn, str, 위성 채널
        Return:
            model: tf.keras.Model, Unet + ConvLSTM 모델
        """
        us_filters = list(reversed(ds_filters[:-1]))
        us_ks = [elm[1:] for elm in reversed(ds_ks[:-1])]
        us_strides = [elm[1:] for elm in reversed(ds_strides[1:])]
        us_use_bns = list(reversed(use_bns[:-1]))
        us_use_maxps = list(reversed(use_maxps[:-1]))
        
        # Input
        ipt = self._input(ipt_shape)
        # DownSampling
        down_stack, skip_stack = self._ds_stack(ipt,
                                                ds_filters,
                                                ds_ks,
                                                ds_strides,
                                                leak_relu_alpha,
                                                use_bns,
                                                use_maxps)
        # ConvLSTM
        convlstm_us_list = self._convlstm_list(down_stack,
                                               cv_lstm_filters,
                                               cv_lstm_ks,
                                               cv_lstm_rc_drp)
        # UpSampling
        convlstm_us_list = self._us_list(skip_stack,
                                         convlstm_us_list,
                                         us_filters,
                                         us_ks,
                                         us_strides,
                                         use_us_drps,
                                         us_drp,
                                         leak_relu_alpha,
                                         us_use_bns,
                                         us_use_maxps)
        # Output
        last = self._output(ipt,
                            convlstm_us_list,
                            ds_strides,
                            last_filters,
                            last_ks,
                            last_seq_filters,
                            last_seq_ks)
        
        return Model(inputs=ipt,
                     outputs=last,
                     name=self.cfg_tr.MODEL_NAME.format(chn, start_date))

    def get_optimizer(self):
        """
        Description:
            모델 학습에 사용할 Optimizer 선택.
            adam, rmsprop, sgd, adagrad 사용가능
        Args:
            None
        Returns:
            optimizer: 옵티마이저
        """
        name = self.cfg_opt.USE
        if name == "adam":
            optimizer = Adam(learning_rate=self.cfg_opt.ADAM.LR,
                            beta_1=self.cfg_opt.ADAM.BETAS[0],
                            beta_2=self.cfg_opt.ADAM.BETAS[1],
                            epsilon=float(self.cfg_opt.ADAM.EPSILON))
        elif name == "rmsprop":
            optimizer = RMSprop(learning_rate=self.cfg_opt.RMSPROP.LR,
                                rho=self.cfg_opt.RMSPROP.RHO,
                                momentum=self.cfg_opt.RMSPROP.MOMENTUM,
                                epsilon=float(self.cfg_opt.RMSPROP.EPSILON))
        elif name == "sgd":
            optimizer = SGD(learning_rate=self.cfg_opt.SGD.LR,
                            momentum=self.cfg_opt.SGD.MOMENTUM)
        elif name == "adagrad":
            optimizer = Adagrad(learning_rate=self.cfg_opt.ADAGRAD.LR,
                                initial_accumulator_value=self.cfg_opt.ADAGRAD.INIT_ACCUM,
                                epsilon=float(self.cfg_opt.ADAGRAD.EPSILON))
        else:
            log_sent = ">>> Able to use : adam, rmsprop, sgd, adagrad / Cannot use : {0}"
            self.log.info(log_sent.format(name))
            self.err_log.error(log_sent.format(name))
        return optimizer
    
    def get_loss_func(self):
        """
        Description:
            모델 학습에 사용할 Loss function 선택.
            mae, mse, logcosh, huber, pixelwise_regular 사용가능
        Args:
            None
        Returns:
            loss_func: 손실 함수
        """
        name = self.cfg_loss.USE
        if name == "mae":
            loss_func = AmpMae(amplification=self.cfg_loss.MAE.AMPLIFICATION)
        elif name == "mse":
            loss_func = AmpMse(amplification=self.cfg_loss.MSE.AMPLIFICATION)
        elif name == "huber":
            loss_func = AmpHuber(delta=self.cfg_loss.HUBER.DELTA,
                                 amplification=self.cfg_loss.HUBER.AMPLIFICATION)
        elif name == "logcosh":
            loss_func = AmpLogCosh(amplification=self.cfg_loss.LOGCOSH.AMPLIFICATION)
        elif name == "pixelwise_regular":
            loss_func = PixelWiseRegularizer(amplification=self.cfg_loss.PIXELWISE_REGULAR.AMPLIFICATION,
                                             magnitude=self.cfg_loss.PIXELWISE_REGULAR.MAGNITUDE,
                                             min_value=self.cfg_loss.PIXELWISE_REGULAR.MIN_VALUE,
                                             max_value=self.cfg_loss.PIXELWISE_REGULAR.MAX_VALUE)
        else:
            log_sent = ">>> Able to use : mae, mse, huber, logcosh, pixelwise_regular / Cannot use : {0}"
            self.log.info(log_sent.format(name))
            self.err_log.error(log_sent.format(name))
        return loss_func

    def save_model_cfg(self, chn, start_date, model):
        """
        Description:
            모델의 설정값 저장
        Args:
            chn, str, 위성 채널
            start_date: str, 모델 생성일자
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        Returns:
            None
        """
        self.log.info(">>> Save Model's Configs")
        model_name = self.cfg_tr.MODEL_NAME.format(chn, start_date)
        hist_file = "{0}.txt".format(model_name)
        hist_dir = return_dir(True, self.cfg_tr.HIST_DIR, chn)
        hist_path = os.path.join(hist_dir, hist_file)
        TRAIN_DATA_LEN = 0
        VALID_DATA_LEN = 0
        try:
            with open(hist_path, "w") as f:
                f.write("Model name: {0}\n".format(model_name))
                f.write("Used Train datas: ({0})\n".format(self.cfg_tr.DATA_TYPE))
                for f_name, f_shape in zip(self.cfg_tr.DATA_FILE, self.cfg_tr.DATA_SHAPE):
                    f.write("    File: {0} / Shape: {1}\n".format(f_name.format(chn), f_shape))
                    TRAIN_DATA_LEN += f_shape[0]
                f.write("Used Validation datas: ({0})\n".format(self.cfg_tr.DATA_TYPE))
                for f_name, f_shape in zip(self.cfg_tr.VALID_DATA_FILE, self.cfg_tr.VALID_DATA_SHAPE):
                    f.write("    File: {0} / Shape: {1}\n".format(f_name.format(chn), f_shape))
                    VALID_DATA_LEN += f_shape[0]
                TRAIN_DATA_SEQLEN = math.ceil((TRAIN_DATA_LEN - self.cfg_d.INPUT_SEQ - self.cfg_d.OUTPUT_SEQ + 1) / self.cfg_tr.BATCH_SIZE)
                VALID_DATA_SEQLEN = math.ceil((VALID_DATA_LEN - self.cfg_d.INPUT_SEQ - self.cfg_d.OUTPUT_SEQ + 1) / self.cfg_tr.BATCH_SIZE)
                f.write("Training / Validation data nums: {0} / {1}\n".format(TRAIN_DATA_LEN, VALID_DATA_LEN))
                f.write("Training / Validation sequence nums: {0} / {1}\n".format(TRAIN_DATA_SEQLEN, VALID_DATA_SEQLEN))
                f.write("Parametes: Data\n")
                self._print_cfg(self.cfg_d, f)
                f.write("Parametes: Model\n")
                self._print_cfg(self.cfg_m, f)
                f.write("Parametes: Optimizer using {0}\n".format(self.cfg_opt.USE.upper()))
                self._print_cfg(getattr(self.cfg_opt, self.cfg_opt.USE.upper()), f)
                f.write("Parametes: Loss function using {0}\n".format(self.cfg_loss.USE.upper()))
                self._print_cfg(getattr(self.cfg_loss, self.cfg_loss.USE.upper()), f)
                f.write("Parametes: Normalizer using {0}\n".format(self.cfg_norm.FUNCTION.upper()))
                self._print_cfg(getattr(self.cfg_norm, chn.upper()), f)
                f.write("Batch size: {0}\n".format(self.cfg_tr.BATCH_SIZE))
                f.write("Steps per 1 Epoch: {0}\n".format(TRAIN_DATA_SEQLEN))
                f.write("Maximum Epoch: {0}\n".format(self.cfg_tr.EPOCH))
                f.write("Early Stopping Patience: {0}\n".format(self.cfg_tr.PATIENCE))
                f.write("Steps per Execution on Model compile: {0}\n".format(self.cfg_tr.STEPS_EXEC))
                f.write("Checkpoint term: {0}\n".format(self.cfg_tr.CKPT_PERIOD))
                if self.ckpt_path is not None:
                    f.write("Import Checkpoint: {0}\n".format(self.ckpt_path.split("/")[-1]))
                model.summary(line_length=120, print_fn=lambda x: f.write(x + "\n"))   # 모델 summary
        except Exception as e:
            self.err_log.error(">>> Error on Saving Model's Parameters : {0}".format(e))
        else:
            self.log.info(">>> Save Model's Configs completely")
    
    def _print_cfg(self, cfg, f, blank=2, cnt=1):
        """
        Description:
            Namedtuple 유형의 설정값 출력
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            f: file object, 작성할 파일 객체
            subject: str, 출력 설정 중 일부만 출력하고자 하는 optimizer, normalizer
            blank: str, 앞의 공백 수
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        Returns:
            None
        """
        cnt += 1
        for field in cfg._fields:
            cfg_f = getattr(cfg, field)
            # Namedtuple 이면 depth를 더 들어감
            if isinstance(cfg_f, tuple) and hasattr(cfg_f, "_asdict") and hasattr(cfg_f, "_fields"):
                f.write("{0}{1}:\n".format(" "*blank*cnt, field))
                self._print_cfg(cfg_f, f, blank, cnt)
            else:
                f.write("{0}{1}: {2}\n".format(" "*blank*cnt, field, cfg_f))
    
    def load_ckpt(self, model):
        """
        Description:
            Checkpoint 경로가 설정되어 있으면 해당 Checkpoint 불러오기.
        Args:
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        Returns:
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델(Checkpoint 반영)
        """
        model.load_weights(self.ckpt_path)
        self.log.info(">>> Load Checkpoint from {0}".format(self.ckpt_path.split("/")[-1]))
    
    def load_model(self, model_path):
        """
        Description:
            Tensorflow 모델 불러오기.
            Custom Class 사용한 걸로 인해 일반 compile=True로 load가 안 됨.
            ==> https://www.tensorflow.org/guide/keras/save_and_serialize#registering_the_custom_object 로 custom 객체 저장방법 수정 필요
        Args:
            model_path: str, 모델 디렉토리 경로
        Returns:
            model: tf.keras.Model, Unet + ConvLSTM 모델
        """
        from tensorflow.keras.models import load_model
        model = load_model(model_path, compile=False)
        self.log.info(">>> Use model: {0}".format(model.name))
        return model


class SatPredModelV4(object):
    """Satellite Timeseries Prediction Model."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            위성(GK2A) 영상 시계열 예측 모델 init 설정.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(SatPredModelV4, self).__init__()
        self.cfg = cfg
        self.cfg_opt = cfg.PARAMS.OPT
        self.cfg_loss = cfg.PARAMS.LOSS
        self.cfg_d = cfg.PARAMS.DATA
        self.cfg_m = cfg.PARAMS.MODEL
        self.cfg_norm = cfg.PARAMS.NORMALIZER
        self.cfg_tr = cfg.TRAIN
        self.ckpt_path = cfg.TRAIN.IMPORT_CKPT_PATH

        self.input_seq = cfg.PARAMS.DATA.INPUT_SEQ
        self.output_seq = cfg.PARAMS.DATA.OUTPUT_SEQ

        self.log, self.err_log = log, err_log

    def __call__(self, start_date):
        """
        Description:
            모델 생성 및 관련 설정값 저장.
        Args:
            start_date: str, 모델 생성일자
        Returns:
            model: tf.keras.model 객체
        """
        chn = self.cfg_d.CHANNEL
        model = self.make_model(chn, start_date)
        self.save_model_cfg(chn, start_date, model)
        if self.ckpt_path is not None:
            self.load_ckpt(model)
        return model

    def make_model(self, chn, start_date):
        """
        Description:
            이미지 시계열 예측 모델 생성(U-net + ConvLSTM).
            입력/출력 시계열 길이 다르게 설정 가능.
            출력 시계열 길이만큼 ConvLSTM 구성 뒤 각각 Upsampling 진행 후 결합.
            V2에서 _us_list에 skip connection 연결점이 끊어져 연결점 추가.
            V3에서 skip connection을 맨처음 downsampling 결과만 사용.
        Args:
            chn: str, 위성 채널
            start_date: str, 모델 생성일자
        Return:
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        """
        self.log.info(">>> Make Model on {0} channel".format(chn.upper()))
        ipt_h, ipt_w, ipt_c = self.cfg_d.INPUT_SHAPE
        ipt_shape = (self.input_seq, ipt_h, ipt_w, ipt_c)

        ds_filters = self.cfg_m.DS.FILTERS
        ds_ks = [(1, ks, ks) for ks in self.cfg_m.DS.KN_SIZES]
        ds_strides = [(1, st, st) for st in self.cfg_m.DS.STRIDES]
        leak_relu_alpha = self.cfg_m.DS.LEAKY_RELU_ALPHA
        use_bns = self.cfg_m.DS.USE_BNS
        use_maxps = self.cfg_m.DS.USE_MAXPS

        cvlstm_filters = [self.cfg_m.CONVLSTM.FILTERS for _ in range(self.output_seq)]
        _ks = self.cfg_m.CONVLSTM.KN_SIZES
        cvlstm_ks = [(_ks, _ks) for _ in range(self.output_seq)]
        cvlstm_rc_drp = self.cfg_m.CONVLSTM.RC_DRP

        use_us_drps = self.cfg_m.US.USE_DRPS
        us_drp = self.cfg_m.US.DRP
        last_filters = self.cfg_m.US.LAST_SEQ_FILTER
        last_ks = (self.cfg_m.US.LAST_KN_SIZE, self.cfg_m.US.LAST_KN_SIZE)
        last_seq_filters = self.cfg_m.US.LAST_SEQ_FILTER
        last_seq_ks = (1, self.cfg_m.US.LAST_SEQ_KN_SIZE, self.cfg_m.US.LAST_SEQ_KN_SIZE)

        # Conv3d Unet + ConvLSTM
        model = self._unet_convlstm(ipt_shape,
                                    ds_filters,
                                    ds_ks,
                                    ds_strides,
                                    leak_relu_alpha,
                                    use_bns,
                                    use_maxps,
                                    cvlstm_filters,
                                    cvlstm_ks,
                                    cvlstm_rc_drp,
                                    use_us_drps,
                                    us_drp,
                                    last_filters,
                                    last_ks,
                                    last_seq_filters,
                                    last_seq_ks,
                                    start_date,
                                    chn)
        opt = self.get_optimizer()
        loss = self.get_loss_func()
        metrics = [MeanAbsoluteError(), RootMeanSquaredError(), LogCoshError()]
        model.compile(optimizer=opt,
                      loss=loss,
                      metrics=metrics,
                      steps_per_execution=self.cfg_tr.STEPS_EXEC)

        return model

    def _downsample(self,
                    x,
                    filters,
                    kn_size,
                    name,
                    stride,
                    use_bn=True,
                    leak_relu_alpha=0.3,
                    use_maxp=True):
        """
        Description:
            DownSampling 3-D convolution block.
        Args:
            x: tf.tensor, input tensor.
            filters, int, Conv에 적용할 filters (hidden state)
            kn_size: tuple, Conv에 적용할 kernel size
            name: str, layer 이름에 붙일 것
            stride: tuple, Conv에 적용할 stride
            use_bn: bool, BatchNormalization 사용 여부, default=True
            leak_relu_alpha: float, Leakly ReLU에 적용할 alpha 값, default=0.3
            use_maxp: bool, Maxpooling 사용 여부, default=True
        Return:
            Downsampling block: tf.keras.Model
        """
        init = he_uniform()
        x = Conv3D(filters,
                   kn_size,
                   strides=stride,
                   padding="same",
                   kernel_initializer=init,
                   use_bias=False,
                   name="ds_conv_{0}".format(name))(x)
        if use_bn:
            x = BatchNormalization(name="ds_bn_{0}".format(name))(x)
        x = LeakyReLU(alpha=leak_relu_alpha, name="ds_act_{0}".format(name))(x)
        if use_maxp:
            x = MaxPooling3D(pool_size=kn_size,
                             strides=(1, 1, 1),
                             padding="same",
                             name="ds_maxp_{0}".format(name))(x)
        return x

    def _upsample_2d(self,
                     x,
                     filters,
                     kn_size,
                     name,
                     seq_idx,
                     stride,
                     use_us_drps=True,
                     us_drp=0.5,
                     last=False):
        """
        Description:
            UpSampling 2-D convolution block.
        Args:
            x: tf.tensor, input tensor.
            filters, int, Conv에 적용할 filters (hidden state)
            kn_size: tuple, Conv에 적용할 kernel size
            name: str, layer 이름에 붙일 것
            seq_idx: str, layer 이름에 붙일 sequence index
            stride: tuple, Conv에 적용할 stride
            use_us_drps: bool, Dropout 사용 여부, default=True
            us_drp: float Dropout 사용할 때 적용할 비율, default=0.5
            last: bool, 마지막 layer 확인 여부, default=False
        Return:
            Upsampling block: tf.keras.Model
        """
        init = he_uniform()
        if not last:
            x = Conv2DTranspose(filters,
                                kn_size,
                                strides=stride,
                                padding="same",
                                kernel_initializer=init,
                                use_bias=False,
                                name="us_conv_{0}_{1}".format(name, seq_idx))(x)
            x = BatchNormalization(name="us_bn_{0}_{1}".format(name, seq_idx))(x)
            if use_us_drps:
                x = Dropout(us_drp, name="us_drp_{0}_{1}".format(name, seq_idx))(x)
            x = ReLU(name="us_act_{0}_{1}".format(name, seq_idx))(x)
        else:
            x = Conv2DTranspose(filters,
                                kn_size,
                                strides=stride,
                                padding="same",
                                kernel_initializer=init,
                                activation="linear",
                                name="{0}_{1}".format(name, seq_idx))(x)
        return x

    def _convlstm(self,
                  x,
                  filters,
                  kn_size,
                  name,
                  cv_lstm_rc_drp=0.5):
        """
        Description:
            2-D Convolutional LSTM block.
        Args:
            x: tf.tensor, input tensor.
            filters, int, ConvLSTM에 적용할 filters (hidden state)
            kn_size: tuple, ConvLSTM에 적용할 kernel size
            name: str, layer 이름에 붙일 것
            cv_lstm_rc_drp: tuple, ConvLSTM에 적용할 recurrent_dropout 비율, default=0.5
        Return:
            ConvLSTM2D block: tf.keras.Model
        """
        init = he_uniform()
        x = ConvLSTM2D(filters,
                       kn_size,
                       padding="same",
                       kernel_initializer=init,
                       use_bias=True,
                       recurrent_dropout=cv_lstm_rc_drp,
                       return_sequences=True,
                       name="conv_lstm_{0}".format(name))(x)
        x = BatchNormalization(name="conv_lstm_bn_{0}".format(name))(x)
        return x

    def _input(self, ipt_shape):
        """
        Description:
            Input layer.
        Args:
            ipt_shape: tuple, Input shape
        Return:
            ipt: KerasTensor, 입력 layer
        """
        ipt = Input(shape=ipt_shape, name="input")
        self.log.info("      input----------- {0}".format(ipt.shape))
        return ipt

    def _ds_stack(self,
                  x,
                  ds_filters,
                  ds_ks,
                  ds_strides,
                  leak_relu_alpha,
                  use_bns,
                  use_maxps):
        """
        Description:
            DownSampling 3-D convolution stacks.
            (seq_len, h, w, filters) 를 (h, w, filters * seq_len)로 변환.
        Args:
            x: KerasTensor, 입력 layer
            ds_filters: list, Downsampling layer의 filters
            ds_ks: list, Downsampling layer의 kernel_sizes
            ds_strides: list, Downsampling layer의 strides
            leak_relu_alpha: float, Downsampling layer의 Leaky ReLU의 alpha 값
            use_bns: list, Downsampling layer의 BatchNormalization 사용여부
            use_maxps: list, Downsampling layer의 Maxpooling 사용여부
        Return:
            down_stack: list, Downsampling을 적용한 부분
            skip_stack: list, Skip connection으로 사용할 부분
        """
        down_stack = []
        skip_stack = []
        for i, (f, ks, st, bn, mxp) in enumerate(zip(ds_filters,
                                                     ds_ks,
                                                     ds_strides,
                                                     use_bns,
                                                     use_maxps)):
            x = self._downsample(x,
                                 filters=f,
                                 kn_size=ks,
                                 name="{0:02d}".format(i+1),
                                 stride=st,
                                 use_bn=bn,
                                 leak_relu_alpha=leak_relu_alpha,
                                 use_maxp=mxp)
            down_stack.append(x)
            self.log.info("      down----------- {0}".format(x.shape))
            x_concat = [Lambda(lambda z: z[:, k, ...],
                               name="ds_cvt_{0:02d}_{1:02d}".format(i+1, k+1))(x)
                        for k in range(self.input_seq)]
            x_concat = Concatenate(name="skip_{0:02d}".format(i+1))(x_concat)
            if i < len(ds_filters) - 1:
                skip_stack.append(x_concat)
                self.log.info("      down_cvt----------- {0}".format(x_concat.shape))
        skip_stack = list(reversed(skip_stack))
        return down_stack, skip_stack

    def _convlstm_list(self,
                       down_stack,
                       cv_lstm_filters,
                       cv_lstm_ks,
                       cv_lstm_rc_drp):
        """
        Description:
            2-D Convolutional LSTM list.
            ConvLSTM -> ConvLSTM 은 seq 상태로 데이터 전달.
            ConvLSTM에서 Upsampling할 부분은 seq 마지막 부분만 전달.
        Args:
            down_stack: list, Downsampling을 적용한 부분
            cv_lstm_filters: list, ConvLSTM layer의 filters
            cv_lstm_ks: list, ConvLSTM layer의 kernel_sizes
            cv_lstm_rc_drp: float, ConvLSTM layer의 recurrent_dropout 비율
        Return:
            convlstm_us_list: list, 각각 seq마다 Upsampling에 사용할 부분
        """
        x = down_stack[-1]
        convlstm_us_list = []
        for i, (f, ks) in enumerate(zip(cv_lstm_filters, cv_lstm_ks)):
            x = self._convlstm(x,
                               filters=f,
                               kn_size=ks,
                               name="{0:02d}".format(i+1),
                               cv_lstm_rc_drp=cv_lstm_rc_drp)
            x_us = Lambda(lambda x: x[:, -1, ...], name="conv_lstm_tail_{0:02d}".format(i+1))(x)
            convlstm_us_list.append(x_us)
        self.log.info("      convlstm----------- {0} * {1}".format(x.shape, self.output_seq))
        self.log.info("      convlstm_us----------- {0} * {1}".format(x_us.shape, self.output_seq))
        return convlstm_us_list

    def _next_us_convlstm(self,
                          us,
                          skip,
                          us_filter,
                          us_ks,
                          leak_relu_alpha,
                          use_bn,
                          use_maxp,
                          i,
                          j):
        """
        Description:
            Next ConvLSTM result upsampling.
            이전 skip connection 반영하여 개별 seq 결과마다 Upsampling 결과 산출.
        Args:
            us: KerasTensor, Upsampling 결과
            skip: KerasTensor, Downsampling에서 전달된 skip connection
            us_filter: int, Upsampling layer의 filter
            us_ks: int, Upsampling layer의 kernel_size
            leak_relu_alpha: float, Downsampling layer의 Leaky ReLU의 alpha 값 동일하게 사용
            use_bns: bool, Upsampling의 BatchNormalization 사용여부
            use_maxps: bool, Upsampling의 Maxpooling 사용여부
            i: int, Upsampling level index
            j: int, ConvLSTM index
        Return:
            next_us: KerasTensor, Skip Connection 반영된 Upsampling 결과 (다음 Upsampling에 전달)
        """
        init = he_uniform()
        # skip connection과 upsampling 결과 결합
        concat = Concatenate(name="us_concat_{0:02d}_{1:02d}".format(i+1, j+1))([skip, us])
        next_us = Conv2D(us_filter,
                         us_ks,
                         padding="same",
                         kernel_initializer=init,
                         use_bias=False,
                         name="next_us_{0:02d}_{1:02d}".format(i+1, j+1))(concat)
        if use_bn:
            next_us = BatchNormalization(name="next_us_bn_{0:02d}_{1:02d}".format(i+1, j+1))(next_us)
        next_us = LeakyReLU(alpha=leak_relu_alpha,
                            name="next_us_act_{0:02d}_{1:02d}".format(i+1, j+1))(next_us)
        if use_maxp:
            next_us = MaxPooling2D(pool_size=us_ks,
                                   strides=(1, 1),
                                   padding="same",
                                   name="next_us_maxp_{0:02d}_{1:02d}".format(i+1, j+1))(next_us)
        return next_us

    def _us_list(self,
                 skip_stack,
                 convlstm_us_list,
                 us_filters,
                 us_ks,
                 us_strides,
                 use_us_drps,
                 us_drp,
                 leak_relu_alpha,
                 us_use_bns,
                 us_use_maxps):
        """
        Description:
            UpSampling 2-D convolution list (Sequence).
            개별 seq 결과마다 Upsampling 결과 산출.
            Downsampling 쪽에서의 skip connection을 모든 seq에 적용.
        Args:
            skip_stack: list, Skip connection으로 사용할 부분
            convlstm_us_list: list, 각각 seq마다 Upsampling에 사용할 부분(convlstm 바로 다음)
            us_filters: list, Upsampling layer의 filters
            us_ks: list, Upsampling layer의 kernel_sizes
            us_strides: list, Upsampling layer의 strides
            use_us_drps: list, Upsampling layer의 Dropout 사용여부
            us_drp: float, Upsampling layer의 Dropout 비율
            leak_relu_alpha: float, Downsampling layer의 Leaky ReLU의 alpha 값 동일하게 사용
            us_use_bns: list, ConvLSTM Upsampling의 BatchNormalization 사용여부
            us_use_maxps: list, ConvLSTM Upsampling의 Maxpooling 사용여부
        Return:
            convlstm_us_list: list, 각각 seq마다 Upsampling에 사용할 부분(마지막 것)
        """
        for i, (f, ks, st, d, skip, bn, maxp) in enumerate(zip(us_filters,
                                                               us_ks,
                                                               us_strides,
                                                               use_us_drps,
                                                               skip_stack,
                                                               us_use_bns,
                                                               us_use_maxps)):
            us_list = []
            for j, c_us in enumerate(convlstm_us_list):
                us = self._upsample_2d(c_us,
                                       filters=f,
                                       kn_size=ks,
                                       name="{0:02d}".format(i+1),
                                       seq_idx="{0:02d}".format(j+1),
                                       stride=st,
                                       use_us_drps=d,
                                       us_drp=us_drp)
                # 다음 convlstm 및 us 로 보낼 것 계산
                us = self._next_us_convlstm(us,
                                            skip,
                                            f,
                                            ks,
                                            leak_relu_alpha,
                                            bn,
                                            maxp,
                                            i,
                                            j)
                us_list.append(us)
                self.log.debug("      next_skip----------- {0} : {1}".format(j, skip.shape))
            self.log.info("      up_concat----------- {0} * {1}".format(us.shape, self.output_seq))
            convlstm_us_list = us_list
        return convlstm_us_list

    def _output(self,
                ipt,
                convlstm_us_list,
                ds_strides,
                last_filters,
                last_ks,
                last_seq_filters,
                last_seq_ks):
        """
        Description:
            Last output.
            입력도 skip connection 사용하여 개별 seq 결과마다 Upsampling 결과 산출.
            Downsampling 쪽에서의 skip connection을 1 times 밀어가며 다음 seq에 적용.
            마지막에 Convolution 이용하여 Output 형상에 맞춤.
        Args:
            ipt: KerasTensor, 입력 layer
            convlstm_us_list: list, 각각 seq마다 Upsampling에 사용할 부분(마지막 것)
            ds_strides: list, Downsampling layer의 strides
            last_filters: int, 최종 Upsampling 결과 features
            last_ks: tuple, 최종 Upsampling의 kernel_size
            last_seq_filters: int, 최종 layer 결과 features
            last_seq_ks: 최종 layer의 kernel_size
        Return:
            last: KerasTensor, 최종 layer
        """
        # Output (one scene result)
        last_skip_seq_list = []
        ipt_skip = [Lambda(lambda x: x[:, k, ...],
                           name="ipt_cvt_{0:02d}".format(k+1))(ipt)
                    for k in range(self.input_seq)]
        ipt_skip = Concatenate(name="ipt_skip")(ipt_skip)
        self.log.info("      ipt_skip----------- {0}".format(ipt_skip.shape))
        for j, us in enumerate(convlstm_us_list):
            last_us = self._upsample_2d(us,
                                        filters=last_filters,
                                        kn_size=last_ks,
                                        name="last",
                                        seq_idx="{0:02d}".format(j+1),
                                        stride=ds_strides[0][1:],   # 맨처음 DS
                                        last=True)
            # 다음 convlstm으로 보낼 t ~ t+5 와 upsampling 결과 결합
            last_skip = Concatenate(name="us_concat_last_{0:02d}".format(j+1))([ipt_skip, last_us])
            last_skip_shape = [1] + list(last_skip.shape)[1:]
            last_skip_seq = Reshape(last_skip_shape,
                                    name="us_concat_last_seq_{0:02d}".format(j+1))(last_skip)
            last_skip_seq_list.append(last_skip_seq)
            self.log.debug("      last_skip----------- {0} {1}".format(j, last_skip.shape))
            # concat결과에서 t를 제외하고 t+1 ~ t+6만 skip connection 보냄
            ipt_skip = Lambda(lambda x: x[..., last_filters:], name="ipt_seq_skip_{0:02d}".format(j+1))(last_skip)
            self.log.debug("      next_ipt_skip----------- {0} {1}".format(j, ipt_skip.shape))
        self.log.info("      last_skip----------- {0} * {1}".format(last_skip.shape, self.output_seq))

        # Output (make sequence)
        init = he_uniform()
        last = Concatenate(axis=1, name="concat_last")(last_skip_seq_list)
        last = Conv3D(last_seq_filters,
                      last_seq_ks,
                      padding="same",
                      kernel_initializer=init,
                      use_bias=False,
                      name="last")(last)
        self.log.info("      output----------- {0}".format(last.shape))
        return last

    def _unet_convlstm(self,
                       ipt_shape,
                       ds_filters,
                       ds_ks,
                       ds_strides,
                       leak_relu_alpha,
                       use_bns,
                       use_maxps,
                       cv_lstm_filters,
                       cv_lstm_ks,
                       cv_lstm_rc_drp,
                       use_us_drps,
                       us_drp,
                       last_filters,
                       last_ks,
                       last_seq_filters,
                       last_seq_ks,
                       start_date,
                       chn):
        """
        Description:
            U-net 3D와 ConvLSTM 2D를 조합한 모델 생성.
        Args:
            ipt_shape: tuple, 입력 데이터 shape
            ds_filters: list, Downsampling layer의 filters
            ds_ks: list, Downsampling layer의 kernel_sizes
            ds_strides: list, Downsampling layer의 strides
            leak_relu_alpha: float, Downsampling layer의 Leaky ReLU의 alpha 값
            use_bns: list, Downsampling layer의 BatchNormalization 사용여부
            use_maxps: list, Downsampling layer의 Maxpooling 사용여부
            cv_lstm_filters: list, ConvLSTM layer의 filters
            cv_lstm_ks: list, ConvLSTM layer의 kernel_sizes
            cv_lstm_rc_drp: float, ConvLSTM layer의 recurrent_dropout 비율
            use_us_drps: list, Upsampling layer의 Dropout 사용여부
            us_drp: float, Upsampling layer의 Dropout 비율
            last_filters: int, 최종 Upsampling 결과 features
            last_ks: tuple, 최종 Upsampling의 kernel_size
            last_seq_filters: int, 최종 layer 결과 features
            last_seq_ks: 최종 layer의 kernel_size
            start_date: str, 모델 생성일자
            chn, str, 위성 채널
        Return:
            model: tf.keras.Model, Unet + ConvLSTM 모델
        """
        us_filters = list(reversed(ds_filters[:-1]))
        us_ks = [elm[1:] for elm in reversed(ds_ks[:-1])]
        us_strides = [elm[1:] for elm in reversed(ds_strides[1:])]
        us_use_bns = list(reversed(use_bns[:-1]))
        us_use_maxps = list(reversed(use_maxps[:-1]))

        # Input
        ipt = self._input(ipt_shape)
        # DownSampling
        down_stack, skip_stack = self._ds_stack(ipt,
                                                ds_filters,
                                                ds_ks,
                                                ds_strides,
                                                leak_relu_alpha,
                                                use_bns,
                                                use_maxps)
        # ConvLSTM
        convlstm_us_list = self._convlstm_list(down_stack,
                                               cv_lstm_filters,
                                               cv_lstm_ks,
                                               cv_lstm_rc_drp)
        # UpSampling
        convlstm_us_list = self._us_list(skip_stack,
                                         convlstm_us_list,
                                         us_filters,
                                         us_ks,
                                         us_strides,
                                         use_us_drps,
                                         us_drp,
                                         leak_relu_alpha,
                                         us_use_bns,
                                         us_use_maxps)
        # Output
        last = self._output(ipt,
                            convlstm_us_list,
                            ds_strides,
                            last_filters,
                            last_ks,
                            last_seq_filters,
                            last_seq_ks)

        return Model(inputs=ipt,
                     outputs=last,
                     name=self.cfg_tr.MODEL_NAME.format(chn, start_date))

    def get_optimizer(self):
        """
        Description:
            모델 학습에 사용할 Optimizer 선택.
            adam, rmsprop, sgd, adagrad 사용가능
        Args:
            None
        Returns:
            optimizer: 옵티마이저
        """
        name = self.cfg_opt.USE
        if name == "adam":
            optimizer = Adam(learning_rate=self.cfg_opt.ADAM.LR,
                            beta_1=self.cfg_opt.ADAM.BETAS[0],
                            beta_2=self.cfg_opt.ADAM.BETAS[1],
                            epsilon=float(self.cfg_opt.ADAM.EPSILON))
        elif name == "rmsprop":
            optimizer = RMSprop(learning_rate=self.cfg_opt.RMSPROP.LR,
                                rho=self.cfg_opt.RMSPROP.RHO,
                                momentum=self.cfg_opt.RMSPROP.MOMENTUM,
                                epsilon=float(self.cfg_opt.RMSPROP.EPSILON))
        elif name == "sgd":
            optimizer = SGD(learning_rate=self.cfg_opt.SGD.LR,
                            momentum=self.cfg_opt.SGD.MOMENTUM)
        elif name == "adagrad":
            optimizer = Adagrad(learning_rate=self.cfg_opt.ADAGRAD.LR,
                                initial_accumulator_value=self.cfg_opt.ADAGRAD.INIT_ACCUM,
                                epsilon=float(self.cfg_opt.ADAGRAD.EPSILON))
        else:
            log_sent = ">>> Able to use : adam, rmsprop, sgd, adagrad / Cannot use : {0}"
            self.log.info(log_sent.format(name))
            self.err_log.error(log_sent.format(name))
        return optimizer

    def get_loss_func(self):
        """
        Description:
            모델 학습에 사용할 Loss function 선택.
            mae, mse, logcosh, huber, pixelwise_regular 사용가능
        Args:
            None
        Returns:
            loss_func: 손실 함수
        """
        name = self.cfg_loss.USE
        if name == "mae":
            loss_func = AmpMae(amplification=self.cfg_loss.MAE.AMPLIFICATION)
        elif name == "mse":
            loss_func = AmpMse(amplification=self.cfg_loss.MSE.AMPLIFICATION)
        elif name == "huber":
            loss_func = AmpHuber(delta=self.cfg_loss.HUBER.DELTA,
                                 amplification=self.cfg_loss.HUBER.AMPLIFICATION)
        elif name == "logcosh":
            loss_func = AmpLogCosh(amplification=self.cfg_loss.LOGCOSH.AMPLIFICATION)
        elif name == "pixelwise_regular":
            loss_func = PixelWiseRegularizer(amplification=self.cfg_loss.PIXELWISE_REGULAR.AMPLIFICATION,
                                             magnitude=self.cfg_loss.PIXELWISE_REGULAR.MAGNITUDE,
                                             min_value=self.cfg_loss.PIXELWISE_REGULAR.MIN_VALUE,
                                             max_value=self.cfg_loss.PIXELWISE_REGULAR.MAX_VALUE)
        else:
            log_sent = ">>> Able to use : mae, mse, huber, logcosh, pixelwise_regular / Cannot use : {0}"
            self.log.info(log_sent.format(name))
            self.err_log.error(log_sent.format(name))
        return loss_func

    def save_model_cfg(self, chn, start_date, model):
        """
        Description:
            모델의 설정값 저장
        Args:
            chn, str, 위성 채널
            start_date: str, 모델 생성일자
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        Returns:
            None
        """
        self.log.info(">>> Save Model's Configs")
        model_name = self.cfg_tr.MODEL_NAME.format(chn, start_date)
        hist_file = "{0}.txt".format(model_name)
        hist_dir = return_dir(True, self.cfg_tr.HIST_DIR, chn)
        hist_path = os.path.join(hist_dir, hist_file)
        TRAIN_DATA_LEN = 0
        VALID_DATA_LEN = 0
        try:
            with open(hist_path, "w") as f:
                f.write("Model name: {0}\n".format(model_name))
                f.write("Used Train datas: ({0})\n".format(self.cfg_tr.DATA_TYPE))
                for f_name, f_shape in zip(self.cfg_tr.DATA_FILE, self.cfg_tr.DATA_SHAPE):
                    f.write("    File: {0} / Shape: {1}\n".format(f_name.format(chn), f_shape))
                    TRAIN_DATA_LEN += f_shape[0]
                f.write("Used Validation datas: ({0})\n".format(self.cfg_tr.DATA_TYPE))
                for f_name, f_shape in zip(self.cfg_tr.VALID_DATA_FILE, self.cfg_tr.VALID_DATA_SHAPE):
                    f.write("    File: {0} / Shape: {1}\n".format(f_name.format(chn), f_shape))
                    VALID_DATA_LEN += f_shape[0]
                TRAIN_DATA_SEQLEN = math.ceil((TRAIN_DATA_LEN - self.cfg_d.INPUT_SEQ - self.cfg_d.OUTPUT_SEQ + 1) / self.cfg_tr.BATCH_SIZE)
                VALID_DATA_SEQLEN = math.ceil((VALID_DATA_LEN - self.cfg_d.INPUT_SEQ - self.cfg_d.OUTPUT_SEQ + 1) / self.cfg_tr.BATCH_SIZE)
                f.write("Training / Validation data nums: {0} / {1}\n".format(TRAIN_DATA_LEN, VALID_DATA_LEN))
                f.write("Training / Validation sequence nums: {0} / {1}\n".format(TRAIN_DATA_SEQLEN, VALID_DATA_SEQLEN))
                f.write("Parametes: Data\n")
                self._print_cfg(self.cfg_d, f)
                f.write("Parametes: Model\n")
                self._print_cfg(self.cfg_m, f)
                f.write("Parametes: Optimizer using {0}\n".format(self.cfg_opt.USE.upper()))
                self._print_cfg(getattr(self.cfg_opt, self.cfg_opt.USE.upper()), f)
                f.write("Parametes: Loss function using {0}\n".format(self.cfg_loss.USE.upper()))
                self._print_cfg(getattr(self.cfg_loss, self.cfg_loss.USE.upper()), f)
                f.write("Parametes: Normalizer using {0}\n".format(self.cfg_norm.FUNCTION.upper()))
                self._print_cfg(getattr(self.cfg_norm, chn.upper()), f)
                f.write("Batch size: {0}\n".format(self.cfg_tr.BATCH_SIZE))
                f.write("Steps per 1 Epoch: {0}\n".format(TRAIN_DATA_SEQLEN))
                f.write("Maximum Epoch: {0}\n".format(self.cfg_tr.EPOCH))
                f.write("Early Stopping Patience: {0}\n".format(self.cfg_tr.PATIENCE))
                f.write("Steps per Execution on Model compile: {0}\n".format(self.cfg_tr.STEPS_EXEC))
                f.write("Checkpoint term: {0}\n".format(self.cfg_tr.CKPT_PERIOD))
                if self.ckpt_path is not None:
                    f.write("Import Checkpoint: {0}\n".format(self.ckpt_path.split("/")[-1]))
                model.summary(line_length=120, print_fn=lambda x: f.write(x + "\n"))   # 모델 summary
        except Exception as e:
            self.err_log.error(">>> Error on Saving Model's Parameters : {0}".format(e))
        else:
            self.log.info(">>> Save Model's Configs completely")

    def _print_cfg(self, cfg, f, blank=2, cnt=1):
        """
        Description:
            Namedtuple 유형의 설정값 출력
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            f: file object, 작성할 파일 객체
            subject: str, 출력 설정 중 일부만 출력하고자 하는 optimizer, normalizer
            blank: str, 앞의 공백 수
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        Returns:
            None
        """
        cnt += 1
        for field in cfg._fields:
            cfg_f = getattr(cfg, field)
            # Namedtuple 이면 depth를 더 들어감
            if isinstance(cfg_f, tuple) and hasattr(cfg_f, "_asdict") and hasattr(cfg_f, "_fields"):
                f.write("{0}{1}:\n".format(" "*blank*cnt, field))
                self._print_cfg(cfg_f, f, blank, cnt)
            else:
                f.write("{0}{1}: {2}\n".format(" "*blank*cnt, field, cfg_f))

    def load_ckpt(self, model):
        """
        Description:
            Checkpoint 경로가 설정되어 있으면 해당 Checkpoint 불러오기.
        Args:
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        Returns:
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델(Checkpoint 반영)
        """
        model.load_weights(self.ckpt_path)
        self.log.info(">>> Load Checkpoint from {0}".format(self.ckpt_path.split("/")[-1]))

    def load_model(self, model_path):
        """
        Description:
            Tensorflow 모델 불러오기.
            Custom Class 사용한 걸로 인해 일반 compile=True로 load가 안 됨.
            ==> https://www.tensorflow.org/guide/keras/save_and_serialize#registering_the_custom_object 로 custom 객체 저장방법 수정 필요
        Args:
            model_path: str, 모델 디렉토리 경로
        Returns:
            model: tf.keras.Model, Unet + ConvLSTM 모델
        """
        from tensorflow.keras.models import load_model
        model = load_model(model_path, compile=False)
        self.log.info(">>> Use model: {0}".format(model.name))
        return model


class SatPredModelV5(object):
    """Satellite Timeseries Prediction Model."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            위성(GK2A) 영상 시계열 예측 모델 init 설정.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(SatPredModelV5, self).__init__()
        self.cfg = cfg
        self.cfg_opt = cfg.PARAMS.OPT
        self.cfg_loss = cfg.PARAMS.LOSS
        self.cfg_d = cfg.PARAMS.DATA
        self.cfg_m = cfg.PARAMS.MODEL
        self.cfg_norm = cfg.PARAMS.NORMALIZER
        self.cfg_tr = cfg.TRAIN
        self.ckpt_path = cfg.TRAIN.IMPORT_CKPT_PATH
        
        self.input_seq = cfg.PARAMS.DATA.INPUT_SEQ
        self.output_seq = cfg.PARAMS.DATA.OUTPUT_SEQ
        
        self.log, self.err_log = log, err_log
    
    def __call__(self, start_date):
        """
        Description:
            모델 생성 및 관련 설정값 저장.
        Args:
            start_date: str, 모델 생성일자
        Returns:
            model: tf.keras.model 객체
        """
        chn = self.cfg_d.CHANNEL
        model = self.make_model(chn, start_date)
        self.save_model_cfg(chn, start_date, model)
        if self.ckpt_path is not None:
            self.load_ckpt(model)
        return model

    def make_model(self, chn, start_date):
        """
        Description:
            이미지 시계열 예측 모델 생성(U-net + ConvLSTM).
            입력/출력 시계열 길이 다르게 설정 가능.
            출력 시계열 길이만큼 ConvLSTM 구성 뒤 각각 Upsampling 진행 후 결합.
            V2에서 _us_list에 skip connection 연결점이 끊어져 연결점 추가.
            V3에서 use_bias=False를 모두 True로 변경.
        Args:
            chn: str, 위성 채널
            start_date: str, 모델 생성일자
        Return:
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        """
        self.log.info(">>> Make Model on {0} channel".format(chn.upper()))
        ipt_h, ipt_w, ipt_c = self.cfg_d.INPUT_SHAPE
        ipt_shape = (self.input_seq, ipt_h, ipt_w, ipt_c)
        
        ds_filters = self.cfg_m.DS.FILTERS
        ds_ks = [(1, ks, ks) for ks in self.cfg_m.DS.KN_SIZES]
        ds_strides = [(1, st, st) for st in self.cfg_m.DS.STRIDES]
        leak_relu_alpha = self.cfg_m.DS.LEAKY_RELU_ALPHA
        use_bns = self.cfg_m.DS.USE_BNS
        use_maxps = self.cfg_m.DS.USE_MAXPS
        
        cvlstm_filters = [self.cfg_m.CONVLSTM.FILTERS for _ in range(self.output_seq)]
        _ks = self.cfg_m.CONVLSTM.KN_SIZES
        cvlstm_ks = [(_ks, _ks) for _ in range(self.output_seq)]
        cvlstm_rc_drp = self.cfg_m.CONVLSTM.RC_DRP

        use_us_drps = self.cfg_m.US.USE_DRPS
        us_drp = self.cfg_m.US.DRP
        last_filters = self.cfg_m.US.LAST_SEQ_FILTER
        last_ks = (self.cfg_m.US.LAST_KN_SIZE, self.cfg_m.US.LAST_KN_SIZE)
        last_seq_filters = self.cfg_m.US.LAST_SEQ_FILTER
        last_seq_ks = (1, self.cfg_m.US.LAST_SEQ_KN_SIZE, self.cfg_m.US.LAST_SEQ_KN_SIZE)
        
        # Conv3d Unet + ConvLSTM
        model = self._unet_convlstm(ipt_shape,
                                    ds_filters,
                                    ds_ks,
                                    ds_strides,
                                    leak_relu_alpha,
                                    use_bns,
                                    use_maxps,
                                    cvlstm_filters,
                                    cvlstm_ks,
                                    cvlstm_rc_drp,
                                    use_us_drps,
                                    us_drp,
                                    last_filters,
                                    last_ks,
                                    last_seq_filters,
                                    last_seq_ks,
                                    start_date,
                                    chn)
        opt = self.get_optimizer()
        loss = self.get_loss_func()
        metrics = [MeanAbsoluteError(), RootMeanSquaredError(), LogCoshError()]
        model.compile(optimizer=opt,
                      loss=loss,
                      metrics=metrics,
                      steps_per_execution=self.cfg_tr.STEPS_EXEC)
    
        return model

    def _downsample(self,
                    x,
                    filters,
                    kn_size,
                    name,
                    stride,
                    use_bn=True,
                    leak_relu_alpha=0.3,
                    use_maxp=True):
        """
        Description:
            DownSampling 3-D convolution block.
        Args:
            x: tf.tensor, input tensor.
            filters, int, Conv에 적용할 filters (hidden state)
            kn_size: tuple, Conv에 적용할 kernel size
            name: str, layer 이름에 붙일 것
            stride: tuple, Conv에 적용할 stride
            use_bn: bool, BatchNormalization 사용 여부, default=True
            leak_relu_alpha: float, Leakly ReLU에 적용할 alpha 값, default=0.3
            use_maxp: bool, Maxpooling 사용 여부, default=True
        Return:
            Downsampling block: tf.keras.Model
        """
        init = he_uniform()
        x = Conv3D(filters,
                   kn_size,
                   strides=stride,
                   padding="same",
                   kernel_initializer=init,
                   use_bias=True,
                   name="ds_conv_{0}".format(name))(x)
        if use_bn:
            x = BatchNormalization(name="ds_bn_{0}".format(name))(x)
        x = LeakyReLU(alpha=leak_relu_alpha, name="ds_act_{0}".format(name))(x)
        if use_maxp:
            x = MaxPooling3D(pool_size=kn_size,
                             strides=(1, 1, 1),
                             padding="same",
                             name="ds_maxp_{0}".format(name))(x)    
        return x
    
    def _upsample_2d(self,
                     x,
                     filters,
                     kn_size,
                     name,
                     seq_idx,
                     stride,
                     use_us_drps=True,
                     us_drp=0.5,
                     last=False):
        """
        Description:
            UpSampling 2-D convolution block.
        Args:
            x: tf.tensor, input tensor.
            filters, int, Conv에 적용할 filters (hidden state)
            kn_size: tuple, Conv에 적용할 kernel size
            name: str, layer 이름에 붙일 것
            seq_idx: str, layer 이름에 붙일 sequence index
            stride: tuple, Conv에 적용할 stride
            use_us_drps: bool, Dropout 사용 여부, default=True
            us_drp: float Dropout 사용할 때 적용할 비율, default=0.5
            last: bool, 마지막 layer 확인 여부, default=False
        Return:
            Upsampling block: tf.keras.Model
        """
        init = he_uniform()
        if not last:
            x = Conv2DTranspose(filters,
                                kn_size,
                                strides=stride,
                                padding="same",
                                kernel_initializer=init,
                                use_bias=True,
                                name="us_conv_{0}_{1}".format(name, seq_idx))(x)
            x = BatchNormalization(name="us_bn_{0}_{1}".format(name, seq_idx))(x)
            if use_us_drps:
                x = Dropout(us_drp, name="us_drp_{0}_{1}".format(name, seq_idx))(x)
            x = ReLU(name="us_act_{0}_{1}".format(name, seq_idx))(x)
        else:
            x = Conv2DTranspose(filters,
                                kn_size,
                                strides=stride,
                                padding="same",
                                kernel_initializer=init,
                                activation="linear",
                                name="{0}_{1}".format(name, seq_idx))(x)
        return x
    
    def _convlstm(self,
                  x,
                  filters,
                  kn_size,
                  name,
                  cv_lstm_rc_drp=0.5):
        """
        Description:
            2-D Convolutional LSTM block.
        Args:
            x: tf.tensor, input tensor.
            filters, int, ConvLSTM에 적용할 filters (hidden state)
            kn_size: tuple, ConvLSTM에 적용할 kernel size
            name: str, layer 이름에 붙일 것
            cv_lstm_rc_drp: tuple, ConvLSTM에 적용할 recurrent_dropout 비율, default=0.5
        Return:
            ConvLSTM2D block: tf.keras.Model
        """
        init = he_uniform()
        x = ConvLSTM2D(filters,
                       kn_size,
                       padding="same",
                       kernel_initializer=init,
                       use_bias=True,
                       recurrent_dropout=cv_lstm_rc_drp,
                       return_sequences=True,
                       name="conv_lstm_{0}".format(name))(x)
        x = BatchNormalization(name="conv_lstm_bn_{0}".format(name))(x)        
        return x

    def _input(self, ipt_shape):
        """
        Description:
            Input layer.
        Args:
            ipt_shape: tuple, Input shape
        Return:
            ipt: KerasTensor, 입력 layer
        """
        ipt = Input(shape=ipt_shape, name="input")
        self.log.info("      input----------- {0}".format(ipt.shape))
        return ipt

    def _ds_stack(self,
                  x,
                  ds_filters,
                  ds_ks,
                  ds_strides,
                  leak_relu_alpha,
                  use_bns,
                  use_maxps):
        """
        Description:
            DownSampling 3-D convolution stacks.
            (seq_len, h, w, filters) 를 (h, w, filters * seq_len)로 변환.
        Args:
            x: KerasTensor, 입력 layer
            ds_filters: list, Downsampling layer의 filters
            ds_ks: list, Downsampling layer의 kernel_sizes
            ds_strides: list, Downsampling layer의 strides
            leak_relu_alpha: float, Downsampling layer의 Leaky ReLU의 alpha 값
            use_bns: list, Downsampling layer의 BatchNormalization 사용여부
            use_maxps: list, Downsampling layer의 Maxpooling 사용여부
        Return:
            down_stack: list, Downsampling을 적용한 부분
            skip_stack: list, Skip connection으로 사용할 부분
        """
        down_stack = []
        skip_stack = []
        for i, (f, ks, st, bn, mxp) in enumerate(zip(ds_filters,
                                                     ds_ks,
                                                     ds_strides,
                                                     use_bns,
                                                     use_maxps)):
            x = self._downsample(x,
                                 filters=f,
                                 kn_size=ks,
                                 name="{0:02d}".format(i+1),
                                 stride=st,
                                 use_bn=bn,
                                 leak_relu_alpha=leak_relu_alpha,
                                 use_maxp=mxp)
            down_stack.append(x)
            self.log.info("      down----------- {0}".format(x.shape))
            x_concat = [Lambda(lambda z: z[:, k, ...],
                               name="ds_cvt_{0:02d}_{1:02d}".format(i+1, k+1))(x)
                        for k in range(self.input_seq)]
            x_concat = Concatenate(name="skip_{0:02d}".format(i+1))(x_concat)
            if i < len(ds_filters) - 1:
                skip_stack.append(x_concat)
                self.log.info("      down_cvt----------- {0}".format(x_concat.shape))
        skip_stack = list(reversed(skip_stack))
        return down_stack, skip_stack

    def _convlstm_list(self,
                       down_stack,
                       cv_lstm_filters,
                       cv_lstm_ks,
                       cv_lstm_rc_drp):
        """
        Description:
            2-D Convolutional LSTM list.
            ConvLSTM -> ConvLSTM 은 seq 상태로 데이터 전달.
            ConvLSTM에서 Upsampling할 부분은 seq 마지막 부분만 전달.
        Args:
            down_stack: list, Downsampling을 적용한 부분
            cv_lstm_filters: list, ConvLSTM layer의 filters
            cv_lstm_ks: list, ConvLSTM layer의 kernel_sizes
            cv_lstm_rc_drp: float, ConvLSTM layer의 recurrent_dropout 비율
        Return:
            convlstm_us_list: list, 각각 seq마다 Upsampling에 사용할 부분
        """
        x = down_stack[-1]
        convlstm_us_list = []
        for i, (f, ks) in enumerate(zip(cv_lstm_filters, cv_lstm_ks)):
            x = self._convlstm(x,
                               filters=f,
                               kn_size=ks,
                               name="{0:02d}".format(i+1),
                               cv_lstm_rc_drp=cv_lstm_rc_drp)
            x_us = Lambda(lambda x: x[:, -1, ...], name="conv_lstm_tail_{0:02d}".format(i+1))(x)
            convlstm_us_list.append(x_us)
        self.log.info("      convlstm----------- {0} * {1}".format(x.shape, self.output_seq))
        self.log.info("      convlstm_us----------- {0} * {1}".format(x_us.shape, self.output_seq))
        return convlstm_us_list
    
    def _next_us_convlstm(self,
                          us,
                          skip,
                          us_filter,
                          us_ks,
                          leak_relu_alpha,
                          use_bn,
                          use_maxp,
                          i,
                          j):
        """
        Description:
            Next ConvLSTM result upsampling.
            이전 skip connection 반영하여 개별 seq 결과마다 Upsampling 결과 산출.
            Downsampling 쪽에서의 skip connection을 1 times 밀어가며 다음 seq에 적용.
        Args:
            us: KerasTensor, Upsampling 결과
            skip: KerasTensor, Downsampling에서 전달된 skip connection
            us_filter: int, Upsampling layer의 filter
            us_ks: int, Upsampling layer의 kernel_size
            leak_relu_alpha: float, Downsampling layer의 Leaky ReLU의 alpha 값 동일하게 사용
            use_bns: bool, Upsampling의 BatchNormalization 사용여부
            use_maxps: bool, Upsampling의 Maxpooling 사용여부
            i: int, Upsampling level index
            j: int, ConvLSTM index
        Return:
            next_us: KerasTensor, Skip Connection 반영된 Upsampling 결과 (다음 Upsampling에 전달)
            next_skip: KerasTensor, 1 times 씩 밀린 skip connection (다음 ConvLSTM에 전달)
        """
        init = he_uniform()
        # 다음 convlstm으로 보낼 t ~ t+5 (skip) 와 upsampling 결과 결합
        concat = Concatenate(name="us_concat_{0:02d}_{1:02d}".format(i+1, j+1))([skip, us])
        next_us = Conv2D(us_filter,
                         us_ks,
                         padding="same",
                         kernel_initializer=init,
                         use_bias=True,
                         name="next_us_{0:02d}_{1:02d}".format(i+1, j+1))(concat)
        if use_bn:
            next_us = BatchNormalization(name="next_us_bn_{0:02d}_{1:02d}".format(i+1, j+1))(next_us)
        next_us = LeakyReLU(alpha=leak_relu_alpha,
                            name="next_us_act_{0:02d}_{1:02d}".format(i+1, j+1))(next_us)
        if use_maxp:
            next_us = MaxPooling2D(pool_size=us_ks,
                                   strides=(1, 1),
                                   padding="same",
                                   name="next_us_maxp_{0:02d}_{1:02d}".format(i+1, j+1))(next_us)
        # concat결과에서 t를 제외하고 t+1 ~ t+6만 새로운 skip으로 보냄
        next_skip = Lambda(lambda z: z[..., us_filter:],
                           name="next_skip_{0:02d}_{1:02d}".format(i+1, j+1))(concat)
        return next_us, next_skip
    
    def _us_list(self,
                 skip_stack,
                 convlstm_us_list,
                 us_filters,
                 us_ks,
                 us_strides,
                 use_us_drps,
                 us_drp,
                 leak_relu_alpha,
                 us_use_bns,
                 us_use_maxps):
        """
        Description:
            UpSampling 2-D convolution list (Sequence).
            개별 seq 결과마다 Upsampling 결과 산출.
            Downsampling 쪽에서의 skip connection을 1 times 밀어가며 다음 seq에 적용.
        Args:
            skip_stack: list, Skip connection으로 사용할 부분
            convlstm_us_list: list, 각각 seq마다 Upsampling에 사용할 부분(convlstm 바로 다음)
            us_filters: list, Upsampling layer의 filters
            us_ks: list, Upsampling layer의 kernel_sizes
            us_strides: list, Upsampling layer의 strides
            use_us_drps: list, Upsampling layer의 Dropout 사용여부
            us_drp: float, Upsampling layer의 Dropout 비율
            leak_relu_alpha: float, Downsampling layer의 Leaky ReLU의 alpha 값 동일하게 사용
            us_use_bns: list, ConvLSTM Upsampling의 BatchNormalization 사용여부
            us_use_maxps: list, ConvLSTM Upsampling의 Maxpooling 사용여부
        Return:
            convlstm_us_list: list, 각각 seq마다 Upsampling에 사용할 부분(마지막 것)
        """
        for i, (f, ks, st, d, skip, bn, maxp) in enumerate(zip(us_filters,
                                                               us_ks,
                                                               us_strides,
                                                               use_us_drps,
                                                               skip_stack,
                                                               us_use_bns,
                                                               us_use_maxps)):
            us_list = []
            for j, c_us in enumerate(convlstm_us_list):
                us = self._upsample_2d(c_us,
                                       filters=f,
                                       kn_size=ks,
                                       name="{0:02d}".format(i+1),
                                       seq_idx="{0:02d}".format(j+1),
                                       stride=st,
                                       use_us_drps=d,
                                       us_drp=us_drp)
                # 다음 convlstm 및 us 로 보낼 것 계산
                us, skip = self._next_us_convlstm(us,
                                                  skip,
                                                  f,
                                                  ks,
                                                  leak_relu_alpha,
                                                  bn,
                                                  maxp,
                                                  i,
                                                  j)
                us_list.append(us)
                self.log.debug("      next_skip----------- {0} : {1}".format(j, skip.shape))
            self.log.info("      up_concat----------- {0} * {1}".format(us.shape, self.output_seq))
            convlstm_us_list = us_list
        return convlstm_us_list
    
    def _output(self,
                ipt,
                convlstm_us_list,
                ds_strides,
                last_filters,
                last_ks,
                last_seq_filters,
                last_seq_ks):
        """
        Description:
            Last output.
            입력도 skip connection 사용하여 개별 seq 결과마다 Upsampling 결과 산출.
            Downsampling 쪽에서의 skip connection을 1 times 밀어가며 다음 seq에 적용.
            마지막에 Convolution 이용하여 Output 형상에 맞춤.
        Args:
            ipt: KerasTensor, 입력 layer
            convlstm_us_list: list, 각각 seq마다 Upsampling에 사용할 부분(마지막 것)
            ds_strides: list, Downsampling layer의 strides
            last_filters: int, 최종 Upsampling 결과 features
            last_ks: tuple, 최종 Upsampling의 kernel_size
            last_seq_filters: int, 최종 layer 결과 features
            last_seq_ks: 최종 layer의 kernel_size
        Return:
            last: KerasTensor, 최종 layer
        """
        # Output (one scene result)
        last_skip_seq_list = []
        ipt_skip = [Lambda(lambda x: x[:, k, ...],
                           name="ipt_cvt_{0:02d}".format(k+1))(ipt)
                    for k in range(self.input_seq)]
        ipt_skip = Concatenate(name="ipt_skip")(ipt_skip)
        self.log.info("      ipt_skip----------- {0}".format(ipt_skip.shape))
        for j, us in enumerate(convlstm_us_list):
            last_us = self._upsample_2d(us,
                                        filters=last_filters,
                                        kn_size=last_ks,
                                        name="last",
                                        seq_idx="{0:02d}".format(j+1),
                                        stride=ds_strides[0][1:],   # 맨처음 DS
                                        last=True)
            # 다음 convlstm으로 보낼 t ~ t+5 와 upsampling 결과 결합
            last_skip = Concatenate(name="us_concat_last_{0:02d}".format(j+1))([ipt_skip, last_us])
            last_skip_shape = [1] + list(last_skip.shape)[1:]
            last_skip_seq = Reshape(last_skip_shape,
                                    name="us_concat_last_seq_{0:02d}".format(j+1))(last_skip)
            last_skip_seq_list.append(last_skip_seq)
            self.log.debug("      last_skip----------- {0} {1}".format(j, last_skip.shape))
            # concat결과에서 t를 제외하고 t+1 ~ t+6만 skip connection 보냄
            ipt_skip = Lambda(lambda x: x[..., last_filters:], name="ipt_seq_skip_{0:02d}".format(j+1))(last_skip)
            self.log.debug("      next_ipt_skip----------- {0} {1}".format(j, ipt_skip.shape))
        self.log.info("      last_skip----------- {0} * {1}".format(last_skip.shape, self.output_seq))

        # Output (make sequence)
        init = he_uniform()
        last = Concatenate(axis=1, name="concat_last")(last_skip_seq_list)
        last = Conv3D(last_seq_filters,
                      last_seq_ks,
                      padding="same",
                      kernel_initializer=init,
                      use_bias=True,
                      name="last")(last)
        self.log.info("      output----------- {0}".format(last.shape))
        return last

    def _unet_convlstm(self,
                       ipt_shape,
                       ds_filters,
                       ds_ks,
                       ds_strides,
                       leak_relu_alpha,
                       use_bns,
                       use_maxps,
                       cv_lstm_filters,
                       cv_lstm_ks,
                       cv_lstm_rc_drp,
                       use_us_drps,
                       us_drp,
                       last_filters,
                       last_ks,
                       last_seq_filters,
                       last_seq_ks,
                       start_date,
                       chn):
        """
        Description:
            U-net 3D와 ConvLSTM 2D를 조합한 모델 생성.
        Args:
            ipt_shape: tuple, 입력 데이터 shape
            ds_filters: list, Downsampling layer의 filters
            ds_ks: list, Downsampling layer의 kernel_sizes
            ds_strides: list, Downsampling layer의 strides
            leak_relu_alpha: float, Downsampling layer의 Leaky ReLU의 alpha 값
            use_bns: list, Downsampling layer의 BatchNormalization 사용여부
            use_maxps: list, Downsampling layer의 Maxpooling 사용여부
            cv_lstm_filters: list, ConvLSTM layer의 filters
            cv_lstm_ks: list, ConvLSTM layer의 kernel_sizes
            cv_lstm_rc_drp: float, ConvLSTM layer의 recurrent_dropout 비율
            use_us_drps: list, Upsampling layer의 Dropout 사용여부
            us_drp: float, Upsampling layer의 Dropout 비율
            last_filters: int, 최종 Upsampling 결과 features
            last_ks: tuple, 최종 Upsampling의 kernel_size
            last_seq_filters: int, 최종 layer 결과 features
            last_seq_ks: 최종 layer의 kernel_size
            start_date: str, 모델 생성일자
            chn, str, 위성 채널
        Return:
            model: tf.keras.Model, Unet + ConvLSTM 모델
        """
        us_filters = list(reversed(ds_filters[:-1]))
        us_ks = [elm[1:] for elm in reversed(ds_ks[:-1])]
        us_strides = [elm[1:] for elm in reversed(ds_strides[1:])]
        us_use_bns = list(reversed(use_bns[:-1]))
        us_use_maxps = list(reversed(use_maxps[:-1]))
        
        # Input
        ipt = self._input(ipt_shape)
        # DownSampling
        down_stack, skip_stack = self._ds_stack(ipt,
                                                ds_filters,
                                                ds_ks,
                                                ds_strides,
                                                leak_relu_alpha,
                                                use_bns,
                                                use_maxps)
        # ConvLSTM
        convlstm_us_list = self._convlstm_list(down_stack,
                                               cv_lstm_filters,
                                               cv_lstm_ks,
                                               cv_lstm_rc_drp)
        # UpSampling
        convlstm_us_list = self._us_list(skip_stack,
                                         convlstm_us_list,
                                         us_filters,
                                         us_ks,
                                         us_strides,
                                         use_us_drps,
                                         us_drp,
                                         leak_relu_alpha,
                                         us_use_bns,
                                         us_use_maxps)
        # Output
        last = self._output(ipt,
                            convlstm_us_list,
                            ds_strides,
                            last_filters,
                            last_ks,
                            last_seq_filters,
                            last_seq_ks)
        
        return Model(inputs=ipt,
                     outputs=last,
                     name=self.cfg_tr.MODEL_NAME.format(chn, start_date))

    def get_optimizer(self):
        """
        Description:
            모델 학습에 사용할 Optimizer 선택.
            adam, rmsprop, sgd, adagrad 사용가능
        Args:
            None
        Returns:
            optimizer: 옵티마이저
        """
        name = self.cfg_opt.USE
        if name == "adam":
            optimizer = Adam(learning_rate=self.cfg_opt.ADAM.LR,
                            beta_1=self.cfg_opt.ADAM.BETAS[0],
                            beta_2=self.cfg_opt.ADAM.BETAS[1],
                            epsilon=float(self.cfg_opt.ADAM.EPSILON))
        elif name == "rmsprop":
            optimizer = RMSprop(learning_rate=self.cfg_opt.RMSPROP.LR,
                                rho=self.cfg_opt.RMSPROP.RHO,
                                momentum=self.cfg_opt.RMSPROP.MOMENTUM,
                                epsilon=float(self.cfg_opt.RMSPROP.EPSILON))
        elif name == "sgd":
            optimizer = SGD(learning_rate=self.cfg_opt.SGD.LR,
                            momentum=self.cfg_opt.SGD.MOMENTUM)
        elif name == "adagrad":
            optimizer = Adagrad(learning_rate=self.cfg_opt.ADAGRAD.LR,
                                initial_accumulator_value=self.cfg_opt.ADAGRAD.INIT_ACCUM,
                                epsilon=float(self.cfg_opt.ADAGRAD.EPSILON))
        else:
            log_sent = ">>> Able to use : adam, rmsprop, sgd, adagrad / Cannot use : {0}"
            self.log.info(log_sent.format(name))
            self.err_log.error(log_sent.format(name))
        return optimizer
    
    def get_loss_func(self):
        """
        Description:
            모델 학습에 사용할 Loss function 선택.
            mae, mse, logcosh, huber, pixelwise_regular 사용가능
        Args:
            None
        Returns:
            loss_func: 손실 함수
        """
        name = self.cfg_loss.USE
        if name == "mae":
            loss_func = AmpMae(amplification=self.cfg_loss.MAE.AMPLIFICATION)
        elif name == "mse":
            loss_func = AmpMse(amplification=self.cfg_loss.MSE.AMPLIFICATION)
        elif name == "huber":
            loss_func = AmpHuber(delta=self.cfg_loss.HUBER.DELTA,
                                 amplification=self.cfg_loss.HUBER.AMPLIFICATION)
        elif name == "logcosh":
            loss_func = AmpLogCosh(amplification=self.cfg_loss.LOGCOSH.AMPLIFICATION)
        elif name == "pixelwise_regular":
            loss_func = PixelWiseRegularizer(amplification=self.cfg_loss.PIXELWISE_REGULAR.AMPLIFICATION,
                                             magnitude=self.cfg_loss.PIXELWISE_REGULAR.MAGNITUDE,
                                             min_value=self.cfg_loss.PIXELWISE_REGULAR.MIN_VALUE,
                                             max_value=self.cfg_loss.PIXELWISE_REGULAR.MAX_VALUE)
        else:
            log_sent = ">>> Able to use : mae, mse, huber, logcosh, pixelwise_regular / Cannot use : {0}"
            self.log.info(log_sent.format(name))
            self.err_log.error(log_sent.format(name))
        return loss_func

    def save_model_cfg(self, chn, start_date, model):
        """
        Description:
            모델의 설정값 저장
        Args:
            chn, str, 위성 채널
            start_date: str, 모델 생성일자
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        Returns:
            None
        """
        self.log.info(">>> Save Model's Configs")
        model_name = self.cfg_tr.MODEL_NAME.format(chn, start_date)
        hist_file = "{0}.txt".format(model_name)
        hist_dir = return_dir(True, self.cfg_tr.HIST_DIR, chn)
        hist_path = os.path.join(hist_dir, hist_file)
        TRAIN_DATA_LEN = 0
        VALID_DATA_LEN = 0
        try:
            with open(hist_path, "w") as f:
                f.write("Model name: {0}\n".format(model_name))
                f.write("Used Train datas: ({0})\n".format(self.cfg_tr.DATA_TYPE))
                for f_name, f_shape in zip(self.cfg_tr.DATA_FILE, self.cfg_tr.DATA_SHAPE):
                    f.write("    File: {0} / Shape: {1}\n".format(f_name.format(chn), f_shape))
                    TRAIN_DATA_LEN += f_shape[0]
                f.write("Used Validation datas: ({0})\n".format(self.cfg_tr.DATA_TYPE))
                for f_name, f_shape in zip(self.cfg_tr.VALID_DATA_FILE, self.cfg_tr.VALID_DATA_SHAPE):
                    f.write("    File: {0} / Shape: {1}\n".format(f_name.format(chn), f_shape))
                    VALID_DATA_LEN += f_shape[0]
                TRAIN_DATA_SEQLEN = math.ceil((TRAIN_DATA_LEN - self.cfg_d.INPUT_SEQ - self.cfg_d.OUTPUT_SEQ + 1) / self.cfg_tr.BATCH_SIZE)
                VALID_DATA_SEQLEN = math.ceil((VALID_DATA_LEN - self.cfg_d.INPUT_SEQ - self.cfg_d.OUTPUT_SEQ + 1) / self.cfg_tr.BATCH_SIZE)
                f.write("Training / Validation data nums: {0} / {1}\n".format(TRAIN_DATA_LEN, VALID_DATA_LEN))
                f.write("Training / Validation sequence nums: {0} / {1}\n".format(TRAIN_DATA_SEQLEN, VALID_DATA_SEQLEN))
                f.write("Parametes: Data\n")
                self._print_cfg(self.cfg_d, f)
                f.write("Parametes: Model\n")
                self._print_cfg(self.cfg_m, f)
                f.write("Parametes: Optimizer using {0}\n".format(self.cfg_opt.USE.upper()))
                self._print_cfg(getattr(self.cfg_opt, self.cfg_opt.USE.upper()), f)
                f.write("Parametes: Loss function using {0}\n".format(self.cfg_loss.USE.upper()))
                self._print_cfg(getattr(self.cfg_loss, self.cfg_loss.USE.upper()), f)
                f.write("Parametes: Normalizer using {0}\n".format(self.cfg_norm.FUNCTION.upper()))
                self._print_cfg(getattr(self.cfg_norm, chn.upper()), f)
                f.write("Batch size: {0}\n".format(self.cfg_tr.BATCH_SIZE))
                f.write("Steps per 1 Epoch: {0}\n".format(TRAIN_DATA_SEQLEN))
                f.write("Maximum Epoch: {0}\n".format(self.cfg_tr.EPOCH))
                f.write("Early Stopping Patience: {0}\n".format(self.cfg_tr.PATIENCE))
                f.write("Steps per Execution on Model compile: {0}\n".format(self.cfg_tr.STEPS_EXEC))
                f.write("Checkpoint term: {0}\n".format(self.cfg_tr.CKPT_PERIOD))
                if self.ckpt_path is not None:
                    f.write("Import Checkpoint: {0}\n".format(self.ckpt_path.split("/")[-1]))
                model.summary(line_length=120, print_fn=lambda x: f.write(x + "\n"))   # 모델 summary
        except Exception as e:
            self.err_log.error(">>> Error on Saving Model's Parameters : {0}".format(e))
        else:
            self.log.info(">>> Save Model's Configs completely")
    
    def _print_cfg(self, cfg, f, blank=2, cnt=1):
        """
        Description:
            Namedtuple 유형의 설정값 출력
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            f: file object, 작성할 파일 객체
            subject: str, 출력 설정 중 일부만 출력하고자 하는 optimizer, normalizer
            blank: str, 앞의 공백 수
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        Returns:
            None
        """
        cnt += 1
        for field in cfg._fields:
            cfg_f = getattr(cfg, field)
            # Namedtuple 이면 depth를 더 들어감
            if isinstance(cfg_f, tuple) and hasattr(cfg_f, "_asdict") and hasattr(cfg_f, "_fields"):
                f.write("{0}{1}:\n".format(" "*blank*cnt, field))
                self._print_cfg(cfg_f, f, blank, cnt)
            else:
                f.write("{0}{1}: {2}\n".format(" "*blank*cnt, field, cfg_f))
    
    def load_ckpt(self, model):
        """
        Description:
            Checkpoint 경로가 설정되어 있으면 해당 Checkpoint 불러오기.
        Args:
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        Returns:
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델(Checkpoint 반영)
        """
        model.load_weights(self.ckpt_path)
        self.log.info(">>> Load Checkpoint from {0}".format(self.ckpt_path.split("/")[-1]))
    
    def load_model(self, model_path):
        """
        Description:
            Tensorflow 모델 불러오기.
            Custom Class 사용한 걸로 인해 일반 compile=True로 load가 안 됨.
            ==> https://www.tensorflow.org/guide/keras/save_and_serialize#registering_the_custom_object 로 custom 객체 저장방법 수정 필요
        Args:
            model_path: str, 모델 디렉토리 경로
        Returns:
            model: tf.keras.Model, Unet + ConvLSTM 모델
        """
        from tensorflow.keras.models import load_model
        model = load_model(model_path, compile=False)
        self.log.info(">>> Use model: {0}".format(model.name))
        return model


class SatPredModelV5(object):
    """Satellite Timeseries Prediction Model."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            위성(GK2A) 영상 시계열 예측 모델 init 설정.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(SatPredModelV5, self).__init__()
        self.cfg = cfg
        self.cfg_opt = cfg.PARAMS.OPT
        self.cfg_loss = cfg.PARAMS.LOSS
        self.cfg_d = cfg.PARAMS.DATA
        self.cfg_m = cfg.PARAMS.MODEL
        self.cfg_norm = cfg.PARAMS.NORMALIZER
        self.cfg_tr = cfg.TRAIN
        self.ckpt_path = cfg.TRAIN.IMPORT_CKPT_PATH
        
        self.input_seq = cfg.PARAMS.DATA.INPUT_SEQ
        self.output_seq = cfg.PARAMS.DATA.OUTPUT_SEQ
        
        self.log, self.err_log = log, err_log
    
    def __call__(self, start_date):
        """
        Description:
            모델 생성 및 관련 설정값 저장.
        Args:
            start_date: str, 모델 생성일자
        Returns:
            model: tf.keras.model 객체
        """
        chn = self.cfg_d.CHANNEL
        model = self.make_model(chn, start_date)
        self.save_model_cfg(chn, start_date, model)
        if self.ckpt_path is not None:
            self.load_ckpt(model)
        return model

    def make_model(self, chn, start_date):
        """
        Description:
            이미지 시계열 예측 모델 생성(U-net + ConvLSTM).
            입력/출력 시계열 길이 다르게 설정 가능.
            출력 시계열 길이만큼 ConvLSTM 구성 뒤 각각 Upsampling 진행 후 결합.
            V2에서 _us_list에 skip connection 연결점이 끊어져 연결점 추가.
            V3에서 use_bias=False를 모두 True로 변경.
        Args:
            chn: str, 위성 채널
            start_date: str, 모델 생성일자
        Return:
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        """
        self.log.info(">>> Make Model on {0} channel".format(chn.upper()))
        ipt_h, ipt_w, ipt_c = self.cfg_d.INPUT_SHAPE
        ipt_shape = (self.input_seq, ipt_h, ipt_w, ipt_c)
        
        ds_filters = self.cfg_m.DS.FILTERS
        ds_ks = [(1, ks, ks) for ks in self.cfg_m.DS.KN_SIZES]
        ds_strides = [(1, st, st) for st in self.cfg_m.DS.STRIDES]
        leak_relu_alpha = self.cfg_m.DS.LEAKY_RELU_ALPHA
        use_bns = self.cfg_m.DS.USE_BNS
        use_maxps = self.cfg_m.DS.USE_MAXPS
        
        cvlstm_filters = [self.cfg_m.CONVLSTM.FILTERS for _ in range(self.output_seq)]
        _ks = self.cfg_m.CONVLSTM.KN_SIZES
        cvlstm_ks = [(_ks, _ks) for _ in range(self.output_seq)]
        cvlstm_rc_drp = self.cfg_m.CONVLSTM.RC_DRP

        use_us_drps = self.cfg_m.US.USE_DRPS
        us_drp = self.cfg_m.US.DRP
        last_filters = self.cfg_m.US.LAST_SEQ_FILTER
        last_ks = (self.cfg_m.US.LAST_KN_SIZE, self.cfg_m.US.LAST_KN_SIZE)
        last_seq_filters = self.cfg_m.US.LAST_SEQ_FILTER
        last_seq_ks = (1, self.cfg_m.US.LAST_SEQ_KN_SIZE, self.cfg_m.US.LAST_SEQ_KN_SIZE)
        
        # Conv3d Unet + ConvLSTM
        model = self._unet_convlstm(ipt_shape,
                                    ds_filters,
                                    ds_ks,
                                    ds_strides,
                                    leak_relu_alpha,
                                    use_bns,
                                    use_maxps,
                                    cvlstm_filters,
                                    cvlstm_ks,
                                    cvlstm_rc_drp,
                                    use_us_drps,
                                    us_drp,
                                    last_filters,
                                    last_ks,
                                    last_seq_filters,
                                    last_seq_ks,
                                    start_date,
                                    chn)
        opt = self.get_optimizer()
        loss = self.get_loss_func()
        metrics = [MeanAbsoluteError(), RootMeanSquaredError(), LogCoshError()]
        model.compile(optimizer=opt,
                      loss=loss,
                      metrics=metrics,
                      steps_per_execution=self.cfg_tr.STEPS_EXEC)
    
        return model

    def _downsample(self,
                    x,
                    filters,
                    kn_size,
                    name,
                    stride,
                    use_bn=True,
                    leak_relu_alpha=0.3,
                    use_maxp=True):
        """
        Description:
            DownSampling 3-D convolution block.
        Args:
            x: tf.tensor, input tensor.
            filters, int, Conv에 적용할 filters (hidden state)
            kn_size: tuple, Conv에 적용할 kernel size
            name: str, layer 이름에 붙일 것
            stride: tuple, Conv에 적용할 stride
            use_bn: bool, BatchNormalization 사용 여부, default=True
            leak_relu_alpha: float, Leakly ReLU에 적용할 alpha 값, default=0.3
            use_maxp: bool, Maxpooling 사용 여부, default=True
        Return:
            Downsampling block: tf.keras.Model
        """
        init = he_uniform()
        x = Conv3D(filters,
                   kn_size,
                   strides=stride,
                   padding="same",
                   kernel_initializer=init,
                   use_bias=True,
                   name="ds_conv_{0}".format(name))(x)
        if use_bn:
            x = BatchNormalization(name="ds_bn_{0}".format(name))(x)
        x = LeakyReLU(alpha=leak_relu_alpha, name="ds_act_{0}".format(name))(x)
        if use_maxp:
            x = MaxPooling3D(pool_size=kn_size,
                             strides=(1, 1, 1),
                             padding="same",
                             name="ds_maxp_{0}".format(name))(x)    
        return x
    
    def _upsample_2d(self,
                     x,
                     filters,
                     kn_size,
                     name,
                     seq_idx,
                     stride,
                     use_us_drps=True,
                     us_drp=0.5,
                     last=False):
        """
        Description:
            UpSampling 2-D convolution block.
        Args:
            x: tf.tensor, input tensor.
            filters, int, Conv에 적용할 filters (hidden state)
            kn_size: tuple, Conv에 적용할 kernel size
            name: str, layer 이름에 붙일 것
            seq_idx: str, layer 이름에 붙일 sequence index
            stride: tuple, Conv에 적용할 stride
            use_us_drps: bool, Dropout 사용 여부, default=True
            us_drp: float Dropout 사용할 때 적용할 비율, default=0.5
            last: bool, 마지막 layer 확인 여부, default=False
        Return:
            Upsampling block: tf.keras.Model
        """
        init = he_uniform()
        if not last:
            x = Conv2DTranspose(filters,
                                kn_size,
                                strides=stride,
                                padding="same",
                                kernel_initializer=init,
                                use_bias=True,
                                name="us_conv_{0}_{1}".format(name, seq_idx))(x)
            x = BatchNormalization(name="us_bn_{0}_{1}".format(name, seq_idx))(x)
            if use_us_drps:
                x = Dropout(us_drp, name="us_drp_{0}_{1}".format(name, seq_idx))(x)
            x = ReLU(name="us_act_{0}_{1}".format(name, seq_idx))(x)
        else:
            x = Conv2DTranspose(filters,
                                kn_size,
                                strides=stride,
                                padding="same",
                                kernel_initializer=init,
                                activation="linear",
                                name="{0}_{1}".format(name, seq_idx))(x)
        return x
    
    def _convlstm(self,
                  x,
                  filters,
                  kn_size,
                  name,
                  cv_lstm_rc_drp=0.5):
        """
        Description:
            2-D Convolutional LSTM block.
        Args:
            x: tf.tensor, input tensor.
            filters, int, ConvLSTM에 적용할 filters (hidden state)
            kn_size: tuple, ConvLSTM에 적용할 kernel size
            name: str, layer 이름에 붙일 것
            cv_lstm_rc_drp: tuple, ConvLSTM에 적용할 recurrent_dropout 비율, default=0.5
        Return:
            ConvLSTM2D block: tf.keras.Model
        """
        init = he_uniform()
        x = ConvLSTM2D(filters,
                       kn_size,
                       padding="same",
                       kernel_initializer=init,
                       use_bias=True,
                       recurrent_dropout=cv_lstm_rc_drp,
                       return_sequences=True,
                       name="conv_lstm_{0}".format(name))(x)
        x = BatchNormalization(name="conv_lstm_bn_{0}".format(name))(x)        
        return x

    def _input(self, ipt_shape):
        """
        Description:
            Input layer.
        Args:
            ipt_shape: tuple, Input shape
        Return:
            ipt: KerasTensor, 입력 layer
        """
        ipt = Input(shape=ipt_shape, name="input")
        self.log.info("      input----------- {0}".format(ipt.shape))
        return ipt

    def _ds_stack(self,
                  x,
                  ds_filters,
                  ds_ks,
                  ds_strides,
                  leak_relu_alpha,
                  use_bns,
                  use_maxps):
        """
        Description:
            DownSampling 3-D convolution stacks.
            (seq_len, h, w, filters) 를 (h, w, filters * seq_len)로 변환.
        Args:
            x: KerasTensor, 입력 layer
            ds_filters: list, Downsampling layer의 filters
            ds_ks: list, Downsampling layer의 kernel_sizes
            ds_strides: list, Downsampling layer의 strides
            leak_relu_alpha: float, Downsampling layer의 Leaky ReLU의 alpha 값
            use_bns: list, Downsampling layer의 BatchNormalization 사용여부
            use_maxps: list, Downsampling layer의 Maxpooling 사용여부
        Return:
            down_stack: list, Downsampling을 적용한 부분
            skip_stack: list, Skip connection으로 사용할 부분
        """
        down_stack = []
        skip_stack = []
        for i, (f, ks, st, bn, mxp) in enumerate(zip(ds_filters,
                                                     ds_ks,
                                                     ds_strides,
                                                     use_bns,
                                                     use_maxps)):
            x = self._downsample(x,
                                 filters=f,
                                 kn_size=ks,
                                 name="{0:02d}".format(i+1),
                                 stride=st,
                                 use_bn=bn,
                                 leak_relu_alpha=leak_relu_alpha,
                                 use_maxp=mxp)
            down_stack.append(x)
            self.log.info("      down----------- {0}".format(x.shape))
            x_concat = [Lambda(lambda z: z[:, k, ...],
                               name="ds_cvt_{0:02d}_{1:02d}".format(i+1, k+1))(x)
                        for k in range(self.input_seq)]
            x_concat = Concatenate(name="skip_{0:02d}".format(i+1))(x_concat)
            if i < len(ds_filters) - 1:
                skip_stack.append(x_concat)
                self.log.info("      down_cvt----------- {0}".format(x_concat.shape))
        skip_stack = list(reversed(skip_stack))
        return down_stack, skip_stack

    def _convlstm_list(self,
                       down_stack,
                       cv_lstm_filters,
                       cv_lstm_ks,
                       cv_lstm_rc_drp):
        """
        Description:
            2-D Convolutional LSTM list.
            ConvLSTM -> ConvLSTM 은 seq 상태로 데이터 전달.
            ConvLSTM에서 Upsampling할 부분은 seq 마지막 부분만 전달.
        Args:
            down_stack: list, Downsampling을 적용한 부분
            cv_lstm_filters: list, ConvLSTM layer의 filters
            cv_lstm_ks: list, ConvLSTM layer의 kernel_sizes
            cv_lstm_rc_drp: float, ConvLSTM layer의 recurrent_dropout 비율
        Return:
            convlstm_us_list: list, 각각 seq마다 Upsampling에 사용할 부분
        """
        x = down_stack[-1]
        convlstm_us_list = []
        for i, (f, ks) in enumerate(zip(cv_lstm_filters, cv_lstm_ks)):
            x = self._convlstm(x,
                               filters=f,
                               kn_size=ks,
                               name="{0:02d}".format(i+1),
                               cv_lstm_rc_drp=cv_lstm_rc_drp)
            x_us = Lambda(lambda x: x[:, -1, ...], name="conv_lstm_tail_{0:02d}".format(i+1))(x)
            convlstm_us_list.append(x_us)
        self.log.info("      convlstm----------- {0} * {1}".format(x.shape, self.output_seq))
        self.log.info("      convlstm_us----------- {0} * {1}".format(x_us.shape, self.output_seq))
        return convlstm_us_list
    
    def _next_us_convlstm(self,
                          us,
                          skip,
                          us_filter,
                          us_ks,
                          leak_relu_alpha,
                          use_bn,
                          use_maxp,
                          i,
                          j):
        """
        Description:
            Next ConvLSTM result upsampling.
            이전 skip connection 반영하여 개별 seq 결과마다 Upsampling 결과 산출.
            Downsampling 쪽에서의 skip connection을 1 times 밀어가며 다음 seq에 적용.
        Args:
            us: KerasTensor, Upsampling 결과
            skip: KerasTensor, Downsampling에서 전달된 skip connection
            us_filter: int, Upsampling layer의 filter
            us_ks: int, Upsampling layer의 kernel_size
            leak_relu_alpha: float, Downsampling layer의 Leaky ReLU의 alpha 값 동일하게 사용
            use_bns: bool, Upsampling의 BatchNormalization 사용여부
            use_maxps: bool, Upsampling의 Maxpooling 사용여부
            i: int, Upsampling level index
            j: int, ConvLSTM index
        Return:
            next_us: KerasTensor, Skip Connection 반영된 Upsampling 결과 (다음 Upsampling에 전달)
            next_skip: KerasTensor, 1 times 씩 밀린 skip connection (다음 ConvLSTM에 전달)
        """
        init = he_uniform()
        # 다음 convlstm으로 보낼 t ~ t+5 (skip) 와 upsampling 결과 결합
        concat = Concatenate(name="us_concat_{0:02d}_{1:02d}".format(i+1, j+1))([skip, us])
        next_us = Conv2D(us_filter,
                         us_ks,
                         padding="same",
                         kernel_initializer=init,
                         use_bias=True,
                         name="next_us_{0:02d}_{1:02d}".format(i+1, j+1))(concat)
        if use_bn:
            next_us = BatchNormalization(name="next_us_bn_{0:02d}_{1:02d}".format(i+1, j+1))(next_us)
        next_us = LeakyReLU(alpha=leak_relu_alpha,
                            name="next_us_act_{0:02d}_{1:02d}".format(i+1, j+1))(next_us)
        if use_maxp:
            next_us = MaxPooling2D(pool_size=us_ks,
                                   strides=(1, 1),
                                   padding="same",
                                   name="next_us_maxp_{0:02d}_{1:02d}".format(i+1, j+1))(next_us)
        # concat결과에서 t를 제외하고 t+1 ~ t+6만 새로운 skip으로 보냄
        next_skip = Lambda(lambda z: z[..., us_filter:],
                           name="next_skip_{0:02d}_{1:02d}".format(i+1, j+1))(concat)
        return next_us, next_skip
    
    def _us_list(self,
                 skip_stack,
                 convlstm_us_list,
                 us_filters,
                 us_ks,
                 us_strides,
                 use_us_drps,
                 us_drp,
                 leak_relu_alpha,
                 us_use_bns,
                 us_use_maxps):
        """
        Description:
            UpSampling 2-D convolution list (Sequence).
            개별 seq 결과마다 Upsampling 결과 산출.
            Downsampling 쪽에서의 skip connection을 1 times 밀어가며 다음 seq에 적용.
        Args:
            skip_stack: list, Skip connection으로 사용할 부분
            convlstm_us_list: list, 각각 seq마다 Upsampling에 사용할 부분(convlstm 바로 다음)
            us_filters: list, Upsampling layer의 filters
            us_ks: list, Upsampling layer의 kernel_sizes
            us_strides: list, Upsampling layer의 strides
            use_us_drps: list, Upsampling layer의 Dropout 사용여부
            us_drp: float, Upsampling layer의 Dropout 비율
            leak_relu_alpha: float, Downsampling layer의 Leaky ReLU의 alpha 값 동일하게 사용
            us_use_bns: list, ConvLSTM Upsampling의 BatchNormalization 사용여부
            us_use_maxps: list, ConvLSTM Upsampling의 Maxpooling 사용여부
        Return:
            convlstm_us_list: list, 각각 seq마다 Upsampling에 사용할 부분(마지막 것)
        """
        for i, (f, ks, st, d, skip, bn, maxp) in enumerate(zip(us_filters,
                                                               us_ks,
                                                               us_strides,
                                                               use_us_drps,
                                                               skip_stack,
                                                               us_use_bns,
                                                               us_use_maxps)):
            us_list = []
            for j, c_us in enumerate(convlstm_us_list):
                us = self._upsample_2d(c_us,
                                       filters=f,
                                       kn_size=ks,
                                       name="{0:02d}".format(i+1),
                                       seq_idx="{0:02d}".format(j+1),
                                       stride=st,
                                       use_us_drps=d,
                                       us_drp=us_drp)
                # 다음 convlstm 및 us 로 보낼 것 계산
                us, skip = self._next_us_convlstm(us,
                                                  skip,
                                                  f,
                                                  ks,
                                                  leak_relu_alpha,
                                                  bn,
                                                  maxp,
                                                  i,
                                                  j)
                us_list.append(us)
                self.log.debug("      next_skip----------- {0} : {1}".format(j, skip.shape))
            self.log.info("      up_concat----------- {0} * {1}".format(us.shape, self.output_seq))
            convlstm_us_list = us_list
        return convlstm_us_list
    
    def _output(self,
                ipt,
                convlstm_us_list,
                ds_strides,
                last_filters,
                last_ks,
                last_seq_filters,
                last_seq_ks):
        """
        Description:
            Last output.
            입력도 skip connection 사용하여 개별 seq 결과마다 Upsampling 결과 산출.
            Downsampling 쪽에서의 skip connection을 1 times 밀어가며 다음 seq에 적용.
            마지막에 Convolution 이용하여 Output 형상에 맞춤.
        Args:
            ipt: KerasTensor, 입력 layer
            convlstm_us_list: list, 각각 seq마다 Upsampling에 사용할 부분(마지막 것)
            ds_strides: list, Downsampling layer의 strides
            last_filters: int, 최종 Upsampling 결과 features
            last_ks: tuple, 최종 Upsampling의 kernel_size
            last_seq_filters: int, 최종 layer 결과 features
            last_seq_ks: 최종 layer의 kernel_size
        Return:
            last: KerasTensor, 최종 layer
        """
        # Output (one scene result)
        last_skip_seq_list = []
        ipt_skip = [Lambda(lambda x: x[:, k, ...],
                           name="ipt_cvt_{0:02d}".format(k+1))(ipt)
                    for k in range(self.input_seq)]
        ipt_skip = Concatenate(name="ipt_skip")(ipt_skip)
        self.log.info("      ipt_skip----------- {0}".format(ipt_skip.shape))
        for j, us in enumerate(convlstm_us_list):
            last_us = self._upsample_2d(us,
                                        filters=last_filters,
                                        kn_size=last_ks,
                                        name="last",
                                        seq_idx="{0:02d}".format(j+1),
                                        stride=ds_strides[0][1:],   # 맨처음 DS
                                        last=True)
            # 다음 convlstm으로 보낼 t ~ t+5 와 upsampling 결과 결합
            last_skip = Concatenate(name="us_concat_last_{0:02d}".format(j+1))([ipt_skip, last_us])
            last_skip_shape = [1] + list(last_skip.shape)[1:]
            last_skip_seq = Reshape(last_skip_shape,
                                    name="us_concat_last_seq_{0:02d}".format(j+1))(last_skip)
            last_skip_seq_list.append(last_skip_seq)
            self.log.debug("      last_skip----------- {0} {1}".format(j, last_skip.shape))
            # concat결과에서 t를 제외하고 t+1 ~ t+6만 skip connection 보냄
            ipt_skip = Lambda(lambda x: x[..., last_filters:], name="ipt_seq_skip_{0:02d}".format(j+1))(last_skip)
            self.log.debug("      next_ipt_skip----------- {0} {1}".format(j, ipt_skip.shape))
        self.log.info("      last_skip----------- {0} * {1}".format(last_skip.shape, self.output_seq))

        # Output (make sequence)
        init = he_uniform()
        last = Concatenate(axis=1, name="concat_last")(last_skip_seq_list)
        last = Conv3D(last_seq_filters,
                      last_seq_ks,
                      padding="same",
                      kernel_initializer=init,
                      use_bias=True,
                      name="last")(last)
        self.log.info("      output----------- {0}".format(last.shape))
        return last

    def _unet_convlstm(self,
                       ipt_shape,
                       ds_filters,
                       ds_ks,
                       ds_strides,
                       leak_relu_alpha,
                       use_bns,
                       use_maxps,
                       cv_lstm_filters,
                       cv_lstm_ks,
                       cv_lstm_rc_drp,
                       use_us_drps,
                       us_drp,
                       last_filters,
                       last_ks,
                       last_seq_filters,
                       last_seq_ks,
                       start_date,
                       chn):
        """
        Description:
            U-net 3D와 ConvLSTM 2D를 조합한 모델 생성.
        Args:
            ipt_shape: tuple, 입력 데이터 shape
            ds_filters: list, Downsampling layer의 filters
            ds_ks: list, Downsampling layer의 kernel_sizes
            ds_strides: list, Downsampling layer의 strides
            leak_relu_alpha: float, Downsampling layer의 Leaky ReLU의 alpha 값
            use_bns: list, Downsampling layer의 BatchNormalization 사용여부
            use_maxps: list, Downsampling layer의 Maxpooling 사용여부
            cv_lstm_filters: list, ConvLSTM layer의 filters
            cv_lstm_ks: list, ConvLSTM layer의 kernel_sizes
            cv_lstm_rc_drp: float, ConvLSTM layer의 recurrent_dropout 비율
            use_us_drps: list, Upsampling layer의 Dropout 사용여부
            us_drp: float, Upsampling layer의 Dropout 비율
            last_filters: int, 최종 Upsampling 결과 features
            last_ks: tuple, 최종 Upsampling의 kernel_size
            last_seq_filters: int, 최종 layer 결과 features
            last_seq_ks: 최종 layer의 kernel_size
            start_date: str, 모델 생성일자
            chn, str, 위성 채널
        Return:
            model: tf.keras.Model, Unet + ConvLSTM 모델
        """
        us_filters = list(reversed(ds_filters[:-1]))
        us_ks = [elm[1:] for elm in reversed(ds_ks[:-1])]
        us_strides = [elm[1:] for elm in reversed(ds_strides[1:])]
        us_use_bns = list(reversed(use_bns[:-1]))
        us_use_maxps = list(reversed(use_maxps[:-1]))
        
        # Input
        ipt = self._input(ipt_shape)
        # DownSampling
        down_stack, skip_stack = self._ds_stack(ipt,
                                                ds_filters,
                                                ds_ks,
                                                ds_strides,
                                                leak_relu_alpha,
                                                use_bns,
                                                use_maxps)
        # ConvLSTM
        convlstm_us_list = self._convlstm_list(down_stack,
                                               cv_lstm_filters,
                                               cv_lstm_ks,
                                               cv_lstm_rc_drp)
        # UpSampling
        convlstm_us_list = self._us_list(skip_stack,
                                         convlstm_us_list,
                                         us_filters,
                                         us_ks,
                                         us_strides,
                                         use_us_drps,
                                         us_drp,
                                         leak_relu_alpha,
                                         us_use_bns,
                                         us_use_maxps)
        # Output
        last = self._output(ipt,
                            convlstm_us_list,
                            ds_strides,
                            last_filters,
                            last_ks,
                            last_seq_filters,
                            last_seq_ks)
        
        return Model(inputs=ipt,
                     outputs=last,
                     name=self.cfg_tr.MODEL_NAME.format(chn, start_date))

    def get_optimizer(self):
        """
        Description:
            모델 학습에 사용할 Optimizer 선택.
            adam, rmsprop, sgd, adagrad 사용가능
        Args:
            None
        Returns:
            optimizer: 옵티마이저
        """
        name = self.cfg_opt.USE
        if name == "adam":
            optimizer = Adam(learning_rate=self.cfg_opt.ADAM.LR,
                            beta_1=self.cfg_opt.ADAM.BETAS[0],
                            beta_2=self.cfg_opt.ADAM.BETAS[1],
                            epsilon=float(self.cfg_opt.ADAM.EPSILON))
        elif name == "rmsprop":
            optimizer = RMSprop(learning_rate=self.cfg_opt.RMSPROP.LR,
                                rho=self.cfg_opt.RMSPROP.RHO,
                                momentum=self.cfg_opt.RMSPROP.MOMENTUM,
                                epsilon=float(self.cfg_opt.RMSPROP.EPSILON))
        elif name == "sgd":
            optimizer = SGD(learning_rate=self.cfg_opt.SGD.LR,
                            momentum=self.cfg_opt.SGD.MOMENTUM)
        elif name == "adagrad":
            optimizer = Adagrad(learning_rate=self.cfg_opt.ADAGRAD.LR,
                                initial_accumulator_value=self.cfg_opt.ADAGRAD.INIT_ACCUM,
                                epsilon=float(self.cfg_opt.ADAGRAD.EPSILON))
        else:
            log_sent = ">>> Able to use : adam, rmsprop, sgd, adagrad / Cannot use : {0}"
            self.log.info(log_sent.format(name))
            self.err_log.error(log_sent.format(name))
        return optimizer
    
    def get_loss_func(self):
        """
        Description:
            모델 학습에 사용할 Loss function 선택.
            mae, mse, logcosh, huber, pixelwise_regular 사용가능
        Args:
            None
        Returns:
            loss_func: 손실 함수
        """
        name = self.cfg_loss.USE
        if name == "mae":
            loss_func = AmpMae(amplification=self.cfg_loss.MAE.AMPLIFICATION)
        elif name == "mse":
            loss_func = AmpMse(amplification=self.cfg_loss.MSE.AMPLIFICATION)
        elif name == "huber":
            loss_func = AmpHuber(delta=self.cfg_loss.HUBER.DELTA,
                                 amplification=self.cfg_loss.HUBER.AMPLIFICATION)
        elif name == "logcosh":
            loss_func = AmpLogCosh(amplification=self.cfg_loss.LOGCOSH.AMPLIFICATION)
        elif name == "pixelwise_regular":
            loss_func = PixelWiseRegularizer(amplification=self.cfg_loss.PIXELWISE_REGULAR.AMPLIFICATION,
                                             magnitude=self.cfg_loss.PIXELWISE_REGULAR.MAGNITUDE,
                                             min_value=self.cfg_loss.PIXELWISE_REGULAR.MIN_VALUE,
                                             max_value=self.cfg_loss.PIXELWISE_REGULAR.MAX_VALUE)
        else:
            log_sent = ">>> Able to use : mae, mse, huber, logcosh, pixelwise_regular / Cannot use : {0}"
            self.log.info(log_sent.format(name))
            self.err_log.error(log_sent.format(name))
        return loss_func

    def save_model_cfg(self, chn, start_date, model):
        """
        Description:
            모델의 설정값 저장
        Args:
            chn, str, 위성 채널
            start_date: str, 모델 생성일자
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        Returns:
            None
        """
        self.log.info(">>> Save Model's Configs")
        model_name = self.cfg_tr.MODEL_NAME.format(chn, start_date)
        hist_file = "{0}.txt".format(model_name)
        hist_dir = return_dir(True, self.cfg_tr.HIST_DIR, chn)
        hist_path = os.path.join(hist_dir, hist_file)
        TRAIN_DATA_LEN = 0
        VALID_DATA_LEN = 0
        try:
            with open(hist_path, "w") as f:
                f.write("Model name: {0}\n".format(model_name))
                f.write("Used Train datas: ({0})\n".format(self.cfg_tr.DATA_TYPE))
                for f_name, f_shape in zip(self.cfg_tr.DATA_FILE, self.cfg_tr.DATA_SHAPE):
                    f.write("    File: {0} / Shape: {1}\n".format(f_name.format(chn), f_shape))
                    TRAIN_DATA_LEN += f_shape[0]
                f.write("Used Validation datas: ({0})\n".format(self.cfg_tr.DATA_TYPE))
                for f_name, f_shape in zip(self.cfg_tr.VALID_DATA_FILE, self.cfg_tr.VALID_DATA_SHAPE):
                    f.write("    File: {0} / Shape: {1}\n".format(f_name.format(chn), f_shape))
                    VALID_DATA_LEN += f_shape[0]
                TRAIN_DATA_SEQLEN = math.ceil((TRAIN_DATA_LEN - self.cfg_d.INPUT_SEQ - self.cfg_d.OUTPUT_SEQ + 1) / self.cfg_tr.BATCH_SIZE)
                VALID_DATA_SEQLEN = math.ceil((VALID_DATA_LEN - self.cfg_d.INPUT_SEQ - self.cfg_d.OUTPUT_SEQ + 1) / self.cfg_tr.BATCH_SIZE)
                f.write("Training / Validation data nums: {0} / {1}\n".format(TRAIN_DATA_LEN, VALID_DATA_LEN))
                f.write("Training / Validation sequence nums: {0} / {1}\n".format(TRAIN_DATA_SEQLEN, VALID_DATA_SEQLEN))
                f.write("Parametes: Data\n")
                self._print_cfg(self.cfg_d, f)
                f.write("Parametes: Model\n")
                self._print_cfg(self.cfg_m, f)
                f.write("Parametes: Optimizer using {0}\n".format(self.cfg_opt.USE.upper()))
                self._print_cfg(getattr(self.cfg_opt, self.cfg_opt.USE.upper()), f)
                f.write("Parametes: Loss function using {0}\n".format(self.cfg_loss.USE.upper()))
                self._print_cfg(getattr(self.cfg_loss, self.cfg_loss.USE.upper()), f)
                f.write("Parametes: Normalizer using {0}\n".format(self.cfg_norm.FUNCTION.upper()))
                self._print_cfg(getattr(self.cfg_norm, chn.upper()), f)
                f.write("Batch size: {0}\n".format(self.cfg_tr.BATCH_SIZE))
                f.write("Steps per 1 Epoch: {0}\n".format(TRAIN_DATA_SEQLEN))
                f.write("Maximum Epoch: {0}\n".format(self.cfg_tr.EPOCH))
                f.write("Early Stopping Patience: {0}\n".format(self.cfg_tr.PATIENCE))
                f.write("Steps per Execution on Model compile: {0}\n".format(self.cfg_tr.STEPS_EXEC))
                f.write("Checkpoint term: {0}\n".format(self.cfg_tr.CKPT_PERIOD))
                if self.ckpt_path is not None:
                    f.write("Import Checkpoint: {0}\n".format(self.ckpt_path.split("/")[-1]))
                model.summary(line_length=120, print_fn=lambda x: f.write(x + "\n"))   # 모델 summary
        except Exception as e:
            self.err_log.error(">>> Error on Saving Model's Parameters : {0}".format(e))
        else:
            self.log.info(">>> Save Model's Configs completely")
    
    def _print_cfg(self, cfg, f, blank=2, cnt=1):
        """
        Description:
            Namedtuple 유형의 설정값 출력
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            f: file object, 작성할 파일 객체
            subject: str, 출력 설정 중 일부만 출력하고자 하는 optimizer, normalizer
            blank: str, 앞의 공백 수
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        Returns:
            None
        """
        cnt += 1
        for field in cfg._fields:
            cfg_f = getattr(cfg, field)
            # Namedtuple 이면 depth를 더 들어감
            if isinstance(cfg_f, tuple) and hasattr(cfg_f, "_asdict") and hasattr(cfg_f, "_fields"):
                f.write("{0}{1}:\n".format(" "*blank*cnt, field))
                self._print_cfg(cfg_f, f, blank, cnt)
            else:
                f.write("{0}{1}: {2}\n".format(" "*blank*cnt, field, cfg_f))
    
    def load_ckpt(self, model):
        """
        Description:
            Checkpoint 경로가 설정되어 있으면 해당 Checkpoint 불러오기.
        Args:
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        Returns:
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델(Checkpoint 반영)
        """
        model.load_weights(self.ckpt_path)
        self.log.info(">>> Load Checkpoint from {0}".format(self.ckpt_path.split("/")[-1]))
    
    def load_model(self, model_path):
        """
        Description:
            Tensorflow 모델 불러오기.
            Custom Class 사용한 걸로 인해 일반 compile=True로 load가 안 됨.
            ==> https://www.tensorflow.org/guide/keras/save_and_serialize#registering_the_custom_object 로 custom 객체 저장방법 수정 필요
        Args:
            model_path: str, 모델 디렉토리 경로
        Returns:
            model: tf.keras.Model, Unet + ConvLSTM 모델
        """
        from tensorflow.keras.models import load_model
        model = load_model(model_path, compile=False)
        self.log.info(">>> Use model: {0}".format(model.name))
        return model


class SatPredModelV7(object):
    """Satellite Timeseries Prediction Model."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            위성(GK2A) 영상 시계열 예측 모델 init 설정.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(SatPredModelV7, self).__init__()
        self.cfg = cfg
        self.cfg_opt = cfg.PARAMS.OPT
        self.cfg_loss = cfg.PARAMS.LOSS
        self.cfg_d = cfg.PARAMS.DATA
        self.cfg_m = cfg.PARAMS.MODEL
        self.cfg_norm = cfg.PARAMS.NORMALIZER
        self.cfg_tr = cfg.TRAIN
        self.ckpt_path = cfg.TRAIN.IMPORT_CKPT_PATH
        
        self.input_seq = cfg.PARAMS.DATA.INPUT_SEQ
        self.output_seq = cfg.PARAMS.DATA.OUTPUT_SEQ
        
        self.log, self.err_log = log, err_log
    
    def __call__(self, start_date):
        """
        Description:
            모델 생성 및 관련 설정값 저장.
        Args:
            start_date: str, 모델 생성일자
        Returns:
            model: tf.keras.model 객체
        """
        chn = self.cfg_d.CHANNEL
        model = self.make_model(chn, start_date)
        self.save_model_cfg(chn, start_date, model)
        if self.ckpt_path is not None:
            self.load_ckpt(model)
        return model

    def make_model(self, chn, start_date):
        """
        Description:
            이미지 시계열 예측 모델 생성(U-net + ConvLSTM).
            입력/출력 시계열 길이 다르게 설정 가능.
            출력 시계열 길이만큼 ConvLSTM 구성 뒤 각각 Upsampling 진행 후 결합.
            V3에서 output_seq 만큼 ConvLSTM 통과하는게 아니라 RepeatVector 적용.
        Args:
            chn: str, 위성 채널
            start_date: str, 모델 생성일자
        Return:
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        """
        self.log.info(">>> Make Model on {0} channel".format(chn.upper()))
        ipt_h, ipt_w, ipt_c = self.cfg_d.INPUT_SHAPE
        ipt_shape = (self.input_seq, ipt_h, ipt_w, ipt_c)
        
        ds_filters = self.cfg_m.DS.FILTERS
        ds_ks = [(1, ks, ks) for ks in self.cfg_m.DS.KN_SIZES]
        ds_strides = [(1, st, st) for st in self.cfg_m.DS.STRIDES]
        leak_relu_alpha = self.cfg_m.DS.LEAKY_RELU_ALPHA
        use_bns = self.cfg_m.DS.USE_BNS
        use_maxps = self.cfg_m.DS.USE_MAXPS
        
        cvlstm_filters = self.cfg_m.CONVLSTM.FILTERS
        _ks = self.cfg_m.CONVLSTM.KN_SIZES
        cvlstm_ks = (_ks, _ks)
        cvlstm_rc_drp = self.cfg_m.CONVLSTM.RC_DRP

        use_us_drps = self.cfg_m.US.USE_DRPS
        us_drp = self.cfg_m.US.DRP
        last_filters = self.cfg_m.US.LAST_SEQ_FILTER
        last_ks = (self.cfg_m.US.LAST_KN_SIZE, self.cfg_m.US.LAST_KN_SIZE)
        last_seq_filters = self.cfg_m.US.LAST_SEQ_FILTER
        last_seq_ks = (1, self.cfg_m.US.LAST_SEQ_KN_SIZE, self.cfg_m.US.LAST_SEQ_KN_SIZE)
        
        # Conv3d Unet + ConvLSTM
        model = self._unet_convlstm(ipt_shape,
                                    ds_filters,
                                    ds_ks,
                                    ds_strides,
                                    leak_relu_alpha,
                                    use_bns,
                                    use_maxps,
                                    cvlstm_filters,
                                    cvlstm_ks,
                                    cvlstm_rc_drp,
                                    use_us_drps,
                                    us_drp,
                                    last_filters,
                                    last_ks,
                                    last_seq_filters,
                                    last_seq_ks,
                                    start_date,
                                    chn)
        opt = self.get_optimizer()
        loss = self.get_loss_func()
        metrics = [MeanAbsoluteError(), RootMeanSquaredError(), LogCoshError()]
        model.compile(optimizer=opt,
                      loss=loss,
                      metrics=metrics,
                      steps_per_execution=self.cfg_tr.STEPS_EXEC)
    
        return model

    def _downsample(self,
                    x,
                    filters,
                    kn_size,
                    name,
                    stride,
                    use_bn=True,
                    leak_relu_alpha=0.3,
                    use_maxp=True):
        """
        Description:
            DownSampling 3-D convolution block.
        Args:
            x: tf.tensor, input tensor.
            filters, int, Conv에 적용할 filters (hidden state)
            kn_size: tuple, Conv에 적용할 kernel size
            name: str, layer 이름에 붙일 것
            stride: tuple, Conv에 적용할 stride
            use_bn: bool, BatchNormalization 사용 여부, default=True
            leak_relu_alpha: float, Leakly ReLU에 적용할 alpha 값, default=0.3
            use_maxp: bool, Maxpooling 사용 여부, default=True
        Return:
            Downsampling block: tf.keras.Model
        """
        init = he_uniform()
        x = Conv3D(filters,
                   kn_size,
                   strides=stride,
                   padding="same",
                   kernel_initializer=init,
                   use_bias=False,
                   name="ds_conv_{0}".format(name))(x)
        if use_bn:
            x = BatchNormalization(name="ds_bn_{0}".format(name))(x)
        x = LeakyReLU(alpha=leak_relu_alpha, name="ds_act_{0}".format(name))(x)
        if use_maxp:
            x = MaxPooling3D(pool_size=kn_size,
                             strides=(1, 1, 1),
                             padding="same",
                             name="ds_maxp_{0}".format(name))(x)    
        return x
    
    def _upsample_2d(self,
                     x,
                     filters,
                     kn_size,
                     name,
                     seq_idx,
                     stride,
                     use_us_drps=True,
                     us_drp=0.5,
                     last=False):
        """
        Description:
            UpSampling 2-D convolution block.
        Args:
            x: tf.tensor, input tensor.
            filters, int, Conv에 적용할 filters (hidden state)
            kn_size: tuple, Conv에 적용할 kernel size
            name: str, layer 이름에 붙일 것
            seq_idx: str, layer 이름에 붙일 sequence index
            stride: tuple, Conv에 적용할 stride
            use_us_drps: bool, Dropout 사용 여부, default=True
            us_drp: float Dropout 사용할 때 적용할 비율, default=0.5
            last: bool, 마지막 layer 확인 여부, default=False
        Return:
            Upsampling block: tf.keras.Model
        """
        init = he_uniform()
        if not last:
            x = Conv2DTranspose(filters,
                                kn_size,
                                strides=stride,
                                padding="same",
                                kernel_initializer=init,
                                use_bias=False,
                                name="us_conv_{0}_{1}".format(name, seq_idx))(x)
            x = BatchNormalization(name="us_bn_{0}_{1}".format(name, seq_idx))(x)
            if use_us_drps:
                x = Dropout(us_drp, name="us_drp_{0}_{1}".format(name, seq_idx))(x)
            x = ReLU(name="us_act_{0}_{1}".format(name, seq_idx))(x)
        else:
            x = Conv2DTranspose(filters,
                                kn_size,
                                strides=stride,
                                padding="same",
                                kernel_initializer=init,
                                activation="linear",
                                name="{0}_{1}".format(name, seq_idx))(x)
        return x
    
    def _convlstm(self,
                  x,
                  filters,
                  kn_size,
                  name,
                  cv_lstm_rc_drp=0.5):
        """
        Description:
            2-D Convolutional LSTM block.
        Args:
            x: tf.tensor, input tensor.
            filters, int, ConvLSTM에 적용할 filters (hidden state)
            kn_size: tuple, ConvLSTM에 적용할 kernel size
            name: str, layer 이름에 붙일 것
            cv_lstm_rc_drp: tuple, ConvLSTM에 적용할 recurrent_dropout 비율, default=0.5
        Return:
            ConvLSTM2D block: tf.keras.Model
        """
        init = he_uniform()
        x = ConvLSTM2D(filters,
                       kn_size,
                       padding="same",
                       kernel_initializer=init,
                       use_bias=True,
                       recurrent_dropout=cv_lstm_rc_drp,
                       return_sequences=False,
                       name="conv_lstm_{0}".format(name))(x)
        x = BatchNormalization(name="conv_lstm_bn_{0}".format(name))(x)        
        return x

    def _input(self, ipt_shape):
        """
        Description:
            Input layer.
        Args:
            ipt_shape: tuple, Input shape
        Return:
            ipt: KerasTensor, 입력 layer
        """
        ipt = Input(shape=ipt_shape, name="input")
        self.log.info("      input----------- {0}".format(ipt.shape))
        return ipt

    def _ds_stack(self,
                  x,
                  ds_filters,
                  ds_ks,
                  ds_strides,
                  leak_relu_alpha,
                  use_bns,
                  use_maxps):
        """
        Description:
            DownSampling 3-D convolution stacks.
            (seq_len, h, w, filters) 를 (h, w, filters * seq_len)로 변환.
        Args:
            x: KerasTensor, 입력 layer
            ds_filters: list, Downsampling layer의 filters
            ds_ks: list, Downsampling layer의 kernel_sizes
            ds_strides: list, Downsampling layer의 strides
            leak_relu_alpha: float, Downsampling layer의 Leaky ReLU의 alpha 값
            use_bns: list, Downsampling layer의 BatchNormalization 사용여부
            use_maxps: list, Downsampling layer의 Maxpooling 사용여부
        Return:
            down_stack: list, Downsampling을 적용한 부분
            skip_stack: list, Skip connection으로 사용할 부분
        """
        down_stack = []
        skip_stack = []
        for i, (f, ks, st, bn, mxp) in enumerate(zip(ds_filters,
                                                     ds_ks,
                                                     ds_strides,
                                                     use_bns,
                                                     use_maxps)):
            x = self._downsample(x,
                                 filters=f,
                                 kn_size=ks,
                                 name="{0:02d}".format(i+1),
                                 stride=st,
                                 use_bn=bn,
                                 leak_relu_alpha=leak_relu_alpha,
                                 use_maxp=mxp)
            down_stack.append(x)
            self.log.info("      down----------- {0}".format(x.shape))
            x_concat = [Lambda(lambda z: z[:, k, ...],
                               name="ds_cvt_{0:02d}_{1:02d}".format(i+1, k+1))(x)
                        for k in range(self.input_seq)]
            x_concat = Concatenate(name="skip_{0:02d}".format(i+1))(x_concat)
            if i < len(ds_filters) - 1:
                skip_stack.append(x_concat)
                self.log.info("      down_cvt----------- {0}".format(x_concat.shape))
        skip_stack = list(reversed(skip_stack))
        return down_stack, skip_stack

    def _convlstm_list(self,
                       down_stack,
                       cv_lstm_filters,
                       cv_lstm_ks,
                       cv_lstm_rc_drp):
        """
        Description:
            2-D Convolutional LSTM list.
            ConvLSTM 다음에 RepeatVector로 복제 후 GaussianNoise 추가.
            ConvLSTM에서 Upsampling할 부분은 seq 마지막 부분만 전달.
        Args:
            down_stack: list, Downsampling을 적용한 부분
            cv_lstm_filters: int, ConvLSTM layer의 filters
            cv_lstm_ks: tuple, ConvLSTM layer의 kernel_sizes
            cv_lstm_rc_drp: float, ConvLSTM layer의 recurrent_dropout 비율
        Return:
            convlstm_us_list: list, 각각 seq마다 Upsampling에 사용할 부분
        """
        x = down_stack[-1]
        x = self._convlstm(x,
                           filters=cv_lstm_filters,
                           kn_size=cv_lstm_ks,
                           name="once",
                           cv_lstm_rc_drp=cv_lstm_rc_drp)
        # Repeat Vector로 복제 후, Gaussian Noise 추가
        x_shape = x.shape[1:]
        x = Reshape((-1,), name="before_repeat")(x)
        x = RepeatVector(self.output_seq, name="conv_lstm_repeat")(x)
        x = Reshape((-1, x_shape[0], x_shape[1], x_shape[2]), name="after_repeat")(x)
        convlstm_us_list = []
        for i in range(self.output_seq):
            x_us = Lambda(lambda y: y[:, -1, ...], name="conv_lstm_tail_{0:02d}".format(i+1))(x)
            x_us_addnoise = GaussianNoise(stddev=0.1,
                                          name="conv_lstm_{0:02d}".format(i+1))(x_us)
            convlstm_us_list.append(x_us_addnoise)
        self.log.info("      convlstm----------- {0} * {1}".format(x_us.shape, self.output_seq))
        self.log.info("      convlstm_us----------- {0} * {1}".format(x_us_addnoise.shape, self.output_seq))
        return convlstm_us_list
    
    def _next_us_convlstm(self,
                          us,
                          skip,
                          us_filter,
                          us_ks,
                          leak_relu_alpha,
                          use_bn,
                          use_maxp,
                          i,
                          j):
        """
        Description:
            Next ConvLSTM result upsampling.
            이전 skip connection 반영하여 개별 seq 결과마다 Upsampling 결과 산출.
            Downsampling 쪽에서의 skip connection을 1 times 밀어가며 다음 seq에 적용.
        Args:
            us: KerasTensor, Upsampling 결과
            skip: KerasTensor, Downsampling에서 전달된 skip connection
            us_filter: int, Upsampling layer의 filter
            us_ks: int, Upsampling layer의 kernel_size
            leak_relu_alpha: float, Downsampling layer의 Leaky ReLU의 alpha 값 동일하게 사용
            use_bns: bool, Upsampling의 BatchNormalization 사용여부
            use_maxps: bool, Upsampling의 Maxpooling 사용여부
            i: int, Upsampling level index
            j: int, ConvLSTM index
        Return:
            next_us: KerasTensor, Skip Connection 반영된 Upsampling 결과 (다음 Upsampling에 전달)
            next_skip: KerasTensor, 1 times 씩 밀린 skip connection (다음 ConvLSTM에 전달)
        """
        init = he_uniform()
        # 다음 convlstm으로 보낼 t ~ t+5 (skip) 와 upsampling 결과 결합
        concat = Concatenate(name="us_concat_{0:02d}_{1:02d}".format(i+1, j+1))([skip, us])
        next_us = Conv2D(us_filter,
                         us_ks,
                         padding="same",
                         kernel_initializer=init,
                         use_bias=False,
                         name="next_us_{0:02d}_{1:02d}".format(i+1, j+1))(concat)
        if use_bn:
            next_us = BatchNormalization(name="next_us_bn_{0:02d}_{1:02d}".format(i+1, j+1))(next_us)
        next_us = LeakyReLU(alpha=leak_relu_alpha,
                            name="next_us_act_{0:02d}_{1:02d}".format(i+1, j+1))(next_us)
        if use_maxp:
            next_us = MaxPooling2D(pool_size=us_ks,
                                   strides=(1, 1),
                                   padding="same",
                                   name="next_us_maxp_{0:02d}_{1:02d}".format(i+1, j+1))(next_us)
        # concat결과에서 t를 제외하고 t+1 ~ t+6만 새로운 skip으로 보냄
        next_skip = Lambda(lambda z: z[..., us_filter:],
                           name="next_skip_{0:02d}_{1:02d}".format(i+1, j+1))(concat)
        return next_us, next_skip
    
    def _us_list(self,
                 skip_stack,
                 convlstm_us_list,
                 us_filters,
                 us_ks,
                 us_strides,
                 use_us_drps,
                 us_drp,
                 leak_relu_alpha,
                 us_use_bns,
                 us_use_maxps):
        """
        Description:
            UpSampling 2-D convolution list (Sequence).
            개별 seq 결과마다 Upsampling 결과 산출.
            Downsampling 쪽에서의 skip connection을 1 times 밀어가며 다음 seq에 적용.
        Args:
            skip_stack: list, Skip connection으로 사용할 부분
            convlstm_us_list: list, 각각 seq마다 Upsampling에 사용할 부분(convlstm 바로 다음)
            us_filters: list, Upsampling layer의 filters
            us_ks: list, Upsampling layer의 kernel_sizes
            us_strides: list, Upsampling layer의 strides
            use_us_drps: list, Upsampling layer의 Dropout 사용여부
            us_drp: float, Upsampling layer의 Dropout 비율
            leak_relu_alpha: float, Downsampling layer의 Leaky ReLU의 alpha 값 동일하게 사용
            us_use_bns: list, ConvLSTM Upsampling의 BatchNormalization 사용여부
            us_use_maxps: list, ConvLSTM Upsampling의 Maxpooling 사용여부
        Return:
            convlstm_us_list: list, 각각 seq마다 Upsampling에 사용할 부분(마지막 것)
        """
        for i, (f, ks, st, d, skip, bn, maxp) in enumerate(zip(us_filters,
                                                               us_ks,
                                                               us_strides,
                                                               use_us_drps,
                                                               skip_stack,
                                                               us_use_bns,
                                                               us_use_maxps)):
            us_list = []
            for j, c_us in enumerate(convlstm_us_list):
                us = self._upsample_2d(c_us,
                                       filters=f,
                                       kn_size=ks,
                                       name="{0:02d}".format(i+1),
                                       seq_idx="{0:02d}".format(j+1),
                                       stride=st,
                                       use_us_drps=d,
                                       us_drp=us_drp)
                # 다음 convlstm 및 us 로 보낼 것 계산
                us, skip = self._next_us_convlstm(us,
                                                  skip,
                                                  f,
                                                  ks,
                                                  leak_relu_alpha,
                                                  bn,
                                                  maxp,
                                                  i,
                                                  j)
                us_list.append(us)
                self.log.debug("      next_skip----------- {0} : {1}".format(j, skip.shape))
            self.log.info("      up_concat----------- {0} * {1}".format(us.shape, self.output_seq))
            convlstm_us_list = us_list
        return convlstm_us_list
    
    def _output(self,
                ipt,
                convlstm_us_list,
                ds_strides,
                last_filters,
                last_ks,
                last_seq_filters,
                last_seq_ks):
        """
        Description:
            Last output.
            입력도 skip connection 사용하여 개별 seq 결과마다 Upsampling 결과 산출.
            Downsampling 쪽에서의 skip connection을 1 times 밀어가며 다음 seq에 적용.
            마지막에 Convolution 이용하여 Output 형상에 맞춤.
        Args:
            ipt: KerasTensor, 입력 layer
            convlstm_us_list: list, 각각 seq마다 Upsampling에 사용할 부분(마지막 것)
            ds_strides: list, Downsampling layer의 strides
            last_filters: int, 최종 Upsampling 결과 features
            last_ks: tuple, 최종 Upsampling의 kernel_size
            last_seq_filters: int, 최종 layer 결과 features
            last_seq_ks: 최종 layer의 kernel_size
        Return:
            last: KerasTensor, 최종 layer
        """
        # Output (one scene result)
        last_skip_seq_list = []
        ipt_skip = [Lambda(lambda x: x[:, k, ...],
                           name="ipt_cvt_{0:02d}".format(k+1))(ipt)
                    for k in range(self.input_seq)]
        ipt_skip = Concatenate(name="ipt_skip")(ipt_skip)
        self.log.info("      ipt_skip----------- {0}".format(ipt_skip.shape))
        for j, us in enumerate(convlstm_us_list):
            last_us = self._upsample_2d(us,
                                        filters=last_filters,
                                        kn_size=last_ks,
                                        name="last",
                                        seq_idx="{0:02d}".format(j+1),
                                        stride=ds_strides[0][1:],   # 맨처음 DS
                                        last=True)
            # 다음 convlstm으로 보낼 t ~ t+5 와 upsampling 결과 결합
            last_skip = Concatenate(name="us_concat_last_{0:02d}".format(j+1))([ipt_skip, last_us])
            last_skip_shape = [1] + list(last_skip.shape)[1:]
            last_skip_seq = Reshape(last_skip_shape,
                                    name="us_concat_last_seq_{0:02d}".format(j+1))(last_skip)
            last_skip_seq_list.append(last_skip_seq)
            self.log.debug("      last_skip----------- {0} {1}".format(j, last_skip.shape))
            # concat결과에서 t를 제외하고 t+1 ~ t+6만 skip connection 보냄
            ipt_skip = Lambda(lambda x: x[..., last_filters:], name="ipt_seq_skip_{0:02d}".format(j+1))(last_skip)
            self.log.debug("      next_ipt_skip----------- {0} {1}".format(j, ipt_skip.shape))
        self.log.info("      last_skip----------- {0} * {1}".format(last_skip.shape, self.output_seq))

        # Output (make sequence)
        init = he_uniform()
        last = Concatenate(axis=1, name="concat_last")(last_skip_seq_list)
        last = Conv3D(last_seq_filters,
                      last_seq_ks,
                      padding="same",
                      kernel_initializer=init,
                      use_bias=False,
                      name="last")(last)
        self.log.info("      output----------- {0}".format(last.shape))
        return last

    def _unet_convlstm(self,
                       ipt_shape,
                       ds_filters,
                       ds_ks,
                       ds_strides,
                       leak_relu_alpha,
                       use_bns,
                       use_maxps,
                       cv_lstm_filters,
                       cv_lstm_ks,
                       cv_lstm_rc_drp,
                       use_us_drps,
                       us_drp,
                       last_filters,
                       last_ks,
                       last_seq_filters,
                       last_seq_ks,
                       start_date,
                       chn):
        """
        Description:
            U-net 3D와 ConvLSTM 2D를 조합한 모델 생성.
        Args:
            ipt_shape: tuple, 입력 데이터 shape
            ds_filters: list, Downsampling layer의 filters
            ds_ks: list, Downsampling layer의 kernel_sizes
            ds_strides: list, Downsampling layer의 strides
            leak_relu_alpha: float, Downsampling layer의 Leaky ReLU의 alpha 값
            use_bns: list, Downsampling layer의 BatchNormalization 사용여부
            use_maxps: list, Downsampling layer의 Maxpooling 사용여부
            cv_lstm_filters: int, ConvLSTM layer의 filters
            cv_lstm_ks: tuple, ConvLSTM layer의 kernel_sizes
            cv_lstm_rc_drp: float, ConvLSTM layer의 recurrent_dropout 비율
            use_us_drps: list, Upsampling layer의 Dropout 사용여부
            us_drp: float, Upsampling layer의 Dropout 비율
            last_filters: int, 최종 Upsampling 결과 features
            last_ks: tuple, 최종 Upsampling의 kernel_size
            last_seq_filters: int, 최종 layer 결과 features
            last_seq_ks: 최종 layer의 kernel_size
            start_date: str, 모델 생성일자
            chn, str, 위성 채널
        Return:
            model: tf.keras.Model, Unet + ConvLSTM 모델
        """
        us_filters = list(reversed(ds_filters[:-1]))
        us_ks = [elm[1:] for elm in reversed(ds_ks[:-1])]
        us_strides = [elm[1:] for elm in reversed(ds_strides[1:])]
        us_use_bns = list(reversed(use_bns[:-1]))
        us_use_maxps = list(reversed(use_maxps[:-1]))
        
        # Input
        ipt = self._input(ipt_shape)
        # DownSampling
        down_stack, skip_stack = self._ds_stack(ipt,
                                                ds_filters,
                                                ds_ks,
                                                ds_strides,
                                                leak_relu_alpha,
                                                use_bns,
                                                use_maxps)
        # ConvLSTM
        convlstm_us_list = self._convlstm_list(down_stack,
                                               cv_lstm_filters,
                                               cv_lstm_ks,
                                               cv_lstm_rc_drp)
        # UpSampling
        convlstm_us_list = self._us_list(skip_stack,
                                         convlstm_us_list,
                                         us_filters,
                                         us_ks,
                                         us_strides,
                                         use_us_drps,
                                         us_drp,
                                         leak_relu_alpha,
                                         us_use_bns,
                                         us_use_maxps)
        # Output
        last = self._output(ipt,
                            convlstm_us_list,
                            ds_strides,
                            last_filters,
                            last_ks,
                            last_seq_filters,
                            last_seq_ks)
        
        return Model(inputs=ipt,
                     outputs=last,
                     name=self.cfg_tr.MODEL_NAME.format(chn, start_date))

    def get_optimizer(self):
        """
        Description:
            모델 학습에 사용할 Optimizer 선택.
            adam, rmsprop, sgd, adagrad 사용가능
        Args:
            None
        Returns:
            optimizer: 옵티마이저
        """
        name = self.cfg_opt.USE
        if name == "adam":
            optimizer = Adam(learning_rate=self.cfg_opt.ADAM.LR,
                            beta_1=self.cfg_opt.ADAM.BETAS[0],
                            beta_2=self.cfg_opt.ADAM.BETAS[1],
                            epsilon=float(self.cfg_opt.ADAM.EPSILON))
        elif name == "rmsprop":
            optimizer = RMSprop(learning_rate=self.cfg_opt.RMSPROP.LR,
                                rho=self.cfg_opt.RMSPROP.RHO,
                                momentum=self.cfg_opt.RMSPROP.MOMENTUM,
                                epsilon=float(self.cfg_opt.RMSPROP.EPSILON))
        elif name == "sgd":
            optimizer = SGD(learning_rate=self.cfg_opt.SGD.LR,
                            momentum=self.cfg_opt.SGD.MOMENTUM)
        elif name == "adagrad":
            optimizer = Adagrad(learning_rate=self.cfg_opt.ADAGRAD.LR,
                                initial_accumulator_value=self.cfg_opt.ADAGRAD.INIT_ACCUM,
                                epsilon=float(self.cfg_opt.ADAGRAD.EPSILON))
        else:
            log_sent = ">>> Able to use : adam, rmsprop, sgd, adagrad / Cannot use : {0}"
            self.log.info(log_sent.format(name))
            self.err_log.error(log_sent.format(name))
        return optimizer
    
    def get_loss_func(self):
        """
        Description:
            모델 학습에 사용할 Loss function 선택.
            mae, mse, logcosh, huber, pixelwise_regular 사용가능
        Args:
            None
        Returns:
            loss_func: 손실 함수
        """
        name = self.cfg_loss.USE
        if name == "mae":
            loss_func = AmpMae(amplification=self.cfg_loss.MAE.AMPLIFICATION)
        elif name == "mse":
            loss_func = AmpMse(amplification=self.cfg_loss.MSE.AMPLIFICATION)
        elif name == "huber":
            loss_func = AmpHuber(delta=self.cfg_loss.HUBER.DELTA,
                                 amplification=self.cfg_loss.HUBER.AMPLIFICATION)
        elif name == "logcosh":
            loss_func = AmpLogCosh(amplification=self.cfg_loss.LOGCOSH.AMPLIFICATION)
        elif name == "pixelwise_regular":
            loss_func = PixelWiseRegularizer(amplification=self.cfg_loss.PIXELWISE_REGULAR.AMPLIFICATION,
                                             magnitude=self.cfg_loss.PIXELWISE_REGULAR.MAGNITUDE,
                                             min_value=self.cfg_loss.PIXELWISE_REGULAR.MIN_VALUE,
                                             max_value=self.cfg_loss.PIXELWISE_REGULAR.MAX_VALUE)
        else:
            log_sent = ">>> Able to use : mae, mse, huber, logcosh, pixelwise_regular / Cannot use : {0}"
            self.log.info(log_sent.format(name))
            self.err_log.error(log_sent.format(name))
        return loss_func

    def save_model_cfg(self, chn, start_date, model):
        """
        Description:
            모델의 설정값 저장
        Args:
            chn, str, 위성 채널
            start_date: str, 모델 생성일자
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        Returns:
            None
        """
        self.log.info(">>> Save Model's Configs")
        model_name = self.cfg_tr.MODEL_NAME.format(chn, start_date)
        hist_file = "{0}.txt".format(model_name)
        hist_dir = return_dir(True, self.cfg_tr.HIST_DIR, chn)
        hist_path = os.path.join(hist_dir, hist_file)
        TRAIN_DATA_LEN = 0
        VALID_DATA_LEN = 0
        try:
            with open(hist_path, "w") as f:
                f.write("Model name: {0}\n".format(model_name))
                f.write("Used Train datas: ({0})\n".format(self.cfg_tr.DATA_TYPE))
                for f_name, f_shape in zip(self.cfg_tr.DATA_FILE, self.cfg_tr.DATA_SHAPE):
                    f.write("    File: {0} / Shape: {1}\n".format(f_name.format(chn), f_shape))
                    TRAIN_DATA_LEN += f_shape[0]
                f.write("Used Validation datas: ({0})\n".format(self.cfg_tr.DATA_TYPE))
                for f_name, f_shape in zip(self.cfg_tr.VALID_DATA_FILE, self.cfg_tr.VALID_DATA_SHAPE):
                    f.write("    File: {0} / Shape: {1}\n".format(f_name.format(chn), f_shape))
                    VALID_DATA_LEN += f_shape[0]
                TRAIN_DATA_SEQLEN = math.ceil((TRAIN_DATA_LEN - self.cfg_d.INPUT_SEQ - self.cfg_d.OUTPUT_SEQ + 1) / self.cfg_tr.BATCH_SIZE)
                VALID_DATA_SEQLEN = math.ceil((VALID_DATA_LEN - self.cfg_d.INPUT_SEQ - self.cfg_d.OUTPUT_SEQ + 1) / self.cfg_tr.BATCH_SIZE)
                f.write("Training / Validation data nums: {0} / {1}\n".format(TRAIN_DATA_LEN, VALID_DATA_LEN))
                f.write("Training / Validation sequence nums: {0} / {1}\n".format(TRAIN_DATA_SEQLEN, VALID_DATA_SEQLEN))
                f.write("Parametes: Data\n")
                self._print_cfg(self.cfg_d, f)
                f.write("Parametes: Model\n")
                self._print_cfg(self.cfg_m, f)
                f.write("Parametes: Optimizer using {0}\n".format(self.cfg_opt.USE.upper()))
                self._print_cfg(getattr(self.cfg_opt, self.cfg_opt.USE.upper()), f)
                f.write("Parametes: Loss function using {0}\n".format(self.cfg_loss.USE.upper()))
                self._print_cfg(getattr(self.cfg_loss, self.cfg_loss.USE.upper()), f)
                f.write("Parametes: Normalizer using {0}\n".format(self.cfg_norm.FUNCTION.upper()))
                self._print_cfg(getattr(self.cfg_norm, chn.upper()), f)
                f.write("Batch size: {0}\n".format(self.cfg_tr.BATCH_SIZE))
                f.write("Steps per 1 Epoch: {0}\n".format(TRAIN_DATA_SEQLEN))
                f.write("Maximum Epoch: {0}\n".format(self.cfg_tr.EPOCH))
                f.write("Early Stopping Patience: {0}\n".format(self.cfg_tr.PATIENCE))
                f.write("Steps per Execution on Model compile: {0}\n".format(self.cfg_tr.STEPS_EXEC))
                f.write("Checkpoint term: {0}\n".format(self.cfg_tr.CKPT_PERIOD))
                if self.ckpt_path is not None:
                    f.write("Import Checkpoint: {0}\n".format(self.ckpt_path.split("/")[-1]))
                model.summary(line_length=120, print_fn=lambda x: f.write(x + "\n"))   # 모델 summary
        except Exception as e:
            self.err_log.error(">>> Error on Saving Model's Parameters : {0}".format(e))
        else:
            self.log.info(">>> Save Model's Configs completely")
    
    def _print_cfg(self, cfg, f, blank=2, cnt=1):
        """
        Description:
            Namedtuple 유형의 설정값 출력
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            f: file object, 작성할 파일 객체
            subject: str, 출력 설정 중 일부만 출력하고자 하는 optimizer, normalizer
            blank: str, 앞의 공백 수
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        Returns:
            None
        """
        cnt += 1
        for field in cfg._fields:
            cfg_f = getattr(cfg, field)
            # Namedtuple 이면 depth를 더 들어감
            if isinstance(cfg_f, tuple) and hasattr(cfg_f, "_asdict") and hasattr(cfg_f, "_fields"):
                f.write("{0}{1}:\n".format(" "*blank*cnt, field))
                self._print_cfg(cfg_f, f, blank, cnt)
            else:
                f.write("{0}{1}: {2}\n".format(" "*blank*cnt, field, cfg_f))
    
    def load_ckpt(self, model):
        """
        Description:
            Checkpoint 경로가 설정되어 있으면 해당 Checkpoint 불러오기.
        Args:
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        Returns:
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델(Checkpoint 반영)
        """
        model.load_weights(self.ckpt_path)
        self.log.info(">>> Load Checkpoint from {0}".format(self.ckpt_path.split("/")[-1]))
    
    def load_model(self, model_path):
        """
        Description:
            Tensorflow 모델 불러오기.
            Custom Class 사용한 걸로 인해 일반 compile=True로 load가 안 됨.
            ==> https://www.tensorflow.org/guide/keras/save_and_serialize#registering_the_custom_object 로 custom 객체 저장방법 수정 필요
        Args:
            model_path: str, 모델 디렉토리 경로
        Returns:
            model: tf.keras.Model, Unet + ConvLSTM 모델
        """
        from tensorflow.keras.models import load_model
        model = load_model(model_path, compile=False)
        self.log.info(">>> Use model: {0}".format(model.name))
        return model


class SatPredModelBasis(object):
    """Satellite Timeseries Prediction Model Basis."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            위성(GK2A) 영상 시계열 예측 모델 기저 클래스 init 설정.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(SatPredModelBasis, self).__init__()
        self.cfg_opt = cfg.PARAMS.OPT
        self.cfg_loss = cfg.PARAMS.LOSS
        self.cfg_reg = cfg.PARAMS.REGULARIZER
        self.cfg_d = cfg.PARAMS.DATA
        self.cfg_m = cfg.PARAMS.MODEL
        self.cfg_norm = cfg.PARAMS.NORMALIZER
        self.cfg_tr = cfg.TRAIN
        self.ckpt_path = cfg.TRAIN.IMPORT_CKPT_PATH
        self.chn = cfg.PARAMS.DATA.CHANNEL

        self.log, self.err_log = log, err_log
    
    def __call__(self, model):
        """
        Description:
            모델 사용 가능하도록 세팅.
        Args:
            model: tf.keras.Model, Keras 모델
        Returns:
            model: tf.keras.Model, 컴파일 완료된 Keras 모델
        """
        model = self.compile_model(model)
        if self.ckpt_path is not None:
            self.load_ckpt(model)
        return model
    
    def make_model_name(self, start_date):
        """
        Description:
            모델명 생성.
        Args:
            start_date: str, 모델 생성일자
        Return:
            model_name: str, 모델명
        """
        return self.cfg_tr.MODEL_NAME.format(self.chn, start_date)

    def compile_model(self, model):
        """
        Description:
            이미지 시계열 예측 모델 컴파일.
        Args:
            model: tf.keras.Model, Keras 모델
        Return:
            model: tf.keras.Model, 컴파일 완료된 Keras 모델
        """
        self.log.info(">>> Make Model on {0} channel".format(self.chn.upper()))
        opt = self.get_optimizer()
        loss = self.get_loss_func()
        reg = self.get_regularizer()
        metrics = [MeanAbsoluteError()]
        if reg is None:
            total_loss = loss
        else:
            total_loss = AdaptRegularizerLoss(main_loss=loss, regularizer=reg)
        model.compile(optimizer=opt,
                      loss=total_loss,
                      metrics=metrics,
                      steps_per_execution=self.cfg_tr.STEPS_EXEC)
        return model

    def get_optimizer(self):
        """
        Description:
            모델 학습에 사용할 Optimizer 선택.
            adam, rmsprop, sgd, adagrad 사용가능
        Args:
            None
        Returns:
            optimizer: 옵티마이저
        """
        name = self.cfg_opt.USE
        if name == "adam":
            optimizer = Adam(learning_rate=self.cfg_opt.ADAM.LR,
                             beta_1=self.cfg_opt.ADAM.BETAS[0],
                             beta_2=self.cfg_opt.ADAM.BETAS[1],
                             epsilon=float(self.cfg_opt.ADAM.EPSILON))
        elif name == "rmsprop":
            optimizer = RMSprop(learning_rate=self.cfg_opt.RMSPROP.LR,
                                rho=self.cfg_opt.RMSPROP.RHO,
                                momentum=self.cfg_opt.RMSPROP.MOMENTUM,
                                epsilon=float(self.cfg_opt.RMSPROP.EPSILON))
        elif name == "sgd":
            optimizer = SGD(learning_rate=self.cfg_opt.SGD.LR,
                            momentum=self.cfg_opt.SGD.MOMENTUM)
        elif name == "adagrad":
            optimizer = Adagrad(learning_rate=self.cfg_opt.ADAGRAD.LR,
                                initial_accumulator_value=self.cfg_opt.ADAGRAD.INIT_ACCUM,
                                epsilon=float(self.cfg_opt.ADAGRAD.EPSILON))
        else:
            log_sent = ">>> Able to use : adam, rmsprop, sgd, adagrad / Cannot use : {0}"
            self.log.info(log_sent.format(name))
            self.err_log.error(log_sent.format(name))
        self.log.info(">>> Use Optimizer: {0}".format(name.upper()))
        return optimizer
    
    def get_loss_func(self):
        """
        Description:
            모델 학습에 사용할 Loss function 선택.
            mae, mse, logcosh, huber 사용가능.
        Args:
            None
        Returns:
            loss_func: 손실 함수
        """
        name = self.cfg_loss.USE
        if name == "mae":
            loss_func = AmpMae(amplification=self.cfg_loss.MAE.AMPLIFICATION)
        elif name == "mse":
            loss_func = AmpMse(amplification=self.cfg_loss.MSE.AMPLIFICATION)
        elif name == "huber":
            loss_func = AmpHuber(delta=self.cfg_loss.HUBER.DELTA,
                                 amplification=self.cfg_loss.HUBER.AMPLIFICATION)
        elif name == "logcosh":
            loss_func = AmpLogCosh(amplification=self.cfg_loss.LOGCOSH.AMPLIFICATION)
        else:
            log_sent = ">>> Able to use : mae, mse, huber, logcosh / Cannot use : {0}"
            self.log.info(log_sent.format(name))
            self.err_log.error(log_sent.format(name))
        self.log.info(">>> Use Loss function: {0}".format(name.upper()))
        return loss_func
    
    def get_regularizer(self):
        """
        Description:
            Loss function 에 추가 반영할 Regularizer 설정.
            pixelwise_regularizer 사용가능.
        Args:
            None
        Returns:
            regularizer: 손실 함수에 반영할 Regularizer
        """
        name = self.cfg_reg.USE
        if name is None:
            regularizer = None
        elif name == "pixelwise_regular":
            regularizer = PixelWiseRegularizer(amplification=self.cfg_reg.PIXELWISE_REGULAR.AMPLIFICATION,
                                               magnitude=self.cfg_reg.PIXELWISE_REGULAR.MAGNITUDE,
                                               min_value=self.cfg_reg.PIXELWISE_REGULAR.MIN_VALUE,
                                               max_value=self.cfg_reg.PIXELWISE_REGULAR.MAX_VALUE,
                                               inverse=self.cfg_reg.PIXELWISE_REGULAR.INVERSE)
        else:
            log_sent = ">>> Able to use : pixelwise_regularizer / Cannot use : {0}"
            self.log.info(log_sent.format(name))
            self.err_log.error(log_sent.format(name))
        
        if isinstance(name, str):
            self.log.info(">>> Use Regularizer: {0}".format(name.upper()))
        return regularizer

    def save_model_cfg(self,
                       chn,
                       start_date,
                       model,
                       train_dataloader,
                       valid_dataloader):
        """
        Description:
            모델의 설정값 저장
        Args:
            chn, str, 위성 채널
            start_date: str, 모델 생성일자
            model: tf.keras.Model, 컴파일 완료된 Keras 모델
            train_dataloader: tf.keras.Sequence 객체, Dataloader 역할(학습)
                - Input Shape: (batch_size, input_seq_len, height, width, 1)
                - Output Shape: (batch_size, output_seq_len, height, width, 1)
            valid_dataloader: tf.keras.Sequence 객체, Dataloader 역할(검증)
                - Input Shape: (batch_size, input_seq_len, height, width, 1)
                - Output Shape: (batch_size, output_seq_len, height, width, 1)
        Returns:
            None
        """
        self.log.info(">>> Save Model's Configs")
        model_name = self.cfg_tr.MODEL_NAME.format(chn, start_date)
        hist_file = "{0}.txt".format(model_name)
        hist_dir = return_dir(True, self.cfg_tr.HIST_DIR, chn)
        hist_path = os.path.join(hist_dir, hist_file)
        TRAIN_DATA_SEQLEN = len(train_dataloader.seq_indices)
        VALID_DATA_SEQLEN = len(valid_dataloader.seq_indices)
        try:
            with open(hist_path, "w") as f:
                f.write("Model name: {0}\n".format(model_name))
                f.write("Used Train datas: ({0})\n".format(self.cfg_tr.DATA_TYPE))
                for f_name, f_shape in zip(self.cfg_tr.DATA_FILE, self.cfg_tr.DATA_SHAPE):
                    f.write("    File: {0} / Shape: {1}\n".format(f_name.format(chn), f_shape))
                f.write("Used Validation datas: ({0})\n".format(self.cfg_tr.DATA_TYPE))
                for f_name, f_shape in zip(self.cfg_tr.VALID_DATA_FILE, self.cfg_tr.VALID_DATA_SHAPE):
                    f.write("    File: {0} / Shape: {1}\n".format(f_name.format(chn), f_shape))
                f.write("Training / Validation sequence nums: {0} / {1}\n".format(TRAIN_DATA_SEQLEN, VALID_DATA_SEQLEN))
                f.write("Parametes: Data\n")
                self._print_cfg(self.cfg_d, f)
                f.write("Parametes: Model\n")
                self._print_cfg(self.cfg_m, f)
                f.write("Parametes: Optimizer using {0}\n".format(self.cfg_opt.USE.upper()))
                self._print_cfg(getattr(self.cfg_opt, self.cfg_opt.USE.upper()), f)
                f.write("Parametes: Loss function using {0}\n".format(self.cfg_loss.USE.upper()))
                self._print_cfg(getattr(self.cfg_loss, self.cfg_loss.USE.upper()), f)
                f.write("Parametes: Normalizer using {0}\n".format(self.cfg_norm.FUNCTION.upper()))
                self._print_cfg(getattr(self.cfg_norm, chn.upper()), f)
                f.write("Parametes: Regularizer using {0}\n".format(self.cfg_reg.USE.upper()))
                self._print_cfg(getattr(self.cfg_reg, self.cfg_reg.USE.upper()), f)
                f.write("Batch size: {0}\n".format(self.cfg_tr.BATCH_SIZE))
                f.write("Steps per 1 Epoch: {0}\n".format(TRAIN_DATA_SEQLEN))
                f.write("Maximum Epoch: {0}\n".format(self.cfg_tr.EPOCH))
                f.write("Early Stopping Patience: {0}\n".format(self.cfg_tr.PATIENCE))
                f.write("Check Best Loss using Loss {0} / Val_loss {1}\n".format(1-self.cfg_tr.VAL_LOSS_RATIO, self.cfg_tr.VAL_LOSS_RATIO))
                f.write("Minimum Training Epoch: {0}\n".format(self.cfg_tr.MIN_TRAIN_EPOCH))
                f.write("Steps per Execution on Model compile: {0}\n".format(self.cfg_tr.STEPS_EXEC))
                f.write("Checkpoint term: {0}\n".format(self.cfg_tr.CKPT_PERIOD))
                if self.ckpt_path is not None:
                    f.write("Import Checkpoint: {0}\n".format(self.ckpt_path.split("/")[-1]))
                model.summary(line_length=120, print_fn=lambda x: f.write(x + "\n"))   # 모델 summary
        except Exception as e:
            self.err_log.error(">>> Error on Saving Model's Parameters : {0}".format(e))
        else:
            self.log.info(">>> Save Model's Configs completely")
    
    def _print_cfg(self, cfg, f, blank=2, cnt=1):
        """
        Description:
            Namedtuple 유형의 설정값 출력
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            f: file object, 작성할 파일 객체
            subject: str, 출력 설정 중 일부만 출력하고자 하는 optimizer, normalizer
            blank: str, 앞의 공백 수
            model: tf.keras.Model, 컴파일 완료된 Unet + TCN 모델
        Returns:
            None
        """
        cnt += 1
        for field in cfg._fields:
            cfg_f = getattr(cfg, field)
            # Namedtuple 이면 depth를 더 들어감
            if isinstance(cfg_f, tuple) and hasattr(cfg_f, "_asdict") and hasattr(cfg_f, "_fields"):
                f.write("{0}{1}:\n".format(" "*blank*cnt, field))
                self._print_cfg(cfg_f, f, blank, cnt)
            else:
                f.write("{0}{1}: {2}\n".format(" "*blank*cnt, field, cfg_f))
    
    def load_ckpt(self, model):
        """
        Description:
            Checkpoint 경로가 설정되어 있으면 해당 Checkpoint 불러오기.
        Args:
            model: tf.keras.Model, 컴파일 완료된 Keras 모델
        Returns:
            model: tf.keras.Model, 컴파일 완료된 Keras 모델(Checkpoint 반영)
        """
        model.load_weights(self.ckpt_path)
        self.log.info(">>> Load Checkpoint from {0}".format(self.ckpt_path.split("/")[-1]))


class SatPredModelV9(SatPredModelBasis):
    """Satellite Timeseries Prediction Model."""
    def __name__(self):
        """
        Description:
            모델 save할 때 오류로 인해 __name__ 지정.
        """
        return self.__class__.__name__
    
    def __init__(self, cfg, log, err_log):
        """
        Description:
            위성(GK2A) 영상 시계열 예측 모델 init 설정.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(SatPredModelV9, self).__init__(cfg, log, err_log)
        cfg_d = cfg.PARAMS.DATA
        cfg_ds = cfg.PARAMS.MODEL.DS
        cfg_clstm = cfg.PARAMS.MODEL.CONVLSTM
        cfg_us = cfg.PARAMS.MODEL.US

        # Hyperparameters
        self.input_seq = cfg_d.INPUT_SEQ
        self.output_seq = cfg_d.OUTPUT_SEQ
        ipt_h, ipt_w, ipt_c = cfg_d.INPUT_SHAPE
        self.ipt_shape = (self.input_seq, ipt_h, ipt_w, ipt_c)
        
        self.ds_filters = cfg_ds.FILTERS
        self.ds_ks = [(1, ks, ks) for ks in cfg_ds.KN_SIZES]
        self.ds_strides = [(1, st, st) for st in cfg_ds.STRIDES]
        self.leak_relu_alpha = cfg_ds.LEAKY_RELU_ALPHA
        self.use_bns = cfg_ds.USE_BNS
        self.use_maxps = cfg_ds.USE_MAXPS
        
        self.cvlstm_filters = [cfg_clstm.FILTERS for _ in range(cfg_d.OUTPUT_SEQ)]
        _ks = cfg_clstm.KN_SIZES
        self.cvlstm_ks = [(_ks, _ks) for _ in range(cfg_d.OUTPUT_SEQ)]
        self.cvlstm_rc_drp = cfg_clstm.RC_DRP

        self.us_filters = list(reversed(self.ds_filters[:-1]))
        self.us_ks = [elm[1:] for elm in reversed(self.ds_ks[:-1])]
        self.us_strides = [elm[1:] for elm in reversed(self.ds_strides[1:])]
        self.us_use_bns = list(reversed(self.use_bns[:-1]))
        self.us_use_maxps = list(reversed(self.use_maxps[:-1]))
        self.use_us_drps = cfg_us.USE_DRPS
        self.us_drp = cfg_us.DRP
        self.last_filters = cfg_us.LAST_SEQ_FILTER
        self.last_ks = (cfg_us.LAST_KN_SIZE, cfg_us.LAST_KN_SIZE)
        self.last_seq_filters = cfg_us.LAST_SEQ_FILTER
        self.last_seq_ks = (1, cfg_us.LAST_SEQ_KN_SIZE, cfg_us.LAST_SEQ_KN_SIZE)
        
        self.log, self.err_log = log, err_log
        
        self.log, self.err_log = log, err_log

    def make_model(self, start_date):
        """
        Description:
            이미지 시계열 예측 모델 생성(U-net + ConvLSTM).
            입력/출력 시계열 길이 다르게 설정 가능.
            출력 시계열 길이만큼 ConvLSTM 구성 뒤 각각 Upsampling 진행 후 결합.
        Args:
            start_date: str, 모델 생성일자
        Return:
            model: tf.keras.Model, 컴파일 완료된 Unet + ConvLSTM 모델
        """
        # Input
        ipt = self._input(self.ipt_shape)
        # DownSampling
        down_stack, skip_stack = self._ds_stack(ipt)
        # ConvLSTM
        convlstm_us_list = self._convlstm_list(down_stack)
        # UpSampling
        us_list = self._us_list(skip_stack, convlstm_us_list)
        # Output
        last = self._output(ipt, us_list)
        
        return Model(inputs=ipt,
                     outputs=last,
                     name=self.make_model_name(start_date))

    def _downsample(self,
                    x,
                    filters,
                    kn_size,
                    name,
                    stride,
                    use_bn=True,
                    leak_relu_alpha=0.3,
                    use_maxp=True):
        """
        Description:
            DownSampling 3-D convolution block.
        Args:
            x: tf.tensor, input tensor.
            filters, int, Conv에 적용할 filters (hidden state)
            kn_size: tuple, Conv에 적용할 kernel size
            name: str, layer 이름에 붙일 것
            stride: tuple, Conv에 적용할 stride
            use_bn: bool, BatchNormalization 사용 여부, default=True
            leak_relu_alpha: float, Leakly ReLU에 적용할 alpha 값, default=0.3
            use_maxp: bool, Maxpooling 사용 여부, default=True
        Return:
            Downsampling block: tf.keras.Model
        """
        init = he_uniform()
        x = Conv3D(filters,
                   kn_size,
                   strides=stride,
                   padding="same",
                   kernel_initializer=init,
                   use_bias=False,
                   name="ds_conv_{0}".format(name))(x)
        if use_bn:
            x = BatchNormalization(name="ds_bn_{0}".format(name))(x)
        x = LeakyReLU(alpha=leak_relu_alpha, name="ds_act_{0}".format(name))(x)
        if use_maxp:
            x = MaxPooling3D(pool_size=kn_size,
                             strides=(1, 1, 1),
                             padding="same",
                             name="ds_maxp_{0}".format(name))(x)    
        return x
    
    def _upsample_2d(self,
                     x,
                     filters,
                     kn_size,
                     name,
                     seq_idx,
                     stride,
                     use_us_drps=True,
                     us_drp=0.5,
                     last=False):
        """
        Description:
            UpSampling 2-D convolution block.
        Args:
            x: tf.tensor, input tensor.
            filters, int, Conv에 적용할 filters (hidden state)
            kn_size: tuple, Conv에 적용할 kernel size
            name: str, layer 이름에 붙일 것
            seq_idx: str, layer 이름에 붙일 sequence index
            stride: tuple, Conv에 적용할 stride
            use_us_drps: bool, Dropout 사용 여부, default=True
            us_drp: float Dropout 사용할 때 적용할 비율, default=0.5
            last: bool, 마지막 layer 확인 여부, default=False
        Return:
            Upsampling block: tf.keras.Model
        """
        init = he_uniform()
        if not last:
            x = Conv2DTranspose(filters,
                                kn_size,
                                strides=stride,
                                padding="same",
                                kernel_initializer=init,
                                use_bias=False,
                                name="us_conv_{0}_{1}".format(name, seq_idx))(x)
            x = BatchNormalization(name="us_bn_{0}_{1}".format(name, seq_idx))(x)
            if use_us_drps:
                x = Dropout(us_drp, name="us_drp_{0}_{1}".format(name, seq_idx))(x)
            x = ReLU(name="us_act_{0}_{1}".format(name, seq_idx))(x)
        else:
            x = Conv2DTranspose(filters,
                                kn_size,
                                strides=stride,
                                padding="same",
                                kernel_initializer=init,
                                activation="linear",
                                name="{0}_{1}".format(name, seq_idx))(x)
        return x
    
    def _convlstm(self,
                  x,
                  filters,
                  kn_size,
                  name,
                  cv_lstm_rc_drp=0.5):
        """
        Description:
            2-D Convolutional LSTM block.
        Args:
            x: tf.tensor, input tensor.
            filters, int, ConvLSTM에 적용할 filters (hidden state)
            kn_size: tuple, ConvLSTM에 적용할 kernel size
            name: str, layer 이름에 붙일 것
            cv_lstm_rc_drp: tuple, ConvLSTM에 적용할 recurrent_dropout 비율, default=0.5
        Return:
            ConvLSTM2D block: tf.keras.Model
        """
        init = he_uniform()
        x = ConvLSTM2D(filters,
                       kn_size,
                       padding="same",
                       kernel_initializer=init,
                       use_bias=True,
                       recurrent_dropout=cv_lstm_rc_drp,
                       return_sequences=True,
                       name="conv_lstm_{0}".format(name))(x)
        x = BatchNormalization(name="conv_lstm_bn_{0}".format(name))(x)        
        return x

    def _input(self, ipt_shape):
        """
        Description:
            Input layer.
        Args:
            ipt_shape: tuple, Input shape
        Return:
            ipt: KerasTensor, 입력 layer
        """
        ipt = Input(shape=ipt_shape, name="input")
        self.log.info("      input----------- {0}".format(ipt.shape))
        return ipt

    def _ds_stack(self, x):
        """
        Description:
            DownSampling 3-D convolution stacks.
            (seq_len, h, w, filters) 를 (h, w, filters * seq_len)로 변환.
        Args:
            x: KerasTensor, 입력 layer
        Return:
            down_stack: list, Downsampling을 적용한 부분
            skip_stack: list, Skip connection으로 사용할 부분
        """
        down_stack = []
        skip_stack = []
        for i, (f, ks, st, bn, mxp) in enumerate(zip(self.ds_filters,
                                                     self.ds_ks,
                                                     self.ds_strides,
                                                     self.use_bns,
                                                     self.use_maxps)):
            x = self._downsample(x,
                                 filters=f,
                                 kn_size=ks,
                                 name="{0:02d}".format(i+1),
                                 stride=st,
                                 use_bn=bn,
                                 leak_relu_alpha=self.leak_relu_alpha,
                                 use_maxp=mxp)
            down_stack.append(x)
            self.log.info("      down----------- {0}".format(x.shape))
            # (batch, seq, h, w, c) -> (batch, h, w, seq, c) -> (batch, h, w, seq*c)
            x_concat = Permute((2, 3, 1, 4), name="ds_cvt_{0:02d}".format(i+1))(x)
            _, h, w, _, _ = x_concat.shape
            x_concat = Reshape((h, w, -1), name="skip_{0:02d}".format(i+1))(x_concat)
            if i < len(self.ds_filters) - 1:
                skip_stack.append(x_concat)
                self.log.info("      down_cvt----------- {0}".format(x_concat.shape))
        skip_stack = list(reversed(skip_stack))
        return down_stack, skip_stack

    def _convlstm_list(self, down_stack):
        """
        Description:
            2-D Convolutional LSTM list.
            ConvLSTM -> ConvLSTM 은 seq 상태로 데이터 전달.
            ConvLSTM에서 Upsampling할 부분은 seq 마지막 부분만 전달.
        Args:
            down_stack: list, Downsampling을 적용한 부분
        Return:
            convlstm_us_list: list, 각각 seq마다 Upsampling에 사용할 부분
        """
        x = down_stack[-1]
        convlstm_us_list = []
        for i, (f, ks) in enumerate(zip(self.cvlstm_filters, self.cvlstm_ks)):
            x = self._convlstm(x,
                               filters=f,
                               kn_size=ks,
                               name="{0:02d}".format(i+1),
                               cv_lstm_rc_drp=self.cvlstm_rc_drp)
            bs, seq, h, w, c = x.shape
            x_us = Cropping3D(cropping=((seq-1, 0), (0, 0), (0, 0)),
                              name="conv_lstm_croptail_{0:02d}".format(i+1))(x)
            x_us = Reshape((h, w, -1), name="conv_lstm_tail_{0:02d}".format(i+1))(x_us)
            convlstm_us_list.append(x_us)
        self.log.info("      convlstm----------- {0} * {1}".format(x.shape, self.output_seq))
        self.log.info("      convlstm_us----------- {0} * {1}".format(x_us.shape, self.output_seq))
        return convlstm_us_list
    
    def _next_us_convlstm(self,
                          us,
                          skip,
                          us_filter,
                          us_ks,
                          leak_relu_alpha,
                          use_bn,
                          use_maxp,
                          i,
                          j):
        """
        Description:
            Next ConvLSTM result upsampling.
            이전 skip connection 반영하여 개별 seq 결과마다 Upsampling 결과 산출.
            Downsampling 쪽에서의 skip connection을 1 times 밀어가며 다음 seq에 적용.
        Args:
            us: KerasTensor, Upsampling 결과
            skip: KerasTensor, Downsampling에서 전달된 skip connection
            us_filter: int, Upsampling layer의 filter
            us_ks: int, Upsampling layer의 kernel_size
            leak_relu_alpha: float, Downsampling layer의 Leaky ReLU의 alpha 값 동일하게 사용
            use_bns: bool, Upsampling의 BatchNormalization 사용여부
            use_maxps: bool, Upsampling의 Maxpooling 사용여부
            i: int, Upsampling level index
            j: int, ConvLSTM index
        Return:
            next_us: KerasTensor, Skip Connection 반영된 Upsampling 결과 (다음 Upsampling에 전달)
            next_skip: KerasTensor, 1 times 씩 밀린 skip connection (다음 ConvLSTM에 전달)
        """
        init = he_uniform()
        # 다음 convlstm으로 보낼 t ~ t+5 (skip) 와 upsampling 결과 결합
        concat = Concatenate(name="us_concat_{0:02d}_{1:02d}".format(i+1, j+1))([skip, us])
        next_us = Conv2D(us_filter,
                         us_ks,
                         padding="same",
                         kernel_initializer=init,
                         use_bias=False,
                         name="next_us_{0:02d}_{1:02d}".format(i+1, j+1))(concat)
        if use_bn:
            next_us = BatchNormalization(name="next_us_bn_{0:02d}_{1:02d}".format(i+1, j+1))(next_us)
        next_us = LeakyReLU(alpha=leak_relu_alpha,
                            name="next_us_act_{0:02d}_{1:02d}".format(i+1, j+1))(next_us)
        if use_maxp:
            next_us = MaxPooling2D(pool_size=us_ks,
                                   strides=(1, 1),
                                   padding="same",
                                   name="next_us_maxp_{0:02d}_{1:02d}".format(i+1, j+1))(next_us)
        # concat결과에서 t를 제외하고 t+1 ~ t+6만 새로운 skip으로 보냄
        next_skip = Cropping2D(cropping=((0, 0), (us_filter, 0)),
                               data_format="channels_first",
                               name="next_skip_{0:02d}_{1:02d}".format(i+1, j+1))(concat)
        return next_us, next_skip
    
    def _us_list(self, skip_stack, convlstm_us_list):
        """
        Description:
            UpSampling 2-D convolution list (Sequence).
            개별 seq 결과마다 Upsampling 결과 산출.
            Downsampling 쪽에서의 skip connection을 1 times 밀어가며 다음 seq에 적용.
        Args:
            skip_stack: list, Skip connection으로 사용할 부분
            convlstm_us_list: list, 각각 seq마다 Upsampling에 사용할 부분(convlstm 바로 다음)
        Return:
            convlstm_us_list: list, 각각 seq마다 Upsampling에 사용할 부분(마지막 것)
        """
        for i, (f, ks, st, d, skip, bn, maxp) in enumerate(zip(self.us_filters,
                                                               self.us_ks,
                                                               self.us_strides,
                                                               self.use_us_drps,
                                                               skip_stack,
                                                               self.us_use_bns,
                                                               self.us_use_maxps)):
            us_list = []
            for j, c_us in enumerate(convlstm_us_list):
                us = self._upsample_2d(c_us,
                                       filters=f,
                                       kn_size=ks,
                                       name="{0:02d}".format(i+1),
                                       seq_idx="{0:02d}".format(j+1),
                                       stride=st,
                                       use_us_drps=d,
                                       us_drp=self.us_drp)
                # 다음 convlstm 및 us 로 보낼 것 계산
                us, skip = self._next_us_convlstm(us,
                                                  skip,
                                                  f,
                                                  ks,
                                                  self.leak_relu_alpha,
                                                  bn,
                                                  maxp,
                                                  i,
                                                  j)
                us_list.append(us)
                self.log.debug("      next_skip----------- {0} : {1}".format(j, skip.shape))
            self.log.info("      up_concat----------- {0} * {1}".format(us.shape, self.output_seq))
            convlstm_us_list = us_list
        return convlstm_us_list
    
    def _output(self, ipt, us_list):
        """
        Description:
            Last output.
            입력도 skip connection 사용하여 개별 seq 결과마다 Upsampling 결과 산출.
            Downsampling 쪽에서의 skip connection을 1 times 밀어가며 다음 seq에 적용.
            마지막에 Convolution 이용하여 Output 형상에 맞춤.
        Args:
            ipt: KerasTensor, 입력 layer
            us_list: list, 각각 seq마다 Upsampling에 사용할 부분(마지막 것)
            ds_strides: list, Downsampling layer의 strides
            last_filters: int, 최종 Upsampling 결과 features
            last_ks: tuple, 최종 Upsampling의 kernel_size
            last_seq_filters: int, 최종 layer 결과 features
            last_seq_ks: 최종 layer의 kernel_size
        Return:
            last: KerasTensor, 최종 layer
        """
        # Output (one scene result)
        last_skip_seq_list = []
        # (batch, seq, h, w, c) -> (batch, h, w, seq, c) -> (batch, h, w, seq*c)
        ipt_skip = Permute((2, 3, 1, 4), name="ipt_cvt")(ipt)
        _, h, w, _, _ = ipt_skip.shape
        ipt_skip = Reshape((h, w, -1), name="ipt_skip")(ipt_skip)
        self.log.info("      ipt_skip----------- {0}".format(ipt_skip.shape))
        for j, us in enumerate(us_list):
            last_us = self._upsample_2d(us,
                                        filters=self.last_filters,
                                        kn_size=self.last_ks,
                                        name="last",
                                        seq_idx="{0:02d}".format(j+1),
                                        stride=self.ds_strides[0][1:],   # 맨처음 DS
                                        last=True)
            # 다음 convlstm으로 보낼 t ~ t+5 와 upsampling 결과 결합
            last_skip = Concatenate(name="us_concat_last_{0:02d}".format(j+1))([ipt_skip, last_us])
            last_skip_shape = [1] + list(last_skip.shape)[1:]
            last_skip_seq = Reshape(last_skip_shape,
                                    name="us_concat_last_seq_{0:02d}".format(j+1))(last_skip)
            last_skip_seq_list.append(last_skip_seq)
            self.log.debug("      last_skip----------- {0} {1}".format(j, last_skip.shape))
            # concat결과에서 t를 제외하고 t+1 ~ t+6만 skip connection 보냄
            ipt_skip = Cropping2D(cropping=((0, 0), (self.last_filters, 0)),
                                  data_format="channels_first",
                                  name="ipt_seq_skip_{0:02d}".format(j+1))(last_skip)
            self.log.debug("      next_ipt_skip----------- {0} {1}".format(j, ipt_skip.shape))
        self.log.info("      last_skip----------- {0} * {1}".format(last_skip.shape, self.output_seq))

        # Output (make sequence)
        init = he_uniform()
        last = Concatenate(axis=1, name="concat_last")(last_skip_seq_list)
        last = Conv3D(self.last_seq_filters,
                      self.last_seq_ks,
                      padding="same",
                      kernel_initializer=init,
                      use_bias=False,
                      name="last")(last)
        self.log.info("      output----------- {0}".format(last.shape))
        return last
    
    def load_model(self, model_path):
        """
        Description:
            Tensorflow 모델 불러오기.
            Custom Class 사용한 걸로 인해 일반 compile=True로 load가 안 됨.
            ==> https://www.tensorflow.org/guide/keras/save_and_serialize#registering_the_custom_object 로 custom 객체 저장방법 수정 필요
        Args:
            model_path: str, 모델 디렉토리 경로
        Returns:
            model: tf.keras.Model, Unet + ConvLSTM 모델
        """
        from tensorflow.keras.models import load_model
        model = load_model(model_path, compile=False)
        self.log.info(">>> Use model: {0}".format(model.name))
        return model