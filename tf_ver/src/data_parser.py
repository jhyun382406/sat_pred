import numpy as np


def min_max_normalizer(
    array: np.memmap=None,
    min_value: float=None,
    max_value: float=None
    ):
    array = np.clip(array, min_value, max_value)
    return (array - min_value) / (max_value - min_value)


def inv_min_max_normalizer(
    array: np.memmap=None,
    min_value: float=None,
    max_value: float=None
    ):
    return min_value + (max_value - min_value) * array