import math
import numpy as np
import tensorflow as tf
import dask.array as da
from tensorflow.keras.utils import Sequence

from utils.code_utils import combined_indices_of_cropped


class GK2ADataset(Sequence):
    """
    Description:
        GK2A 위성 np.memmap에서 시계열 데이터 생성하는 제너레이터.
        normalizer를 통해 MinMaxScaling까지 처리.
    Args:
        memory_map: np.memmap, source of this dataset, expected shape = (T, H, W).
        in_seq: int, the number of input sequences.
        out_seq: int, the number of output sequences.
        normalizer: function
        transform: transforms.Compose, the function of data augmentation
        pred_mode: bool, prediction doesn't need to make y.
    Returns:
        1. 학습
            x: tf.tensor, float32, (batch_size, input_seq_length, height, width, channel)
            y: tf.tensor, float32, (batch_size, output_seq_length, height, width, channel)
        2. 추론
            x: tf.tensor, float32, (batch_size, input_seq_length, height, width, channel)
    """
    def __init__(
        self,
        memory_map: np.memmap=None,
        batch_size: int=1,
        in_seq: int=None,
        out_seq: int=None,
        normalizer=None,
        pred_mode: bool=False
        ):
        assert memory_map is not None, "memory_map should not be None"

        self.memory_map = memory_map
        self.in_seq = in_seq
        self.out_seq = out_seq
        self.batch_size = batch_size
        self.data_len = memory_map.shape[0]
        self.normalizer = self.get_normalizer(normalizer)
        # self.transform = self.get_transform(transform)

        if pred_mode:
            # self.target_indices = np.arange(1)   # 1번만 추론
            self.target_indices = np.arange(math.ceil((self.data_len - in_seq + 1) / batch_size)) * batch_size
            self.shuffle = False
        else:
            self.target_indices = np.arange(math.ceil((self.data_len - in_seq - out_seq + 1) / batch_size)) * batch_size
            np.random.shuffle(self.target_indices)
            self.shuffle = True
        
        self.pred_mode = pred_mode

    def get_normalizer(self, normalizer):
        if normalizer is not None:
            return normalizer
        else:
            return False

    def __len__(self):
        return len(self.target_indices)
    
    def __getitem__(self, index):
        # pred_mode == True이면 out_seq 제외
        map_index = self.target_indices[index]
        if not self.pred_mode:
            batch_total_inout_seq = self.in_seq + self.out_seq + self.batch_size - 1
        else:
            batch_total_inout_seq = self.in_seq + self.batch_size - 1
        # batch_size를 고려
        if map_index + batch_total_inout_seq >= self.data_len:
            img = self.memory_map[-batch_total_inout_seq:]
        else:
            img = self.memory_map[map_index: map_index+batch_total_inout_seq]

        if not isinstance(img, np.ndarray):
            img = np.array(img)
        # img = img.transpose(1, 2, 0)
        # img = self.transform(img)   # transform 전: (H, W, N) / 후: (N, H, W)
        img = np.nan_to_num(img, nan=-100)   # 결측처리
        img = self.normalizer(img) if self.normalizer else img
        
        # pred_mode == True이면 input 만 필요
        if not self.pred_mode:
            x = np.empty((self.batch_size, self.in_seq, img.shape[1], img.shape[2], 1), dtype=np.float32)
            y = np.empty((self.batch_size, self.out_seq, img.shape[1], img.shape[2], 1), dtype=np.float32)
            for batch_idx in range(self.batch_size):
                x[batch_idx] = img[batch_idx:batch_idx+self.in_seq][..., None]
                y[batch_idx] = img[batch_idx+self.in_seq:batch_idx+self.in_seq+self.out_seq][..., None]
            x = tf.convert_to_tensor(x, dtype=np.float32)
            y = tf.convert_to_tensor(y, dtype=np.float32)
            if index == len(self.target_indices) - 1:
                np.random.shuffle(self.target_indices)
            del img
            return (x, y)
        else:
            x = np.empty((self.batch_size, self.in_seq, img.shape[1], img.shape[2], 1), dtype=np.float32)
            for batch_idx in range(self.batch_size):
                x[batch_idx] = img[batch_idx:batch_idx+self.in_seq][..., None]
            x = tf.convert_to_tensor(x, dtype=np.float32)
            del img
            return x


class GK2ADatasetV2(Sequence):
    """
    Description:
        GK2A 위성 np.memmap에서 시계열 데이터 생성하는 제너레이터.
        Calibration 및 태양천정각(SZA) 적용 후 normalizer를 통해 MinMaxScaling까지 처리.
        전체 사용 index는 외부에서 계산 후, 인자로 받아옴.
        Random crop도 진행.
        추론 시에 crop한 영역 별도 추론할 수 있도록 분리.
    Args:
        memory_map: dask array, source of this dataset, expected shape = (T, H, W).
        in_seq: int, the number of input sequences.
        out_seq: int, the number of output sequences.
        use_random_crop: bool, flag of random cropping
        input_shape: tuple, shape of model input
        normalizer: function
        calibration_table: numpy array, Calibration table (VI006)
        seq_indices: list, initial indices of available sequences (Day, Night, Dawn/Twilight)
        transform: transforms.Compose, the function of data augmentation
        pred_mode: bool, prediction doesn't need to make y.
    Returns:
        1. 학습
            x: tf.tensor, float32, (batch_size, input_seq_length, height, width, channel)
            y: tf.tensor, float32, (batch_size, output_seq_length, height, width, channel)
        2. 추론
            x: tf.tensor, float32, (batch_size, input_seq_length, height, width, channel)
    """
    def __init__(
        self,
        memory_map: np.memmap=None,
        batch_size: int=1,
        in_seq: int=None,
        out_seq: int=None,
        use_random_crop: bool=True,
        input_shape: tuple=None,
        normalizer=None,
        calibration_table=None,
        seq_indices=None,
        pred_mode: bool=False
        ):
        assert memory_map is not None, "memory_map should not be None"
        self.in_seq = in_seq
        self.out_seq = out_seq
        self.use_random_crop = use_random_crop
        self.input_shape = input_shape
        self.total_seq = in_seq + out_seq if not out_seq is None else in_seq
        self.batch_size = batch_size
        self.data_len = memory_map.shape[0]
        self.normalizer = self.get_normalizer(normalizer)
        self.calibration_table = self.get_calibration_table(calibration_table)
        # self.transform = self.get_transform(transform)

        if pred_mode:
            # Index of Sequence indices
            h_start_indices, w_start_indices = combined_indices_of_cropped(memory_map.shape,
                                                                           input_shape)
            self.split_nums = len(h_start_indices) * len(w_start_indices)
            self.batch_seq_nums = self.data_len - self.in_seq + 1
            self.memory_map = self.get_cropped(memory_map, h_start_indices, w_start_indices)
            seq_slice_total = self.batch_seq_nums * self.split_nums
            self.target_indices = np.arange(math.ceil(seq_slice_total / batch_size)) * batch_size
            self.shuffle = False
        else:
            # Index of Sequence indices
            self.memory_map = memory_map
            self.seq_indices = np.array(seq_indices)
            self.target_indices = np.arange(math.ceil(len(seq_indices) / batch_size)) * batch_size
            np.random.shuffle(self.seq_indices)
            np.random.shuffle(self.target_indices)
            self.shuffle = True
        
        self.pred_mode = pred_mode

    def get_normalizer(self, normalizer):
        """
        Description:
            채널 data에 적용할 Normalizer.
            없으면 False 반환.
        Args:
            normalizer: function, lambda 로 만들어진 normalizer
        Returns:
            normalizer: function, lambda 로 만들어진 normalizer
        """
        if normalizer is not None:
            return normalizer
        else:
            return False
    
    def get_calibration_table(self, calibration_table):
        """
        Description:
            채널 raw data에 적용할 Calibration_table.
            없으면 False 반환.
        Args:
            calibration_table: numpy array, 채널의 Calibration table
        Returns:
            calibration_table: numpy array, 채널의 Calibration table
        """
        if calibration_table is not None:
            return calibration_table
        else:
            return False
    
    def get_cropped(self, memory_map, h_start_indices, w_start_indices):
        """
        Description:
            추론할 전체 영역에 대해 잘라내어 별도 추론해야하므로
            1개 영역을 h_num*w_num 개로 만듦.
        Args:
            memory_map: dask array, source of this dataset, expected shape = (T, H, W)
            h_start_indices: list, 병합 이미지의 Height 쪽 시작 index
            w_start_indices: list, 병합 이미지의 Width 쪽 시작 index
        Returns:
            new_memory_map: dask array, source of this dataset, expected shape = (data_len, crop_nums, T, H, W)
        """
        input_h, input_w, _ = self.input_shape
        h_num, w_num = len(h_start_indices), len(w_start_indices)

        new_memory_map = da.empty((self.data_len, h_num*w_num, input_h, input_w), dtype=np.float32)
        for i, h_i in enumerate(h_start_indices):
            for j, w_i in enumerate(w_start_indices):
                new_memory_map[:, i*w_num+j, :, :] = memory_map[:, h_i:h_i+input_h, w_i:w_i+input_w]
        
        return new_memory_map

    def __len__(self):
        """
        Description:
            Dataloader의 길이.
        Args:
            None
        Returns:
            dataloader_length: int, Dataloader의 길이
        """
        return len(self.target_indices)
    
    def __getitem__(self, index):
        """
        Description:
            학습/검증과 추론은 최종 다른 data를 만들게 함.
            추론 때는 정답(y)가 필요없기 때문에 만들지 않음.
        Args:
            index: int, Dataloader의 인덱스
        Returns:
            1. 학습
                x: tf.tensor, float32, (batch_size, input_seq_length, height, width, channel)
                y: tf.tensor, float32, (batch_size, output_seq_length, height, width, channel)
            2. 추론
                x: tf.tensor, float32, (batch_size, input_seq_length, height, width, channel)
        """
        # pred_mode == True이면 input 만 필요
        if not self.pred_mode:
            img = self._make_batch_total(index)
            img = self._additional_convert(img)
            x = self._make_batch_train_seq(img, self.in_seq, 0, self.in_seq)
            y = self._make_batch_train_seq(img, self.out_seq, self.in_seq, self.total_seq)
            x = self._cvt_tensor(x)
            y = self._cvt_tensor(y)
            if index == len(self.target_indices) - 1:
                np.random.shuffle(self.target_indices)
            del img
            return (x, y)
        else:
            img = self._make_batch_pred_seq(index)
            img = self._additional_convert(img)
            x = self._cvt_tensor(img)
            del img
            return x
    
    def _make_batch_total(self, index):
        """
        Description:
            Target indices로부터 실제 Sequence 시작 index 가져오고 해당 Sequence만 추출.
                - batch_size를 고려하여 batch의 total_seq를 계산
                - random crop 적용할 수 있음
                - numpy array로 변환
        Args:
            index: int, Dataloader의 인덱스
        Returns:
            img: numpy array, (batch_total_seq_length, height, width)
        """
        # batch의 total_seq 길이
        map_index = self.target_indices[index]
        real_seq_index = self.seq_indices[map_index]
        batch_total_seq = self.total_seq + self.batch_size - 1

        # batch_size를 고려하여 total_seq 슬라이싱
        if real_seq_index + batch_total_seq >= self.data_len:
            img = self.memory_map[-batch_total_seq:]
        else:
            img = self.memory_map[real_seq_index: real_seq_index+batch_total_seq]
        
        # random crop (just training)
        if self.use_random_crop:
            img = self._crop_randomly(img)
        if not isinstance(img, np.ndarray):
            img = np.array(img)
        
        return img
    
    def _make_batch_pred_seq(self, index):
        """
        Description:
            Target indices로부터 실제 Sequence 시작 index 가져오고 해당 Sequence만 추출.
                - pred_mode == True이면 out_seq는 제외
                - 원본 영역을 모델 입력 형상에 맞게 slicing하여 crop 시킴
                - batch_size를 고려하여 real_seq_index 계산
                - batch의 data_indices, patch_indices를 계산 후 해당 데이터 추출
                - numpy array로 변환
        Args:
            index: int, Dataloader의 인덱스
        Returns:
            img: numpy array, (batch_size, input_seq_length, height, width, channel)
        """
        # batch의 total_seq 길이
        map_index = self.target_indices[index]
        max_target_index = self.batch_seq_nums * self.split_nums
        if map_index + self.batch_size > max_target_index:
            real_seq_index = np.arange(map_index, max_target_index)
        else:
            real_seq_index = np.arange(map_index, map_index + self.batch_size)
        
        # batch_size를 고려하여 total_seq 슬라이싱
        batch_start_indices = np.array([i for i in range(self.batch_seq_nums * self.split_nums)])
        batch_start_indices = batch_start_indices.reshape((self.batch_seq_nums, self.split_nums))
        data_indices, patch_indices = np.where(np.isin(batch_start_indices, real_seq_index) == True)
        
        input_h, input_w, _ = self.input_shape
        img = da.empty((len(real_seq_index), self.in_seq, input_h, input_w, 1), dtype=np.uint16)
        for i, (data_idx, patch_idx) in enumerate(zip(data_indices, patch_indices)):
            img[i] = self.memory_map[data_idx: data_idx + self.in_seq, patch_idx][..., None]

        if not isinstance(img, np.ndarray):
            img = np.array(img)
        
        return img
    
    def _crop_randomly(self, img):
        """
        Description:
            모델 input shape에 맞도록 random crop.
        Args:
            img: numpy array, (batch_total_seq_length, height, width)
        Returns:
            img: numpy array, (batch_total_seq_length, height, width)
        """
        img_h, img_w = img.shape[1:]
        ipt_h, ipt_w = self.input_shape[:2]
        h_start_idx = np.random.randint(img_h - ipt_h)
        w_start_idx = np.random.randint(img_w - ipt_w)
        return img[:, h_start_idx:h_start_idx+ipt_h, w_start_idx:w_start_idx+ipt_w]
    
    def _additional_convert(self, img):
        """
        Description:
            추가적으로 input data에 취하는 값 변환.
                - 만약이 결측이 존재한다면 -100으로 일괄 처리
                - calibration table로 값 변환
                - normalizer 적용
        Args:
            img: numpy array, (batch_total_seq_length, height, width)
        Returns:
            img: numpy array, (batch_total_seq_length, height, width)
        """
        # img = img.transpose(1, 2, 0)
        # img = self.transform(img)   # transform 전: (H, W, N) / 후: (N, H, W)
        img = np.nan_to_num(img, nan=-100)   # 결측처리
        img = self.calibration_table[img] if isinstance(self.calibration_table, np.ndarray) else img
        img = self.normalizer(img) if self.normalizer else img
        return img
    
    def _make_batch_train_seq(self,
                              img,
                              seq_len,
                              add_batch_start_idx,
                              add_batch_end_idx):
        """
        Description:
            1개 Batch에 해당되는 tensor 생성.
            마지막 Batch에 slicing 오류 생기지 않도록 adapted_batch_size 계산.
        Args:
            img: numpy array, (batch_total_seq_length, height, width)
            seq_len: int, 생성할 batch tensor의 sequence 길이
            add_batch_start_idx: int, img를 슬라이싱할 때 시작위치에서 batch_idx에 추가로 더할 값
            add_batch_end_idx: int, img를 슬라이싱할 때 종료위치에서 batch_idx에 추가로 더할 값
        Returns:
            x: tf.tensor, (batch_size, seq_length, height, width, channel)
        """
        full_batch_size = self.batch_size + self.in_seq - 1
        patch_len = img.shape[0]
        if full_batch_size > patch_len:
            adapted_batch_size = patch_len - self.in_seq + 1
        else:
            adapted_batch_size = self.batch_size
        
        x = np.empty(
            (adapted_batch_size, seq_len, img.shape[1], img.shape[2], 1),
            dtype=np.float32
        )
        for batch_idx in range(adapted_batch_size):
            batch_start_idx = batch_idx + add_batch_start_idx
            batch_end_idx = batch_idx + add_batch_end_idx
            x[batch_idx] = img[batch_start_idx:batch_end_idx][..., None]

        return x

    def _cvt_tensor(self, img):
        """
        Description:
            Array를 tensor로 변환.
        Args:
            img: numpy array, (batch_size, seq_length, height, width, channel)
        Returns:
            x: tf.tensor, (batch_size, seq_length, height, width, channel)
        """
        return tf.convert_to_tensor(img, dtype=np.float32)
