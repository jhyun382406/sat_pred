import datetime
import os
import gzip
import numpy as np
import netCDF4 as nc

import warnings
warnings.filterwarnings(action="ignore")


class Data(object):
    """공통 부분."""
    def __init__(self):
        """초기값."""
        super(Data, self).__init__()
        self.CVT_TIME_FORMAT = "%Y%m%d%H%M"
        self.UTC2KST = 9
        self.KST2UTC = self.UTC2KST * (-1)
    
    def _kst_to_utc(self, date):
        """
        Description:
            KST 기준 시간을 UTC로 변경.
        Args:
            date: str, KST 연월일시분(12자리)
        Returns:
            date: str, UTC 연월일시분(12자리)
        """
        date = datetime.datetime.strptime(str(date), self.CVT_TIME_FORMAT)
        date += datetime.timedelta(hours=self.KST2UTC)
        date = datetime.datetime.strftime(date, self.CVT_TIME_FORMAT)
        return date

    def _utc_to_kst(self, date):
        """
        Description:
            UTC 기준 시간을 KST로 변경.
        Args:
            date: str, UTC 연월일시분(12자리)
        Returns:
            date: str, KST 연월일시분(12자리)
        """
        date = datetime.datetime.strptime(str(date), self.CVT_TIME_FORMAT)
        date += datetime.timedelta(hours=self.UTC2KST)
        date = datetime.datetime.strftime(date, self.CVT_TIME_FORMAT)
        return date

    def _compute_date(self, date, standard, time_step):
        """
        Description:
            분단위 기준으로 이후 시간 계산.
        Args:
            date: str, 연월일시분(12자리)
            standard: str, day, hour, min, sec 만 사용 가능
            time_step: int, standard에서 증감할 시간 간격(양수면 이후, 음수면 이전)
        Returns:
            date: str, 연월일시분(12자리)
        """
        date = datetime.datetime.strptime(str(date), self.CVT_TIME_FORMAT)
        if standard == "day":
            date += datetime.timedelta(days=time_step)
        elif standard == "hour":
            date += datetime.timedelta(hours=time_step)
        elif standard == "min":
            date += datetime.timedelta(minutes=time_step)
        elif standard == "sec":
            date += datetime.timedelta(seconds=time_step)
        else:
            print("Wrong time step standard: {0}".format(standard))
            print("Just using: day, hour, min, sec")
        date = datetime.datetime.strftime(date, self.CVT_TIME_FORMAT)
        return date

    def _make_time_list(self, start_time, end_time, standard, time_step):
        """
        Description:
            시간대 리스트 생성.
        Args:
            start_time: str, 연월일시분(12자리)
            end_time: str, 연월일시분(12자리)
            standard: str, day, hour, min, sec 만 사용 가능
            time_step: int, standard에서 증감할 시간 간격
        Returns:
            time_list: list, 문자열 시간대 리스트
        """
        time_list = []
        while start_time <= end_time:
            time_list.append(start_time)
            start_time = self._compute_date(start_time, standard, time_step)
        return time_list


class GK2ADataExtractor(Data):
    def __init__(self, log, err_log):
        """위성(GK2A) 설정."""
        super(GK2ADataExtractor, self).__init__()
        self.nx = 900
        self.ny = 900
        self.sat_dir = "/fogdata/datas/gk2a/{0}"
        self.sat_file = "gk2a_ami_le1b_{0}_ko020lc_{1}.nc"
        # sat_time = "202203051800"
        self.MAX_LOOP = 3
        self.ARRAY_TYPE = np.uint16
        self.saved_dir = "/fogdata/sat_pred/sat_arys"
        self.saved_file = "gk2a_{0}_{1}_{2}.dat"
        
        self.log, self.err_log = log, err_log

    def main(self, chn, kst_start_time, kst_end_time, time_step):
        """
        Description:
            레이더 데이터를 numpy 배열로 불러오기.
        Args:
            chn, str, 위성 채널
            kst_start_time: str, KST 연월일시분(12자리)
            kst_end_time: str, KST 연월일시분(12자리)
            time_step: int, standard에서 증감할 시간 간격(분)
        Returns:
            rdr_array_crop: numpy array, Crop한 전체 레이더 배열
        """
        kst_time_list = self._make_time_list(kst_start_time,
                                             kst_end_time,
                                             "min",
                                             time_step)
        self.log.info("Using Channel / Init date(KST) / End date(KST): {0} / {1} / {2}".format(chn, kst_time_list[0], kst_time_list[-1]))
        self.log.info("Time counts: {0}".format(len(kst_time_list)))
        sat_array = self._make_sat_ary(chn, kst_time_list)
        return sat_array, kst_time_list[-1]

    def _make_sat_ary(self, chn, kst_time_list):
        """
        Description:
            위성 데이터로 numpy 배열 생성.
        Args:
            kst_time_list: list, KST 문자열 시간대 리스트
        Returns:
            sat_array: numpy array, 전체 레이더 배열
        """
        sat_array = np.zeros((len(kst_time_list), self.ny, self.nx),
                             dtype=self.ARRAY_TYPE)
        for idx, kst_time in enumerate(kst_time_list):
            sat_data = self._load_sat_data(chn, kst_time)
            sat_array[idx] = sat_data
        self.log.info("Original Data Shape: {0}".format(sat_array.shape))
        return sat_array
    
    def _load_sat_data(self, chn, kst_time):
        """
        Description:
            위성 데이터(netCDF 파일) 불러오기.
            해당 시간대에 데이터가 없으면 2분 전 데이터를 사용.
            MAX_LOOP 만큼 진행해도 없으면 NaN 배열로 사용.
        Args:
            chn, str, 위성 채널
            kst_time: str, KST 연월일시분(12자리)
        Returns:
            rdr_dBz: numpy array, 레이더 dBZ 배열
        """
        utc_time = self._kst_to_utc(kst_time)
        sat_dir = self.sat_dir.format(chn)
        sat_file = self.sat_file.format(chn, utc_time)
        sat_path = os.path.join(sat_dir, sat_file)
        
        # 위성 데이터 있는지 확인 후 없을 시 2분전 데이터를 가져옴(MAX_LOOP까지 반복)
        NOW_LOOP = 1
        while not os.path.isfile(sat_path):
            self.log.info("File doesn't exists: {0} (UTC)".format(utc_time))
            self.log.debug("File doesn't exists: {0} (UTC)".format(sat_path))
            utc_time = self._compute_date(utc_time, "min", -2)
            sat_file = self.sat_file.format(chn, utc_time)
            sat_path = os.path.join(sat_dir, sat_file)
            NOW_LOOP += 1
            if NOW_LOOP > self.MAX_LOOP:
                self.log.info("File doesn't exists: {0} (UTC)".format(utc_time))
                break
        
        # 파일이 존재해도 netCDF가 열리지 않으면 NaN 처리
        CAN_OPEN = True
        try:
            pass
            with nc.Dataset(sat_path, "r", format="netcdf4") as sat_nc:
                ipixel = sat_nc.variables['image_pixel_values']
                ipixel_ary = np.array(ipixel)
                # set error pixel's value to 0
                ipixel_ary[ipixel_ary > 49151] = 0 #set error pixel's value to 0
                # image_pixel_values Bit Size per pixel masking
                channel = ipixel.getncattr("channel_name")
                if ((channel == 'VI004') or (channel == 'VI005') or (channel == 'NR016')):
                    mask = 0b0000011111111111 #11bit mask
                elif ((channel == 'VI006') or (channel == 'NR013') or (channel == 'WV063')):
                    mask = 0b0000111111111111 #12bit mask
                elif (channel == 'SW038'):
                    mask = 0b0011111111111111 #14bit mask
                else:
                    mask = 0b0001111111111111 #13bit mask
                sat_data = np.bitwise_and(ipixel_ary, mask)
                
        except Exception as e:
            self.err_log.error("File cannot open {0} (UTC): {1}".format(utc_time, e))
            CAN_OPEN = False
        
        if CAN_OPEN:
            sat_data = sat_data.astype(self.ARRAY_TYPE)
        else:
            self.log.info("Cannot Used File: {0}(UTC) --> Make NaN array".format(utc_time))
            sat_data = np.ones((self.ny, self.nx), dtype=self.ARRAY_TYPE) * np.nan

        return sat_data

    def save_sat_ary(self, sat_array, chn, kst_start_time, kst_end_time):
        """
        Description:
            위성 데이터 배열 저장.
            memmap으로 저장 -> 추후 불러올 때 memmap 사용
            save로 저장 -> 추후 불러올 때 load 사용
        Args:
            sat_array: numpy array, 전체 위성 데이터 배열
            chn, str, 위성 채널
            kst_start_time: str, KST 연월일시분(12자리)
            kst_end_time: str, KST 연월일시분(12자리)
        Returns:
            None
        """
        saved_file = self.saved_file.format(chn, kst_start_time, kst_end_time)
        saved_path = os.path.join(self.saved_dir, saved_file)
        memmap = np.memmap(saved_path,
                           dtype=self.ARRAY_TYPE,
                           mode="w+",
                           shape=sat_array.shape)
        memmap[:] = sat_array[:]
        self.log.info("Save completely")


class RadarDataExtractor(Data):
    def __init__(self, log, err_log):
        """레이터 레이더(HSR) 설정."""
        super(RadarDataExtractor, self).__init__()
        self.nx = 2305
        self.ny = 2881
        self.headerByte = 1024
        self.headerLeng = int(self.headerByte / 2)
        self.rdr_path = "/data/kesti/2022_stepping_stone/radar/{0}/{1}"
        self.rdrFileName = "{0}/RDR_CMP_HSR_PUB_{1}.bin.gz"
        self.MAX_LOOP = 3
        self.ARRAY_TYPE = np.float32
        self.saved_dir = "/data/kesti/didimdol/jhyun/hsr_arys"
        self.saved_file = "hsr_{0}_{1}.dat"
        
        self.log, self.err_log = log, err_log
    
    def main(self, kst_start_time, kst_end_time, time_step):
        """
        Description:
            레이더 데이터를 numpy 배열로 불러오기.
        Args:
            kst_start_time: str, KST 연월일시분(12자리)
            kst_end_time: str, KST 연월일시분(12자리)
            time_step: int, standard에서 증감할 시간 간격(분)
        Returns:
            rdr_array_crop: numpy array, Crop한 전체 레이더 배열
        """
        kst_time_list = self._make_time_list(kst_start_time,
                                             kst_end_time,
                                             "min",
                                             time_step)
        rdr_array = self._make_rdr_ary(kst_time_list)
        rdr_array_crop = self._crop_rdr_ary(rdr_array)
        return rdr_array_crop

    def _make_rdr_ary(self, kst_time_list):
        """
        Description:
            레이더 데이터로 numpy 배열 생성.
        Args:
            kst_time_list: list, KST 문자열 시간대 리스트
        Returns:
            rdr_array: numpy array, 전체 레이더 배열
        """
        rdr_array = np.zeros((len(kst_time_list), self.ny, self.nx),
                             dtype=self.ARRAY_TYPE)
        for idx, kst_time in enumerate(kst_time_list):
            rdr_dBz = self._load_rdr_data(kst_time)
            rdr_array[idx] = rdr_dBz
        self.log.info("Original Data Shape: {0}".format(rdr_array.shape))
        return rdr_array

    def _load_rdr_data(self, kst_time):
        """
        Description:
            레이더 데이터(bin.gz 파일) 불러오기.
            해당 시간대에 데이터가 없으면 5분 전 데이터를 사용.
            MAX_LOOP 만큼 진행해도 없으면 NaN 배열로 사용.
            데이터 명세에 왼쪽아래->오른위 순으로 되어 위아래 반전 필요.
        Args:
            kst_time: str, KST 연월일시분(12자리)
        Returns:
            rdr_dBz: numpy array, 레이더 dBZ 배열
        """
        utc_time = self._kst_to_utc(kst_time)
        rdr_path = self.rdr_path.format(utc_time[:6], utc_time[6:8])
        rdrFileName = self.rdrFileName.format(rdr_path, utc_time)
        # print(">>>", rdrFileName)
        
        # rdr 데이터 있는지 확인 후 없을 시 5분전 데이터를 가져옴(MAX_LOOP까지 반복)
        NOW_LOOP = 1
        while not os.path.isfile(rdrFileName):
            utc_time = self._compute_date(utc_time, "min", -5)
            rdr_path = self.rdr_path.format(utc_time[:6], utc_time[6:8])
            rdrFileName = self.rdrFileName.format(rdr_path, utc_time)
            NOW_LOOP += 1
            if NOW_LOOP > self.MAX_LOOP:
                break
            self.log.info("File doesn't exists: {0} (UTC)".format(utc_time))
            self.log.debug("File doesn't exists: {0} (UTC)".format(rdrFileName))
        
        # 파일이 존재해도 Binary가 열리지 않으면 5분전 데이터를 가져옴
        CAN_OPEN = True
        try:
            with gzip.open(rdrFileName, "rb") as fg:
                rdrData = np.frombuffer(fg.read(), dtype=np.int16)
        except Exception as e:
            self.err_log.error("File cannot open {0} (UTC): {1}".format(utc_time, e))
            CAN_OPEN = False
            # raise e
        
        # 앞의 header 제외 후, 100으로 나눔, 위아래 바꿔줘야 함(HSR이 왼쪽아래->오른위 순)
        if CAN_OPEN:
            self.log.info("Used File: {0}(UTC)".format(utc_time))
            rdrData = rdrData[self.headerLeng:(self.nx * self.ny + self.headerLeng)]
            rdr_dBz = np.where(rdrData <= 0, np.nan, rdrData / 100)
            rdr_dBz = rdr_dBz.reshape(self.ny, self.nx, order="C")
            rdr_dBz = np.flipud(rdr_dBz)   # 위아래 반전
            rdr_dBz = rdr_dBz.astype(self.ARRAY_TYPE)
        else:
            self.log.info("Cannot Used File: {0}(UTC) --> Make NaN array".format(utc_time))
            rdr_dBz = np.ones((self.ny, self.nx), dtype=self.ARRAY_TYPE) * np.nan

        return rdr_dBz
    
    def _crop_rdr_ary(self, rdr_array):
        """
        Description:
            레이더 데이터 배열에서 일정 크기만 잘라내기.
            23.02.17.ver 중심점에서 상하좌우 256픽셀만 추출.
        Args:
            rdr_array: numpy array, 전체 레이더 배열
        Returns:
            rdr_array_crop: numpy array, Crop한 전체 레이더 배열
        """
        y_px = int(self.ny/2)
        x_px = int(self.nx/2)
        h_w_half = 256
        rdr_array_crop = rdr_array[::,
                                   y_px-h_w_half:y_px+h_w_half,
                                   x_px-h_w_half:x_px+h_w_half]
        self.log.info("Crop Data Shape: {0}".format(rdr_array_crop.shape))
        return rdr_array_crop
    
    def save_rdr_ary(self, rdr_array, kst_start_time, kst_end_time):
        """
        Description:
            레이더 데이터 배열 저장.
            memmap으로 저장 -> 추후 불러올 때 memmap 사용
            save로 저장 -> 추후 불러올 때 load 사용
        Args:
            rdr_array: numpy array, 전체 레이더 배열
            kst_start_time: str, KST 연월일시분(12자리)
            kst_end_time: str, KST 연월일시분(12자리)
        Returns:
            None
        """
        saved_file = self.saved_file.format(kst_start_time, kst_end_time)
        saved_path = os.path.join(self.saved_dir, saved_file)
        memmap = np.memmap(saved_path,
                           dtype=self.ARRAY_TYPE,
                           mode="w+",
                           shape=rdr_array.shape)
        memmap[:] = rdr_array[:]
        del memmap
        self.log.info("Save completely")


if __name__ == "__main__":
    from setproctitle import setproctitle
    setproctitle("extract_gk2a")
    
    from log_module import LogConfig, get_log_view

    # 로그
    upper_dir = os.path.dirname(os.path.abspath(__file__))
    file_prefix = "extract_gk2a"
    lc = LogConfig(upper_dir, file_prefix)
    # log = get_log_view(lc, log_level="DEBUG")   # default: INFO
    log = get_log_view(lc)   # default: INFO
    err_log = get_log_view(lc, log_level="WARNING", error_log=True)
    
    # kst_start_time_list = ["202203010000", "202203010002", "202203010004", "202203010006", "202203010008"]
    # kst_end_time = "202205312358"
    # # kst_end_time = "202203010258"
    # chn_list = ["ir087", "ir105", "ir112", "ir123", "ir133", "nr016", "sw038", "wv073"]
    # time_step = 10
    kst_start_time_list = ["202203170000"]
    kst_end_time = "202203172358"
    # kst_end_time = "202203010258"
    chn_list = ["wv073"]
    time_step = 10

    sat = GK2ADataExtractor(log, err_log)
    
    # 배열 생성 후 저장
    for kst_start_time in kst_start_time_list:
        log.info("----------------------------------")
        for chn in chn_list:
            sat_ary, file_kst_end_time = sat.main(chn, kst_start_time, kst_end_time, time_step)
            sat.save_sat_ary(sat_ary, chn, kst_start_time, file_kst_end_time)
            del sat_ary
