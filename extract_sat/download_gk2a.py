import datetime
import requests as req
import urllib
import argparse
import logging

logging.basicConfig(level=logging.INFO)


# 인자값 받는 인스턴스 생성
parser = argparse.ArgumentParser(description="GK2A 파일 API로 받기")
parser.add_argument("--chn", required=True, help="Channel name")
parser.add_argument("--sdate", required=True, help="Start date (UTC), YYYYMMDDHH24MISS (12자리)")
parser.add_argument("--edate", required=True, help="End date (UTC), YYYYMMDDHH24MISS (12자리)")

# 입력받은 인자값을 args에 저장
args = parser.parse_args()
chn = args.chn
sdate = args.sdate
edate = args.edate
# chn = "vi006"
# sdate = "202202281500"
# edate = "202205311458"
date = datetime.datetime.strptime(sdate, '%Y%m%d%H%M')
edate = datetime.datetime.strptime(edate, '%Y%m%d%H%M')

while date <= edate:
    now = datetime.datetime.strftime(date, '%Y%m%d%H%M')
    # url = "https://apihub.kma.go.kr/api/typ01/url/sat_file_down2.php?typ=bin&lvl=l1b&are=ko&chn={0}&tm={1}&authKey=iW6rwKXhTsOuq8Cl4T7D5A".format(chn, now)
    url = "https://apihub.kma.go.kr/api/typ05/api/GK2A/LE1B/{0}/KO/data?date={1}&authKey=M6JWB0wsQ3OiVgdMLKNziw".format(chn.upper(), now)
    try:
        urllib.request.urlretrieve(url, "/fogdata/datas/gk2a/{0}/gk2a_ami_le1b_{0}_ko020lc_{1}.nc".format(chn, now))
        logging.info(now)
        #if now.endswith("1500"):
        #    logging.info(now)
    except Exception as e:
        logging.error("NotFound: {0} / {1}".format(now, e))
    date = date + datetime.timedelta(minutes=2)
