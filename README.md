# sat_pred

## Name
- GK2A 위성영상 예측
- Tensorflow 이용하여 기본 U-net 구조에 마지막 latent 부분에 ConvLSTM 또는 TCN 추가

## Description
- 기본적으로 위성 영상(NetCDF 파일, 2차원 이미지 형식)을 이용하여 다음 위성 영상을 예측
- Crop 없이 900x900 (해상도 2km) 원본을 그대로 사용, 물리 자원 한계상 Sequence 길이, Batch size는 조정 필요
- V8, V9에서는 1차적으로 유효 영역만 crop하여 사용하며 모델은 더 작은 patch를 적용
  - 학습 후 추론할 때는 작은 size로 추론 후, 추론 결과를 병합하는 식
  - 일부 영역을 겹치게 추론하고, 겹친 개수만큼 나눔
  - input_seq보다 길어지는 추론 결과부터 object가 이동하는 경향이 거의 없음
  - 최초 추론 결과 input_seq개도 이후 시간대 추론보다는 input 값의 영향이 큰 것으로 보임
- Model 버전 설명 (model.py 또는 tcn_model.py)
  - model.py: ConvLSTM 사용
    - Init: ConvLSTM의 return_sequence=True로 input_seq = output_seq, Upsample도 맨 마지막만 진행
    - V2: output_seq만큼 ConvLSTM을 추가 후, 개별 seq 예측단마다 Upsample 진행 (skip connection 은 t+1 ~ t+6을 다음 seq에 전달)
    - V3: V2에 누락된 skip connection 부분 결합
    - V4: V3에서 skip connection을 최초 Downsample 부분을 반복하여 사용(output 부분은 V3와 동일)
    - V5: V4에서 use_bias를 True로 변경
    - V7: V3에서 output_seq만큼 ConvLSTM을 추가 대신 RepeatVector 적용
    - V9: V7에서 Basis 부분 분리하고, Lambda layer를 모두 제거
    - V3가 가장 괜찮게 나오는 편 (ConvLSTM 중에서)
  - tcn_model.py: TCN 사용
    - V6: V3에서 ConvLSTM을 TCN으로 변경, RepeatVector 적용
    - V8: V6에서 Basis 부분 분리하고, Lambda layer를 모두 제거
    - V6가 가장 괜찮게 나오는 편 (전체)

## Environment
tensorflow/tensorflow:2.10.1-gpu 도커 이미지를 기반으로 구축 (Ubuntu 20.04.5)
- Python 3.8.10
- tensorflow 2.10.1
- numpy 1.23.4
- pandas 1.5.2
- opencv-python 4.7.0.68
- dask 2022.10.2
- netCDF4 1.6.2
- keras-tcn 3.5.0
- pysolar 0.10
- 이외 파이썬 라이브러리: requirements.txt

## Execution
main.py 에서 config/ 에 있는 yaml 파일을 반영하여 실행
- 데이터 파싱: python main.py -c /path/of/config/file -m data
- 학습: python main.py -c /path/of/config/file -m train
- 추론: python main.py -c /path/of/config/file -m predict -st 202203150700 -et 202203150850
- 이미지 생성: python main.py -c /path/of/config/file -m image -st 202203150700 -et 202203150850
- 검증: python main.py -c /path/of/config/file -m valid -st 202203150700 -et 202203150850

## File Structure
- datas/
  - 채널 별 디렉토리 존재
  - 원본 GK2A NetCDF 파일이 적재
- extract_sat/
  - extract_data.py
    - NetCDF 파일을 .dat 파일로 변환
    - 대용량이기 때문에 np.memmap 으로 저장 (데이터 타입: uint16)
    - 해당 .dat 파일을 추후 학습 등에서 Dataloader에서 불러다가 사용
    - 해당 내용을 src/extract_data.py 로 옮겨서 해당 파일은 없어도 됨
  - download_gk2a.py
    - 기상청 API허브(https://apihub.kma.go.kr/)에서 API 주소를 받아서 GK2A 파일 다운로드
    - 1일 다운로드 제한 존재 (1,000건, 1GB, 매일 자정에 초기화)
- sat_arys/
  - extract_sat/ 에 있는 NetCDF 파일을 모델이 바로 사용할 수 있도록 .dat로 저장하는 위치
  - 결과는 KST 시간대로 저장
- result_data/
  - 추론 결과 저장하는 위치(채널별 저장)
  - 결과는 UTC 시간대로 저장
- tf_ver/
  - calibration_source/: Calibration에 사용할 데이터
  - checkpoints/: 채널, 날짜 별로 학습 Checkpoint 저장
  - models/: 채널, 날짜 별로 학습한 모델 저장
  - history/: 채널 별로 데이터 설정, 모델 및 관련 하이퍼 파라미터 저장
  - log/: 로그 파일 적재
  - err_log/: 에러 로그 파일 적재
  - trainlog/: 학습 시 적용한 CSV Logger 내용 저장
  - configs/: 채널 별로 설정 yaml 파일 적재
  - utils/: 범용 설정 코드
    - code_utils.py: 범용 유틸리티 함수
    - log_module.py: 로그 용
    - tf_settings.py: tensorflow 설정
  - src/: 전처리, 학습, 추론, 후처리 코드
    - custom_loss.py: Loss 함수, 원래 loss 함수에 증폭값 부여
    - data_parser.py: Normalizer 등 데이터 변환
    - dataset.py: Dataloader 역할
    - extract_data.py: 학습 및 추론 시 원본 데이터를 array로 가져옴
    - inference: 추론 및 추론 결과 npy로 저장
    - make_img.py: 추론 결과 및 해당 시간대 원본 위성 흑백 이미지 생성 및 저장
    - model.py: Tensorflow 모델 생성 및 컴파일 (ConvLSTM 사용)
      - Init: ConvLSTM의 return_sequence=True로 input_seq = output_seq, Upsample도 맨 마지막만 진행
      - V2: output_seq만큼 ConvLSTM을 추가 후, 개별 seq 예측단마다 Upsample 진행 (skip connection 은 t+1 ~ t+6을 다음 seq에 전달)
      - V3: V2에 누락된 skip connection 부분 결합
      - V4: V3에서 skip connection을 최초 Downsample 부분을 반복하여 사용(output 부분은 V3와 동일)
      - V5: V4에서 use_bias를 True로 변경
      - V7: V3에서 output_seq만큼 ConvLSTM을 추가 대신 RepeatVector 적용
      - V9: V7에서 Basis 부분 분리하고, Lambda layer를 모두 제거
      - V3가 가장 괜찮게 나오는 편 (ConvLSTM 중에서)
    - tcn_model.py: Tensorflow 모델 생성 및 컴파일 (TCN 사용)
      - V6: V3에서 ConvLSTM을 TCN으로 변경, RepeatVector 적용
      - V8: V6에서 Basis 부분 분리하고, Lambda layer를 모두 제거
      - V6가 가장 괜찮게 나오는 편 (전체)
    - preprocess.py: 입력 데이터 전처리
    - train.py: 모델 학습 및 저장
    - valid.py: 모델 검증 및 저장
  - main.py: 전체 통합 메인 모듈 (데이터 파싱, 학습, 추론, 이미지 생성, 검증)

## Additional Research
- 학습 모델에 custom class 반영한 경우, custom 객체 저장방법 반영해보기 (현재 compile=False 적용)
  - https://www.tensorflow.org/guide/keras/save_and_serialize#registering_the_custom_object
- 다른 AI 알고리즘 테스트
  - 예) GAN 계열 생성모델, Visual Transformer 등
  - SimVP v2: https://github.com/chengtan9907/OpenSTL 확인 필요
- Loss function 바꿔서 진행해보기
  - 이미지 전체가 아닌 일부 patch에서만 loss 구하는 거 적용
  - Super Resolution에서 사용하는 Perceptual loss 내용 반영해보기 (선명도 증대)
- 네트워크 수정
  - Connection에서 Downsampling 쪽과 Seq. 결과 layer를 모두 concatenate 해보는 방법
  - Keras 내장된 Multi head attention layer 추가해보는 방식 (self-attention 처럼 진행)